from bluetooth import *
from ioHandler import IOHandler
import sys, time, pygame, json

# Connection settings
addr = "00:15:83:2A:49:0A"
uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

# Search for HexaHERM
print "Searching for HexaHERM"
service_matches = find_service( uuid = uuid, address = addr )
if len(service_matches) == 0:
    print "Could not find HexaHERM"
    sys.exit(0)
print "Connection found!"

# Create the client socket
sock = BluetoothSocket( RFCOMM )
robo = service_matches[0]
port = robo["port"]
host = robo["host"]
sock.connect((host, port))

# Main program
pygame.init()
io = IOHandler()
mode = 'movement'

# Robot variables
autonomic = False
height = 130
vel = 200
angvel = 0.8
torque = 800
com_vel = vel
com_angvel = angvel

pre_move = None
pre_tilt = None
pre_shift = None
pre_vel = None
pre_angvel = None
pre_torque = None


print "Ready for input"
while True:
    time.sleep(1/20.)
    io.update()
    if io.check_key(pygame.K_ESCAPE):
        sys.exit(0)

    cmd = {}

    move = {
        "movement_x": 0.0,
        "movement_y": 0.0,
        "rotation": 0.0
    }
    tilt = {
        "tilt_x": 0.0,
        "tilt_y": 0.0,
        "tilt_z": 0.0
    }
    shift = {
        "shift_x": 0.0,
        "shift_y": 0.0,
        "shift_z": 0.0
    }
    
    if mode == 'tilt':
        shift["shift_x"] = io.get_joystick_axis(1)
        shift["shift_y"] = io.get_joystick_axis(0)
        tilt["tilt_x"] = io.get_joystick_axis(2)
        tilt["tilt_y"] = - io.get_joystick_axis(3)
        
        tilt["tilt_z"] = 0
        shift["shift_z"] = 0
        
        if io.check_joystick(1):
            shift["shift_z"] += 1
        if io.check_joystick(3):
            shift["shift_z"] -= 1

        if io.check_joystick(4):
            tilt["tilt_z"] += 1
        if io.check_joystick(5):
            tilt["tilt_z"] -= 1
            
        if io.check_joystick(6):
            height = max(0, height - 5)
            cmd["set_height"] = height
        if io.check_joystick(7):
            height = min(200, height + 5)
            cmd["set_height"] = height
            
        if io.check_joystick_pressed(8):
            mode = 'movement'
            print 'Switched to movement-mode'
        if io.check_joystick_pressed(9):
            cmd["dance"] = True


    elif mode == 'movement':
        move["movement_x"] = - com_vel * io.get_joystick_axis(1)
        move["movement_y"] = - com_vel * io.get_joystick_axis(0)
        
        move["movement_x"] = int(2**0.5 * move["movement_x"])
        move["movement_y"] = int(2**0.5 * move["movement_y"])
        
        vel    = vel    + 5   * io.get_joystick_hat()[1]
        angvel = angvel + 0.1 * io.get_joystick_hat()[0]
        
        if io.check_joystick_pressed(1):
            torque = max(0, torque - 100)
        if io.check_joystick_pressed(3):
            torque = min(1023, torque + 100)
            
        if io.check_joystick(4):
            move["rotation"] -= com_angvel
        if io.check_joystick(5):
            move["rotation"] += com_angvel
        
        if io.check_joystick(6):
            height = max(0, height - 5)
            cmd["set_height"] = height
        if io.check_joystick(7):
            height = min(200, height + 5)
            cmd["set_height"] = height
            
        if io.check_joystick_pressed(8):
            mode = 'tilt'
            print 'Switched to tilt-mode'
        if io.check_joystick_pressed(9):
            autonomic = not autonomic
            cmd["autonomic"] = autonomic

            
    if move != pre_move:
        for key in move:
            cmd[key] = move[key]
    if tilt != pre_tilt:
        for key in tilt:
            cmd[key] = tilt[key]
    if shift != pre_shift:
        for key in shift:
            cmd[key] = shift[key]
    if vel != pre_vel:
        cmd["velocity"] = vel
    if angvel != pre_angvel:
        cmd["angular_velocity"] = angvel
    if torque != pre_torque:
        cmd["set_torque"] = torque
        
    pre_move = move
    pre_tilt = tilt
    pre_shift = shift
    pre_vel = vel
    pre_angvel = angvel
    pre_torque = torque
    
    if cmd:
        print json.dumps(cmd)
        sock.send(json.dumps(cmd))

    #sock.recv(1024)

sock.close()
