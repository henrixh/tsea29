import pygame, sys

# -- IOHandler --
# Collects user key- and mouse-input and stores it in maps

class IOHandler():
    # All stored user input
    text = ''
    
    keys = {}
    keys_pressed = {}
    keys_released = {}
    
    mouse = {'x': 0, 'y': 0, 'pre_x': 0, 'pre_y': 0}
    mouse_pressed = {}
    mouse_released = {}
    
    joystick = {}
    joystick_pressed = {}
    joystick_released = {}
    joystick_hat = (0,0)
    joystick_axis = {}
    joystick_keyboard_replacements = {
        0: pygame.K_SPACE,
    }
    
    # Main pygame event handler
    def update(self):
        # Reset
        for e in self.keys_pressed:  self.keys_pressed[e] = False
        for e in self.keys_released: self.keys_released[e] = False
        for e in self.mouse_pressed:  self.mouse_pressed[e] = False
        for e in self.mouse_released: self.mouse_released[e] = False
        for e in self.joystick_pressed:  self.joystick_pressed[e] = False
        for e in self.joystick_released: self.joystick_released[e] = False
        self.mouse['pre_x'] = self.mouse['x']
        self.mouse['pre_y'] = self.mouse['y']

        # Init joystick
        joystick = pygame.joystick
        joystick.init()
        for js in range(joystick.get_count()):
            joystick = joystick.Joystick(js)
            joystick.init()
            
        # Loop through stack of events
        pygame.event.pump()
        for event in pygame.event.get():
            
            # Quit-button
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                
            # Key pressed
            if event.type == pygame.KEYDOWN:
                self._handle_button_event(event.key, self.keys, self.keys_pressed, True)
                if self.keys_pressed[event.key]:
                    if event.key >= 97 and event.key <= 122:
                        self.text += pygame.key.name(event.key)
                    if event.key == 32 and self.text:
                        self.text += ' '
                    if event.key == 8:
                        self.text = self.text[0:-1]
                
            # Key released
            if event.type == pygame.KEYUP:
                self._handle_button_event(event.key, self.keys, self.keys_released, False)
                
            # Mouse pressed
            if event.type == pygame.MOUSEBUTTONDOWN:
                self._handle_button_event(event.button, self.mouse, self.mouse_pressed, True)
                
            # Mouse released
            if event.type == pygame.MOUSEBUTTONUP:
                self._handle_button_event(event.button, self.mouse, self.mouse_released, False)
                
            # Mouse move
            if event.type == pygame.MOUSEMOTION:
                (self.mouse['x'], self.mouse['y']) = event.pos

            # Joystick button press
            if event.type == pygame.JOYBUTTONDOWN:
                self._handle_button_event(event.button, self.joystick, self.joystick_pressed, True)
                if event.button in self.joystick_keyboard_replacements:
                    self._handle_button_event(self.joystick_keyboard_replacements[event.button], self.keys, self.keys_pressed, True)

            # Joystick button release
            if event.type == pygame.JOYBUTTONUP:
                self._handle_button_event(event.button, self.joystick, self.joystick_released, False)
                if event.button in self.joystick_keyboard_replacements:
                    self._handle_button_event(self.joystick_keyboard_replacements[event.button], self.keys, self.keys_released, False)

            # Joystick ball motion
            if event.type == pygame.JOYHATMOTION:
                self.joystick_hat = event.value
                hat = [event.value[0], -event.value[1]]
                self._set_arrowkeys_as_joystick(hat)

            # Joystick axis motion
            if event.type == pygame.JOYAXISMOTION:
                self.joystick_axis[event.axis] = event.value
                hat = [None,None]
                hat[event.axis % 2 == 1] = int(event.value)
                self._set_arrowkeys_as_joystick(hat)


    # Activate the arrowkeys via joystick hat or axis
    def _set_arrowkeys_as_joystick(self, pos):
        if pos[0] == 0:
            self._handle_button_event(pygame.K_LEFT,  self.keys, self.keys_released, False)
            self._handle_button_event(pygame.K_RIGHT, self.keys, self.keys_released, False)
        if pos[1] == 0:
            self._handle_button_event(pygame.K_UP,   self.keys, self.keys_released, False)
            self._handle_button_event(pygame.K_DOWN, self.keys, self.keys_released, False)
        if pos[0] ==-1: self._handle_button_event(pygame.K_LEFT,  self.keys, self.keys_pressed, True)
        if pos[0] == 1: self._handle_button_event(pygame.K_RIGHT, self.keys, self.keys_pressed, True)
        if pos[1] ==-1: self._handle_button_event(pygame.K_UP,    self.keys, self.keys_pressed, True)
        if pos[1] == 1: self._handle_button_event(pygame.K_DOWN,  self.keys, self.keys_pressed, True)
                
    # Store whether a button is held, along with the switch (pressed or released)
    def _handle_button_event(self, button, dic, switch, down):
        try: dic[button]
        except: dic[button] = not down
        if (down and not dic[button]) or (not down and dic[button]):
            switch[button] = True
        dic[button] = down
        
    # Check if key or keys are in a dictionary
    def _check_dict(self, key, dic, All):
        if isinstance(key, list):
            r = [self._check_dict(k, dic, All) for k in key]
            if All: return all(r)
            else: return any(r)
        try: return dic[key]
        except: return False

            
    # Keys
    
    # Check if key is held down
    def check_key(self, key, All=False):
        return self._check_dict(key, self.keys, All)
    
    # Check if key is pressed
    def check_key_pressed(self, key, All=False):
        return self._check_dict(key, self.keys_pressed, All)

    # Check if key is released
    def check_key_released(self, key, All=False):
        return self._check_dict(key, self.keys_released, All)
    

    # Mouse
    
    # Check if mousebutton is held down
    def check_mouse(self, button, All=False):
        return self._check_dict(button, self.mouse, All)
    
    # Check if mousebutton is pressed
    def check_mouse_pressed(self, button, All=False):
        return self._check_dict(button, self.mouse_pressed, All)

    # Check if mousebutton is released
    def check_mouse_released(self, button, All=False):
        return self._check_dict(button, self.mouse_released, All)
    
    # Get mouse position - tuple (x,y)
    def get_mouse(self):
        try: return (self.mouse['x'], self.mouse['y'])
        except: return False
    
    # Get mouse relative movement - tuple(rx,ry)
    def get_mouse_previous(self):
        try: return (self.mouse['pre_x'], self.mouse['pre_y'])
        except: return False
    

    # Joystick
    
    # Check if joystick button is held down
    def check_joystick(self, button, All=False):
        return self._check_dict(button, self.joystick, All)
    
    # Check if joystick button is pressed
    def check_joystick_pressed(self, button, All=False):
        return self._check_dict(button, self.joystick_pressed, All)

    # Check if joystick button is released
    def check_joystick_released(self, button, All=False):
        return self._check_dict(button, self.joystick_released, All)

    # Get joystick hat position - tuple (x,y)
    def get_joystick_hat(self):
        try: return self.joystick_hat
        except: return (0,0)
        
    # Get joystick axis position
    def get_joystick_axis(self, axis):
        try: return self.joystick_axis[axis]
        except: return 0
        
        
    # User written text (A-Z + space)
    
    # Reset written text
    def reset_text(self, text=''):
        self.text = text

    # Get written text
    def get_text(self):
        return self.text


if __name__ == "__main__":
    pygame.init()
    ioHandler = IOHandler()
    while True:
        ioHandler.update()
        if ioHandler.check_key_pressed(pygame.K_ESCAPE):
            pygame.quit()
            sys.exit()



