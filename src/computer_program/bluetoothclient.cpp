#include "bluetoothclient.h"

////////////
// Public //
////////////

/**
 * Construct a BluetoothClient
 * User is responsible for turning on power to local bluetooth device
 */
BluetoothClient::BluetoothClient ()
{
  isConnected = false;
  uuidstring = "94f39d29-7d6d-437d-973b-fba39e49d4ee";
  const QString uuidQstring(uuidstring);
  const QBluetoothUuid uuid(uuidQstring);

  QBluetoothAddress addr("00:15:83:2A:49:0A");

  discoveryAgent = new QBluetoothServiceDiscoveryAgent ();
  discoveryAgent->setUuidFilter (uuid);
  discoveryAgent->setRemoteAddress(addr);
  connect (discoveryAgent, SIGNAL (finished ()), this, SLOT (scanComplete ()));
  qDebug () << "Attempting to connect";
}

/**
 * Destroy the BluetoothClient
 */
BluetoothClient::~BluetoothClient()
{
  delete discoveryAgent;
  delete uuid;
  delete sock;
  delete currentHexaHerm;
}

/**
 * Start a scan for H.E.X.A Herms.
 * Will emit the signal scanned () when the scan has finished.
 */
void
BluetoothClient::scan () {
  qDebug () << "Scanning for Hexa H.E.R.M.";
  discoveryAgent->start (QBluetoothServiceDiscoveryAgent::FullDiscovery);
}

/**
 * Connect to a specific device.
 *
 * Will emit the signal connected () when
 * it is safe to send data to the device.
 *
 * Will emit the signal disconnected () if the connection goes
 * down.
 *
 * Will emit the signal dataRecieved () when there is data to recive
 * from the hexaherm.
 *
 * @param hexaherm the hexaherm to connect to.
 */
void
BluetoothClient::connectToHerm (QBluetoothServiceInfo &hexaherm) {
  if (isConnected) {
    qDebug () << "Already connected. Please disconnect first";
    return;
  }

  currentHexaHerm = new QBluetoothServiceInfo (hexaherm);

  // Create socket
  sock = new QBluetoothSocket (QBluetoothServiceInfo::RfcommProtocol);
  sock->connectToService(hexaherm);
    sock->
  // Connect signals
  connect(sock, SIGNAL(readyRead()),
          this, SLOT(dataRecieved()));

  connect(sock, SIGNAL(connected()),
          this, SLOT(connectionEstablished()));

  connect(sock, SIGNAL(disconnected()),
          this, SIGNAL(disconnectionEstablished()));
}

void
BluetoothClient::disconnectFromHerm() {
    sock->disconnectFromService();
    disconnectionEstablished();
}

/**
 * Send a string to a hexaherm. The string will be sent as UTF-8.
 * @param text string to send.
 */
void
BluetoothClient::send (QString &text) {
  const char * data = text.toUtf8 ().constData ();
  sock->write (data, strlen (data));
}

/**
 * Read recieved data.
 * @return The read data. May be the empty string.
 */
QString
BluetoothClient::read () {
  int n = 10000;
  char * data = (char *) calloc (n,sizeof(char));
  sock->read (data, n);

  QString qstr = QString::fromLatin1 (data);

  free (data);
  return qstr;
}

///////////////////
// Private Slots //
///////////////////

void
BluetoothClient::scanComplete () {
  qDebug () << "Done scanning for H.E.X.A Herms";
  QList<QBluetoothServiceInfo> foundherms = discoveryAgent->discoveredServices();
  hexaherms.clear();
  discoveryAgent->clear ();

  uuidstring = "00000000-0000-0000-0000-000000000000";
  const QString uuidQstring(uuidstring);
  const QBluetoothUuid uuid(uuidQstring);

  qDebug () << "List contains" << foundherms.size() << "items";
  foreach( QBluetoothServiceInfo item, foundherms ) {
      qDebug () << "Avaliable:" << item.serviceAvailability() << "Channel:" << item.serverChannel();
      qDebug () << "Service uuid:" << item.serviceUuid();
      qDebug () << "Service class uuids:" << item.serviceClassUuids();
      qDebug () << item.device().name() << item.serviceName();
      // qDebug () << item.device().address();
      qDebug () << "--------------";

      if (item.serviceUuid() != uuid) {
          hexaherms.append(item);
      }
    }
    if(hexaherms.size() > 0){
        emit scanned (true);
    } else {
        emit scanned (false);
    }
}

void
BluetoothClient::connectionEstablished() {
  isConnected = true;
  emit connected ();
  emit updateWindow ();
}

void
BluetoothClient::disconnectionEstablished() {
  isConnected = false;
  emit disconnected ();
}

void
BluetoothClient::dataRecieved () {
  emit gotData ();
  emit newData (this->read());
}
