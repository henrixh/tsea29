#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "commands.h"
#include "stdlib.h"
#include "math.h"
#include "ctime"
#include "QTextStream"

using namespace std;

map<string,int> commands_idx;

/**
 * Collection of available commands.
 */
void initializeCommands()
{
    // Available commands with their indexes
    commands_idx["list"] = 1;
    commands_idx["keys"] = 2;

    commands_idx["ctp1"] = 3;
    commands_idx["ctp2"] = 4;
    commands_idx["clear"] = 5;
    commands_idx["torque"] = 6;
    commands_idx["vel"] = 7;
    commands_idx["angvel"] = 8;
    commands_idx["move"] = 9;
    commands_idx["height"] = 10;

    commands_idx["front_kp"] = 11;
    commands_idx["front_kd"] = 12;
    commands_idx["front_ki"] = 13;
    commands_idx["front_sc"] = 14;

    commands_idx["v_kp"] = 15;
    commands_idx["v_kd"] = 16;
    commands_idx["v_ki"] = 17;
    commands_idx["v_sc"] = 18;

    commands_idx["omega_kp"] = 19;
    commands_idx["omega_kd"] = 20;
    commands_idx["omega_ki"] = 21;
    commands_idx["omega_sc"] = 22;
}

/**
 * Create the main window.
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Bluetooth
    btclient = new BluetoothClient;

    // Receive data through bluetooth
    connect(btclient, SIGNAL(newData(const QString)),
            this, SLOT(sortData(const QString)));

    // Switch window if connection was established
    connect(btclient, SIGNAL(updateWindow()),
            this, SLOT(connected_window()));

    // See if we found Hexa H.E.R.M.
    connect(btclient, SIGNAL(scanned(bool)),
            this, SLOT(update_scan(bool)));

    // Initialize available commands and hide second & third "Bluetooth"-tab
    initializeCommands();
    ui->connection->removeTab(1);
    ui->connection->removeTab(1);

    // Remove the ability for widgets to have focus
    ui->controlParameters->setFocusPolicy(Qt::NoFocus);
    ui->controlParameters2->setFocusPolicy(Qt::NoFocus);
    ui->controlParameters3->setFocusPolicy(Qt::NoFocus);
    ui->status_log->setFocusPolicy(Qt::NoFocus);
    ui->decision_log->setFocusPolicy(Qt::NoFocus);
    ui->sensors->setFocusPolicy(Qt::NoFocus);
    ui->commandoProtocol->setFocusPolicy(Qt::NoFocus);
    ui->telemetry->setFocusPolicy(Qt::NoFocus);
    ui->connection->setFocusPolicy(Qt::NoFocus);
    ui->button_auto->setFocusPolicy(Qt::NoFocus);

    // Setup graph(s)
    ui->plot->yAxis->setLabel("y");
    ui->plot->xAxis->setLabel("x");
    ui->plot->yAxis->setRange(0, 300);
    ui->plot->xAxis->setRange(0, x_limit);

    ui->plot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(9);
    ui->plot->legend->setFont(legendFont);
    ui->plot->legend->setBrush(QBrush(QColor(255, 255, 255, 230)));
    ui->plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom | Qt::AlignRight);

    ui->plot->addGraph(); // Front BLUE
    ui->plot->graph(0)->setPen(QPen(Qt::blue));
    ui->plot->graph(0)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 1));
    ui->plot->graph(0)->setName("Front");

    ui->plot->addGraph(); // RightFront RED
    ui->plot->graph(1)->setPen(QPen(Qt::red));
    ui->plot->graph(1)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 1));
    ui->plot->graph(1)->setName("RightFront");

    ui->plot->addGraph(); // LeftFront GREEN
    ui->plot->graph(2)->setPen(QPen(Qt::green));
    ui->plot->graph(2)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 1));
    ui->plot->graph(2)->setName("LeftFront");

    ui->plot->addGraph(); // RightBack CYAN
    ui->plot->graph(3)->setPen(QPen(Qt::cyan));
    ui->plot->graph(3)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 1));
    ui->plot->graph(3)->setName("RightBack");

    ui->plot->addGraph(); // LeftBack MAGENTA
    ui->plot->graph(4)->setPen(QPen(Qt::magenta));
    ui->plot->graph(4)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 1));
    ui->plot->graph(4)->setName("LeftBack");

    ui->plot->replot();
}

/**
 * Destroy the main window
 */
MainWindow::~MainWindow()
{
    // Disconnect from robot
    btclient->disconnectFromHerm();
    delete ui;
}

/**
 * Given a command typed in prompt, extracts the parameter.
 * @param s string to be extracted from.
 * @return parameter (or initial string).
 */
string extractParameter(string s){
    // Help function for prompt-inputs
    string parameter;
    int end = s.find(" "); // Find where the parameter begins.
    if(end > 0){
        for(int i = end+1; i < s.size(); i++){
            parameter.push_back(s[i]);
        }
    } else {
        return s; // Returns the initial string if no parameter was found.
    }
    return parameter;
}

/**
 * Takes the input given in the prompt and checks if it's valid.
 * If it's valid, given function will be called.
 */
void MainWindow::on_prompt_editingFinished()
{
    string input = ui->prompt->text().toStdString();

    vector<QString> names;
    vector<QJsonValue> values;

    QString name;
    QJsonValue val;

    QString parameter;
    string inputCommand;

    QString curr_prompt;
    QTableWidgetItem *item;
    QString update;

    // Extract name of the command in the given input
    int end = input.find(" ");
    if(end > 0){
        for(int i = 0; i < end; i++){
            inputCommand.push_back(input[i]);
        }
    } else {
        inputCommand = input;
    }


    // If input isn't empty:
    if(input.size() != 0){
        // Check if input is a valid command
        curr_prompt = ui->prompt->text();
        ui->prompt->clear();

        switch(commands_idx[inputCommand]){
            // Command: list
            case 1:
                ui->commandoProtocol->addItem("List of commands:");
                ui->commandoProtocol->addItem("torque\t[double param]");
                ui->commandoProtocol->addItem("vel\t[double param]");
                ui->commandoProtocol->addItem("angvel\t[double param]");
                ui->commandoProtocol->addItem("height\t[double param]");
                ui->commandoProtocol->addItem("move\t(to enable movement)");
                ui->commandoProtocol->addItem("clear\t(empty this window)");
                break;
            // Command: keys
            case 2:
                ui->commandoProtocol->addItem("List of movement-keys:");
                ui->commandoProtocol->addItem("Arrow-Up:\tMove forward.");
                ui->commandoProtocol->addItem("Arrow-Down:\tMove backwards.");
                ui->commandoProtocol->addItem("Arrow-Left:\tMove left.");
                ui->commandoProtocol->addItem("Arrow-Right:\tMove right.");
                ui->commandoProtocol->addItem("Q:\tRotate left.");
                ui->commandoProtocol->addItem("E:\tRotate right.");
                break;
            // Command: ctp1
            case 3:
                ui->commandoProtocol->addItem("List of controlparameters (1/2): ");
                ui->commandoProtocol->addItem("front_kp\t[float value]");
                ui->commandoProtocol->addItem("front_kd\t[float value]");
                ui->commandoProtocol->addItem("front_ki\t[float value]");
                ui->commandoProtocol->addItem("front_sc\t[float value]");
                ui->commandoProtocol->addItem("v_kp\t[float value]");
                ui->commandoProtocol->addItem("v_kd\t[float value]");
                break;
            // Command: ctp2
            case 4:
                ui->commandoProtocol->addItem("List of controlparameters (2/2): ");
                ui->commandoProtocol->addItem("v_ki\t[float value]");
                ui->commandoProtocol->addItem("v_sc\t[float value]");
                ui->commandoProtocol->addItem("omega_kp\t[float value]");
                ui->commandoProtocol->addItem("omega_kd\t[float value]");
                ui->commandoProtocol->addItem("omega_ki\t[float value]");
                ui->commandoProtocol->addItem("omega_sc\t[float value]");
                break;
            // Command: clear
            case 5:
                ui->commandoProtocol->clear();
                break;
            // Command: torque
            case 6:
                name = "set_torque";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters->item(4, 0)->setFlags(ui->controlParameters->item(4, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters->setItem(4, 0, item);
                    ui->controlParameters->item(4, 0)->setFlags(ui->controlParameters->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters->item(4, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: vel
            case 7:
                name = "velocity";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                // Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters->item(1, 0)->setFlags(ui->controlParameters->item(1, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters->setItem(1, 0, item);
                    ui->controlParameters->item(1, 0)->setFlags(ui->controlParameters->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters->item(1, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: angvel
            case 8:
                name = "angular_velocity";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                // Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters->item(2, 0)->setFlags(ui->controlParameters->item(2, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters->setItem(2, 0, item);
                    ui->controlParameters->item(2, 0)->setFlags(ui->controlParameters->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters->item(2, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: move
            case 9:
                // Update control parameter
                update = "move";
                movement = true;
                item = new QTableWidgetItem(update);

                ui->commandoProtocol->addItem(curr_prompt);
                ui->controlParameters->setItem(5, 0, item);
                ui->controlParameters->item(5, 0)->setFlags(ui->controlParameters->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                ui->controlParameters->item(5, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

                ui->prompt->clearFocus();
                break;
            // Command: height
            case 10:
                name = "set_height";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                // Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters->item(3, 0)->setFlags(ui->controlParameters->item(3, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters->setItem(3, 0, item);
                    ui->controlParameters->item(3, 0)->setFlags(ui->controlParameters->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters->item(3, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: front_kp
            case 11:
                name = "set_front_kp";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(0, 0)->setFlags(ui->controlParameters2->item(0, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(0, 0, item);
                    ui->controlParameters2->item(0, 0)->setFlags(ui->controlParameters2->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(0, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: front_kd
            case 12:
                name = "set_front_kd";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(1, 0)->setFlags(ui->controlParameters2->item(1, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(1, 0, item);
                    ui->controlParameters2->item(1, 0)->setFlags(ui->controlParameters2->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(1, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: front_ki
            case 13:
                name = "set_front_ki";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(0, 0)->setFlags(ui->controlParameters3->item(0, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(0, 0, item);
                    ui->controlParameters3->item(0, 0)->setFlags(ui->controlParameters3->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(0, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: front_sc
            case 14:
                name = "set_front_sc";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(1, 0)->setFlags(ui->controlParameters3->item(1, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(1, 0, item);
                    ui->controlParameters3->item(1, 0)->setFlags(ui->controlParameters3->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(1, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: v_kp
            case 15:
                name = "set_v_kp";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(2, 0)->setFlags(ui->controlParameters2->item(2, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(2, 0, item);
                    ui->controlParameters2->item(2, 0)->setFlags(ui->controlParameters2->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(2, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: v_kd
            case 16:
                name = "set_v_kd";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(3, 0)->setFlags(ui->controlParameters2->item(3, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(3, 0, item);
                    ui->controlParameters2->item(3, 0)->setFlags(ui->controlParameters2->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(3, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: v_ki
            case 17:
                name = "set_v_ki";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(2, 0)->setFlags(ui->controlParameters3->item(2, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(2, 0, item);
                    ui->controlParameters3->item(2, 0)->setFlags(ui->controlParameters3->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(2, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: v_sc
            case 18:
                name = "set_v_sc";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(3, 0)->setFlags(ui->controlParameters3->item(3, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(3, 0, item);
                    ui->controlParameters3->item(3, 0)->setFlags(ui->controlParameters3->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(3, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: omega_kp
            case 19:
                name = "set_omega_kp";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(4, 0)->setFlags(ui->controlParameters2->item(4, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(4, 0, item);
                    ui->controlParameters2->item(4, 0)->setFlags(ui->controlParameters2->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(4, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: omega_kd
            case 20:
                name = "set_omega_kd";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters2->item(5, 0)->setFlags(ui->controlParameters2->item(5, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters2->setItem(5, 0, item);
                    ui->controlParameters2->item(5, 0)->setFlags(ui->controlParameters2->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters2->item(5, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: omega_ki
            case 21:
                name = "set_omega_ki";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(4, 0)->setFlags(ui->controlParameters3->item(4, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(4, 0, item);
                    ui->controlParameters3->item(4, 0)->setFlags(ui->controlParameters3->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(4, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            // Command: omega_sc
            case 22:
                name = "set_omega_sc";
                for(int i = end+1; i < input.size(); i++){
                    parameter.push_back(input[i]);
                }
                val = parameter.toDouble();

                //Update control parameter
                if(!(val == 0 && parameter.toStdString() != "0")){
                    ui->commandoProtocol->addItem(curr_prompt);
                    names.push_back(name);
                    values.push_back(val);
                    sendData(createJsonObject(names, values));

                    update = QString::fromStdString(extractParameter(input));
                    item = new QTableWidgetItem(update);
                    ui->controlParameters3->item(5, 0)->setFlags(ui->controlParameters3->item(5, 0)->flags() | Qt::ItemIsEditable);

                    ui->controlParameters3->setItem(5, 0, item);
                    ui->controlParameters3->item(5, 0)->setFlags(ui->controlParameters3->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
                    ui->controlParameters3->item(5, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
                break;
            default:
                ui->commandoProtocol->addItem("Not a function");
                break;
        }
        ui->commandoProtocol->scrollToBottom();
    }
}

/**
 * If prompt is edited, disable the movement-keys.
 * @param arg1
 */
void MainWindow::on_prompt_textChanged(const QString &arg1)
{
    // Update control parameter if prompt has received focus
    QTableWidgetItem *item;
    QString focus = "Prompt";

    item = new QTableWidgetItem(focus);

    ui->controlParameters->setItem(5, 0, item);
    ui->controlParameters->item(5, 0)->setFlags(ui->controlParameters->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(5, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    movement = false;
}

/**
 * Attempts to connect to Hexa H.E.R.M.
 */
void MainWindow::on_button_connect_clicked()
{
    btclient->connectToHerm(btclient->hexaherms.front());

    //connected_window();
}

/**
 * Creates a temporary log file.
 * Switches tab to prompt-window.
 */
void MainWindow::connected_window()
{
    // "Welcome message"
    ui->commandoProtocol->clear();
    ui->commandoProtocol->addItem("Type the following:");
    ui->commandoProtocol->addItem("'keys' for key-bindings.");
    ui->commandoProtocol->addItem("'list' for available commands.");
    ui->commandoProtocol->addItem("'ctp1' for control parameters. (1/2)");
    ui->commandoProtocol->addItem("'ctp2' for control parameters. (2/2)");

    // Update control parameter
    ui->status_log->addItem("Connected!");
    ui->status_log->item(0)->setFlags(ui->status_log->item(0)->flags() ^ Qt::ItemIsSelectable ^ Qt::ItemIsEditable);
    ui->status_log->scrollToBottom();

    // Update current "Bluetooth"-tab
    ui->connection->insertTab(1, ui->tab_2, "Bluetooth");
    ui->connection->removeTab(0);

    // Set global variable
    connected = true;

    // Create log file
    QString path = "../computer_program/Logs/log";
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%d-%m-%Y-%I-%M-%S.txt", timeinfo);
    string str(buffer);

    filename = path + QString::fromStdString(str);
    cout << filename.toStdString() << endl;
    file.setFileName(filename);
    file.open(QIODevice::ReadWrite);

    sendData(autoMode(false));
}


/**
 * Resets all settings and tells H.E.X.A Herm
 * to stop before disconnect.
 * Lets user decide if log file should be kept or removed.
 * Switches tab to connect-window.
 */
void MainWindow::on_button_disconnect_clicked()
{
    // Update variables
    QTableWidgetItem *item2;
    QTableWidgetItem *item3;
    QTableWidgetItem *item4;
    QTableWidgetItem *item5;
    QTableWidgetItem *item6;
    QTableWidgetItem *item7;
    QTableWidgetItem *item8;
    QTableWidgetItem *item9;
    QTableWidgetItem *item10;
    QColor *colour;
    QString update;
    QString update_param = "DEF";
    QString update_focus = "N/A";

    update = "OFF";
    colour = new QColor(255, 125, 125, 255);
    item2 = new QTableWidgetItem(update);
    item3 = new QTableWidgetItem(update_param);
    item4 = new QTableWidgetItem(update_param);
    item5 = new QTableWidgetItem(update_param);
    item6 = new QTableWidgetItem(update_param);
    item7 = new QTableWidgetItem(update_focus);

    // Autonomic mode
    ui->controlParameters->setItem(0, 0, item2);
    ui->controlParameters->item(0, 0)->setFlags(ui->controlParameters->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(0, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->controlParameters->item(0, 0)->setBackgroundColor(*colour);

    // Velocity
    ui->controlParameters->setItem(1, 0, item3);
    ui->controlParameters->item(1, 0)->setFlags(ui->controlParameters->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(1, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Angular Velocity
    ui->controlParameters->setItem(2, 0, item4);
    ui->controlParameters->item(2, 0)->setFlags(ui->controlParameters->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(2, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Height
    ui->controlParameters->setItem(3, 0, item5);
    ui->controlParameters->item(3, 0)->setFlags(ui->controlParameters->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(3, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Torque
    ui->controlParameters->setItem(4, 0, item6);
    ui->controlParameters->item(4, 0)->setFlags(ui->controlParameters->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(4, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Focus
    ui->controlParameters->setItem(5, 0, item7);
    ui->controlParameters->item(5, 0)->setFlags(ui->controlParameters->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(5, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Show in status log
    ui->status_log->clear();
    ui->status_log->addItem("Disconnected! (by user)");
    ui->status_log->item(0)->setFlags(ui->status_log->item(0)->flags() ^ Qt::ItemIsSelectable ^ Qt::ItemIsEditable);
    ui->status_log->scrollToBottom();

    // Stop moving the robot before disconnect
    front = false;
    rear = false;
    left = false;
    right = false;
    rot_left = false;
    rot_right = false;

    // Reset graphs
    x_limit = 100;
    ui->plot->graph(0)->clearData();
    ui->plot->graph(1)->clearData();
    ui->plot->graph(2)->clearData();
    ui->plot->graph(3)->clearData();
    ui->plot->graph(4)->clearData();
    ui->plot->xAxis->setRange(0, x_limit);
    ui->plot->replot();
    x_axis = 0;

    // Set global variable
    connected = false;

    // If we're still connected, tell robot to stop before disconnecting
    if(btclient->isConnected){
        sendData(setMovement(front, rear, left, right, rot_left, rot_right));

        // Update control parameters and make sure everything is turned to default settings
        if(ui->controlParameters->item(0, 0)->text() == "ON"){
            sendData(autoMode(false));
            autonomic = false;
        }

        btclient->disconnectFromHerm();
    }

    // Update current "Bluetooth"-tab
    ui->connection->setCurrentIndex(0);
    ui->connection->insertTab(0, ui->tab_3, "Bluetooth");
    ui->connection->removeTab(1);

    ui->startLabel->setText("Start scanning for Hexa H.E.R.M.");
    ui->button_connect->setEnabled(false);
    ui->button_scan->setEnabled(true);

    file.close();
}

/**
 * Toggle autonomic mode on H.E.X.A Herm.
 * Sends a command accordingly.
 */
void MainWindow::on_button_auto_clicked()
{
    QString update;
    QTableWidgetItem *item;
    QColor *colour;

    // if AUTOMODE is OFF, turn it ON.
    if(ui->controlParameters->item(0, 0)->text() == "OFF"){
        // Enable autonomic mode on robot.
        sendData(autoMode(true));
        autonomic = true;
        update = "ON";
        colour = new QColor(125, 255, 125, 255);
    // else do otherwise.
    } else {
        // Disable autonomic mode on robot.
        sendData(autoMode(false));
        autonomic = false;
        update = "OFF";
        colour = new QColor(255, 125, 125, 255);
        sendData(setMovement(false, false, false, false, false, false));
    }

    // Update control parameter
    item = new QTableWidgetItem(update);
    ui->controlParameters->setItem(0, 0, item);
    ui->controlParameters->item(0, 0)->setFlags(ui->controlParameters->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
    ui->controlParameters->item(0, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->controlParameters->item(0, 0)->setBackgroundColor(*colour);
}

/**
 * Move those hips Hermie!
 */
void MainWindow::on_button_dance_clicked()
{
    vector<QString> names;
    vector<QJsonValue> values;

    QString name = "lets_dance";
    QJsonValue val = true;

    // Setup JsonObject
    names.push_back(name);
    values.push_back(val);

    sendData(createJsonObject(names, values));
}

/**
 * Detect if any movement key was pressed
 * and if so, send the right command.
 * @param event the key that was pressed.
 */
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    /*
     * Only handle keyEvents regarding movement if the robot
     * is turned on, and autonomic mode is off.
     * Will only register a keyPress once when holding a button.
     */
    if(!event->isAutoRepeat() && !autonomic && movement){
        // 8 possible combinations -> 8 possible directions
        if(event->key() == Qt::Key_Up || event->key() == Qt::Key_W) front = true;
        if(event->key() == Qt::Key_Down || event->key() == Qt::Key_S) rear = true;
        if(event->key() == Qt::Key_Left || event->key() == Qt::Key_A) left = true;
        if(event->key() == Qt::Key_Right || event->key() == Qt::Key_D) right = true;
        if(event->key() == Qt::Key_Q) rot_left = true;
        if(event->key() == Qt::Key_E) rot_right = true;
        sendData(setMovement(front, rear, left, right, rot_left, rot_right));
    }
}

/**
 * Detect if any movement key was released
 * and if so, send the right command.
 * @param event the key that was released.
 */
void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    // See comments for keyPressEvent above.
    if(!event->isAutoRepeat() && !autonomic && movement){
        if(event->key() == Qt::Key_Up || event->key() == Qt::Key_W) front = false;
        if(event->key() == Qt::Key_Down || event->key() == Qt::Key_S) rear = false;
        if(event->key() == Qt::Key_Left || event->key() == Qt::Key_A) left = false;
        if(event->key() == Qt::Key_Right || event->key() == Qt::Key_D) right = false;
        if(event->key() == Qt::Key_Q) rot_left = false;
        if(event->key() == Qt::Key_E) rot_right = false;
        sendData(setMovement(front, rear, left, right, rot_left, rot_right));
    }
}

/**
 * If user wants to keep log file, don't delete it.
 */
void MainWindow::on_log_yes_clicked()
{
    // Update current "Bluetooth"-tab
    ui->connection->setCurrentIndex(0);
    ui->connection->insertTab(0, ui->tab_1, "Bluetooth");
    ui->connection->removeTab(1);

    reset_parameters();
}

/**
 * If user doesn't want to keep log file, delete it.
 */
void MainWindow::on_log_no_clicked()
{
    // Update current "Bluetooth"-tab
    ui->connection->setCurrentIndex(0);
    ui->connection->insertTab(0, ui->tab_1, "Bluetooth");
    ui->connection->removeTab(1);

    reset_parameters();

    // And delete the log-file from last session
    file.remove();
}

/**
 * Updates the data on screen with the new
 * data received from bluetooth connection.
 * @param input the received data.
 */
void MainWindow::sortData(const QString input)
{
    /**
     * Creates a JsonDocument and JsonObject
     * out of the received data.
     */
    QByteArray bytearray = input.toUtf8();
    QJsonDocument jDoc = QJsonDocument::fromJson(bytearray);
    QJsonObject jObj = jDoc.object();
    QString strJson(jDoc.toJson(QJsonDocument::Indented));

    // For debugging
    cout << strJson.toStdString() << endl;
    //if (jObj.take("timestamp") == QJsonValue::Undefined) return;

    // Take the value from the variables in the JsonObject...
    QJsonValue d_omega = jObj.take("d_omega");
    QJsonValue dx = jObj.take("dx");
    QJsonValue dy = jObj.take("dy");
    QJsonValue tick_rate = jObj.take("tick_rate");
    QJsonValue timestamp = jObj.take("timestamp");

    if (timestamp == QJsonValue::Undefined) return;

    // ...and create widget items with the values as parameters...
    QTableWidgetItem *item_d_omega = new QTableWidgetItem(QString::number(d_omega.toDouble()));
    QTableWidgetItem *item_dx = new QTableWidgetItem(QString::number(dx.toDouble()));
    QTableWidgetItem *item_dy = new QTableWidgetItem(QString::number(dy.toDouble()));
    QTableWidgetItem *item_tick_rate = new QTableWidgetItem(QString::number(tick_rate.toDouble()));
    QTableWidgetItem *item_timestamp = new QTableWidgetItem(timestamp.toString());

    // ...and view them in the widget.
    ui->telemetry->setItem(0, 0, item_d_omega);
    ui->telemetry->item(0, 0)->setFlags(ui->telemetry->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->telemetry->setItem(1, 0, item_dx);
    ui->telemetry->item(1, 0)->setFlags(ui->telemetry->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->telemetry->setItem(2, 0, item_dy);
    ui->telemetry->item(2, 0)->setFlags(ui->telemetry->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->telemetry->setItem(3, 0, item_tick_rate);
    ui->telemetry->item(3, 0)->setFlags(ui->telemetry->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->telemetry->setItem(4, 0, item_timestamp);
    ui->telemetry->item(4, 0)->setFlags(ui->telemetry->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    // Same process as above, but for sensors
    QJsonValue s_front = jObj.take("front");
    QJsonValue s_gyro = jObj.take("gyro");
    QJsonValue s_sides = jObj.take("sides");

    QJsonArray array_sides = s_sides.toArray();

    QTableWidgetItem *item_gyro = new QTableWidgetItem(QString::number(s_gyro.toDouble()));
    QTableWidgetItem *item_front = new QTableWidgetItem(QString::number(s_front.toDouble()));
    QTableWidgetItem *item_rightfront = new QTableWidgetItem(QString::number(array_sides[0].toDouble()));
    QTableWidgetItem *item_leftfront = new QTableWidgetItem(QString::number(array_sides[1].toDouble()));
    QTableWidgetItem *item_rightback = new QTableWidgetItem(QString::number(array_sides[2].toDouble()));
    QTableWidgetItem *item_leftback = new QTableWidgetItem(QString::number(array_sides[3].toDouble()));

    ui->sensors->setItem(0, 0, item_front);
    ui->sensors->item(0, 0)->setFlags(ui->sensors->item(0, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->sensors->setItem(1, 0, item_rightfront);
    ui->sensors->item(1, 0)->setFlags(ui->sensors->item(1, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->sensors->setItem(2, 0, item_leftfront);
    ui->sensors->item(2, 0)->setFlags(ui->sensors->item(2, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->sensors->setItem(3, 0, item_rightback);
    ui->sensors->item(3, 0)->setFlags(ui->sensors->item(3, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->sensors->setItem(4, 0, item_leftback);
    ui->sensors->item(4, 0)->setFlags(ui->sensors->item(4, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    ui->sensors->setItem(5, 0, item_gyro);
    ui->sensors->item(5, 0)->setFlags(ui->sensors->item(5, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);

    update_graph(s_front.toDouble(), array_sides[0].toDouble(), array_sides[1].toDouble(), array_sides[2].toDouble(), array_sides[3].toDouble());

    // Update status and decision logs
    QJsonValue log = jObj.take("log");
    QJsonArray array_log = log.toArray();

    for(int i = 0; i < array_log.size(); i++){
        if(array_log[i].toString().contains("[ai]", Qt::CaseInsensitive)){
            ui->decision_log->addItem(array_log[i].toString());
        } else {
            ui->status_log->addItem(array_log[i].toString());
        }
    }
    ui->status_log->scrollToBottom();
    ui->decision_log->scrollToBottom();

    // The information that will be written to log.txt file
    QTextStream stream(&file);
    stream << "==================NEW===================" << endl;
    stream << "TIME: [" + item_timestamp->text() + "]" << endl;
    stream << "d_omega: " + item_d_omega->text() << endl;
    stream << "dx: " + item_dx->text() << endl;
    stream << "dy: " + item_dy->text() << endl;
    stream << "tick_rate: " + item_tick_rate->text() << endl;
    stream << "==============SENSORVALUES==============" << endl;
    stream << "front: " + item_front->text() << endl;
    stream << "gyro: " + item_gyro->text() << endl;
    stream << "rightfront: " + item_rightfront->text() << endl;
    stream << "leftfront: " + item_leftfront->text() << endl;
    stream << "rightback: " + item_rightback->text() << endl;
    stream << "leftback: " + item_leftback->text() << endl;
    stream << "==================END===================" << endl;
}

/**
 * Sends the QString which is in QJsonDocument-format.
 * @param data to be sent.
 */
void MainWindow::sendData(QString data)
{
    qDebug() << data;
    btclient->send(data);
}

/**
 * Scan for Hexa H.E.R.M.
 */
void MainWindow::on_button_scan_clicked()
{
    btclient->scan();
    ui->startLabel->setText("Scanning for Hexa H.E.R.M...");
    ui->button_scan->setEnabled(false);
    ui->button_connect->setEnabled(false);
}

/**
 * If Hexa H.E.R.M. was found, enable connect-button.
 * @param result boolean which is true if Hexa H.E.R.M. was found
 * and false if otherwise.
 */
void MainWindow::update_scan(bool result)
{

    ui->button_scan->setEnabled(true);

    if(result){
        ui->startLabel->setText("Hexa H.E.R.M. found!");
        ui->button_connect->setEnabled(true);
    } else {
        ui->startLabel->setText("No Hexa H.E.R.M. was found... try again!");
    }

    //ui->startLabel->setText("Hexa H.E.R.M. found!");
    //ui->button_connect->setEnabled(true);
}

/**
 * Adds another iteration of data to the graphs.
 * @param distance_front value from front sensor.
 * @param distance_rfront value from right_front sensor.
 * @param distance_lfront value from left_front sensor.
 * @param distance_rback value from right_back sensor.
 * @param distance_lback value from left_back sensor.
 */
void MainWindow::update_graph(double distance_front, double distance_rfront, double distance_lfront, double distance_rback, double distance_lback)
{
    ui->plot->graph(0)->addData(x_axis, distance_front);
    ui->plot->graph(1)->addData(x_axis, distance_rfront);
    ui->plot->graph(2)->addData(x_axis, distance_lfront);
    ui->plot->graph(3)->addData(x_axis, distance_rback);
    ui->plot->graph(4)->addData(x_axis, distance_lback);
    ui->plot->replot();
    x_axis++;

    // Update xAxis ranges so the newest data is somewhat centralized
    if(x_limit - 50 < x_axis){
        x_limit += 10;
        ui->plot->xAxis->setRange(x_limit-100, x_limit);
    }
}

/**
 * Help-function for resetting controlparameters
 * to avoid the problem where window won't update.
 */
void MainWindow::reset_parameters()
{
    QString update_param = "DEF";
    // Reset Controlparameter2 and Controlparameter3
    for(int i = 0; i < 6; i++){
        QTableWidgetItem *testitem1 = new QTableWidgetItem(update_param);
        QTableWidgetItem *testitem2 = new QTableWidgetItem(update_param);

        ui->controlParameters2->setItem(i, 0, testitem1);
        ui->controlParameters3->setItem(i, 0, testitem2);
        ui->controlParameters2->item(i, 0)->setFlags(ui->controlParameters2->item(i, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
        ui->controlParameters3->item(i, 0)->setFlags(ui->controlParameters3->item(i, 0)->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
        ui->controlParameters2->item(i, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->controlParameters3->item(i, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    }
}
