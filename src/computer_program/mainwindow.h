#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QKeyEvent>
#include <iostream>
#include <vector>
#include <string>
#include <QDebug>
#include <bluetoothclient.h>
#include <qcustomplot.h>

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    Ui::MainWindow *ui;
    BluetoothClient * btclient;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

private slots:
    void on_prompt_editingFinished();
    void on_button_connect_clicked();
    void connected_window();
    void on_button_disconnect_clicked();
    void on_button_auto_clicked();
    void on_prompt_textChanged(const QString &arg1);
    void on_log_yes_clicked();
    void on_log_no_clicked();
    void sortData(const QString input);
    void sendData(QString data);
    void on_button_scan_clicked();
    void update_scan(bool result);
    void update_graph(double distance_front, double distance_rfront, double distance_lfront, double distance_rback, double distance_lback);
    void reset_parameters();

    void on_button_dance_clicked();

private:

    bool front = false;
    bool rear = false;
    bool left = false;
    bool right = false;
    bool movement = false;

    bool rot_left = false;
    bool rot_right = false;
    bool rotating = false;

    bool connected = false;

    bool autonomic = false;

    int x_axis = 0;
    int x_limit = 100;
    QVector<double> x1;
    QVector<double> y1;

    QString filename;
    QFile file;
};

void initializeCommands();
string extractParameter(string s);

#endif // MAINWINDOW_H
