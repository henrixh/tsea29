#include "commands.h"
#include <iostream>
#include "math.h"

using namespace std;

/**
 * Builds up a movement command depending on which buttons are
 * pressed/released.
 * @param up arrow-key up.
 * @param down arrow-key down.
 * @param left arrow-key left.
 * @param right arrow-key right.
 * @param rotation Q or E (or 0).
 */
QString setMovement(bool up, bool down, bool left, bool right, bool rot_left, bool rot_right)
{
    // Movement variables
    double movement_x = 0;
    double movement_y = 0;
    double rot_val = 0;

    if (left)   movement_y += 1;
    if (right)  movement_y -= 1;
    if (up)     movement_x += 1;
    if (down)   movement_x -= 1;
    if (rot_left)  rot_val -= 1;
    if (rot_right) rot_val += 1;

    double len = sqrt(movement_y * movement_y + movement_x * movement_x);
    if (len == 0) len = 1;

    // Temporary speed
    movement_x *= 200 / len;
    movement_y *= 200 / len;
    rot_val *= 0.8;

    // Create vectors for JsonObject
    vector<QString> names;
    vector<QJsonValue> values;

    names.push_back("movement_x");
    values.push_back(movement_x);
    names.push_back("movement_y");
    values.push_back(movement_y);
    names.push_back("rotation");
    values.push_back(rot_val);

    return createJsonObject(names, values);
}

/**
 * Build the command for autonomic mode.
 * @param x decides if it's ON or OFF.
 */
QString autoMode(bool x)
{
    // Toggle autonomic mode
    vector<QString> names;
    vector<QJsonValue> values;

    QString name = "autonomic";
    QJsonValue val;
    if(x){
        val = true;
    } else {
        val = false;
    }

    // Setup JsonObject
    names.push_back(name);
    values.push_back(val);
    return createJsonObject(names, values);
}

/**
 * Create a JsonObject for the new command.
 * @param names list of command-names.
 * @param values list of command-values.
 */
QString createJsonObject(vector<QString> names, vector<QJsonValue> values)
{
    // Create new JsonObject
    QJsonObject *obj = new QJsonObject();

    for(int i = 0; i < names.size(); i++){
        obj->insert(names[i], values[i]);
    }

    return sendData(obj);
}

/**
 * Build a QJsonDocument and send it in QString-format.
 * @param obj QJsonObject created in previous step.
 */
QString sendData(QJsonObject *obj)
{
    // Create new JsonDocument
    QJsonDocument *doc = new QJsonDocument(*obj);
    QString strJson(doc->toJson(QJsonDocument::Indented));

    delete doc;
    delete obj;

    // Returns JSON-doc in QString-format
    return strJson;
}








