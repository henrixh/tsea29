#ifndef BLUETOOTHCLIENT_H
#define BLUETOOTHCLIENT_H

#include <QtCore/QObject>

#include <QBluetoothLocalDevice>
#include <QtDebug>
#include <QBluetoothSocket>
#include <QBluetoothAddress>
#include <QBluetoothServiceInfo>
#include <QBluetoothServiceDiscoveryAgent>
#include <QString>
#include <QBluetoothUuid>

QT_FORWARD_DECLARE_CLASS(QBluetoothSocket)

QT_USE_NAMESPACE

class BluetoothClient : public QObject
{
  Q_OBJECT

public:
  BluetoothClient ();
  ~BluetoothClient();
  QList<QBluetoothServiceInfo> hexaherms;
  void scan ();
  void connectToHerm (QBluetoothServiceInfo &hexaherm);
  void disconnectFromHerm();
  void send (QString &data);
  QString read ();
  bool isConnected;

private:
  QBluetoothServiceDiscoveryAgent * discoveryAgent;
  const char * uuidstring;
  QBluetoothUuid * uuid;
  QBluetoothSocket * sock;
  QBluetoothServiceInfo * currentHexaHerm;

private slots:
  void connectionEstablished ();
  void disconnectionEstablished ();
  void dataRecieved ();
  void scanComplete ();

signals:
  void gotData ();
  void connected ();
  void updateWindow ();
  void disconnected ();
  void scanned (bool);

  void newData (const QString);
};

#endif // BLUETOOTHRECEIVER_H
