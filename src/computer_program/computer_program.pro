#-------------------------------------------------
#
# Project created by QtCreator 2015-11-02T12:34:10
#
#-------------------------------------------------

QT       += core gui bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = computer_program
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        qcustomplot.cpp \
        bluetoothclient.cpp \
        commands.cpp
HEADERS  += mainwindow.h \
        qcustomplot.h \
        bluetoothclient.h \
        commands.h

FORMS    += mainwindow.ui
