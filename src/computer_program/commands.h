#ifndef COMMANDS_H
#define COMMANDS_H

#include "string"
#include "QJsonArray"
#include "QJsonObject"
#include "QString"
#include "QPair"
#include "QJsonValue"
#include "QJsonDocument"
#include "mainwindow.h"

using namespace std;

QString setMovement(bool up, bool down, bool left, bool right, bool rot_left, bool rot_right);
QString autoMode(bool x);
QString createJsonObject(vector<QString> name, vector<QJsonValue> val);
QString sendData(QJsonObject *obj);
#endif // COMMANDS_H

