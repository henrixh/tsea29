import threading
## @package threadlaunch
# Support for the creation of threads

## A thread in control of a handler instance
# The thread allows the handler to run solely inside of the thread
# itself. The thread is terminated once its handler is terminated.
class HexaThread(threading.Thread):
    
    ## Initialize HexaThread
    # @param handler The handler instance that the thread is in charge of
    def __init__(self, handler):
        threading.Thread.__init__(self)
        self._handler = handler

    ## Run the handler belonging to the thread
    def run(self):
        self._handler.run()


## Creates a new thread and puts the handler inside of it
# @param handler The handler to be put inside a thread
def launchHandler(handler):
    thread = HexaThread (handler)
    thread.start()
    return thread

## Creates a multitude of new threads for each handler
# @param handler The handlers to be put inside unique threads
def launchHandlers(handlers):
    return map(launchHandler, handlers)
