import time, serial, struct
## @package sensordata
# Communication between the beagleboard and the sensor module

## Handles all sensor data sent from the sensor module
class SensorDataHandler:

    sensorUnit = None

    ## Initialize SensorData
    # @param ai_parameter_send Pipe for outgoing sensor information data
    # @param telemetry_send Pipe for outgoing sensor information data
    def __init__(self, id, ai_parameter_send, telemetry_send):
        self.id = id
        self._ai_parameter_send = lambda data : ai_parameter_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)

        self.previous_front = None
        self.front_error_count = -1
        
        self.sensorUnit = None
        try:
            self.sensorUnit = serial.Serial('/dev/ttyUSB1', 76800, timeout = 0.1)
        except Exception as error:
            message = "[LegCommunicationHandler] WARNING: Serial connection failed to initiate: {}".format(error)
            self._telemetry_send(message)


    ## Run the SensorDataHandler
    # Start running the loop for reading data from sensors. Read and forward
    # sensor data to relevant recievers.
    # 
    # Should run in its own thread
    def run(self):
        if self.sensorUnit:
            self.readData()

    ## Calculates voltage from raw data
    # @ param ADC_number for the binary data from AD-converter
    def vinCalculation(self, ADC_number):
        vref      = 5
        divFactor = 1024
        vin = ((ADC_number*vref)/divFactor)
        return vin

    # Convert front-sensordata into centimeters
    def convert_short_to_cm (self, x):
        a = -3.8158e-6
        b = 0.00401362
        c = -1.3567
        d = 169.681
        return a*x*x*x + b*x*x + c*x + d

    # Convert side-sensordata into centimeters
    def convert_long_to_cm (self, x):
        a = -4.9729e-6
        b = 0.00527358
        c = -1.92279
        d = 274.853
        return a*x*x*x + b*x*x + c*x + d

    # Cheap and optional solution to error in front sensor sending false data
    def fix_front_error(self, value):
        if self.front_error_count >= 0:
            self.front_error_count += 1
        if self.front_error_count >= 63:
            self.front_error_count = 0
        if 0 <= self.front_error_count < 5:
            return self.previous_front
        if self.front_error_count == 5:
            self.previous_front = None
        if not self.previous_front:
            self.previous_front = value
            return value
        elif value < self.previous_front / 2 and self.front_error_count == -1:
            self.front_error_count = 0
            return self.previous_front
        return value
    
    ## Calculate angular rate
    # V = bias + gain*angularRate
    # ADC_number = (Vin*1024)/Vref
    # @param ADC_number for incoming raw data
    def angularRate(self, ADC_number):
        angRate   = 0.0
        vref      = 6
        divFactor = 1024.0
        bias      = 2.5
        gain      = 0.0067
        offset    = -0.8
        limit     = 90

        #ADC_number -= 4
        angRate = ( (ADC_number*vref)/divFactor - bias) /gain - offset
        if abs(angRate) > limit:
            return 0
        return angRate

    ## Sync read to chksum
    # @ param dev for the serial signal from USB-port (????)
    # @ param byte for magic
    # @ param packetSize for size of protocol being recived
    def syncUart(self, byte, packetSize):
        while True:
            a = self.sensorUnit.read(1)
            if (a != "" and struct.unpack("B", a)[0] == byte):
                break
        packet = self.sensorUnit.read(packetSize - 1)

    ## Compare checksums
    # @return - True or False if the checksums matched.
    def checkSum(self, data, length):
        checksum = data[0] + data[2]
        for i in range(3, length):
            checksum += data[i] & 0x00FF
            checksum += data[i] >> 8
        if checksum & 0xFFFF == data[1]:
            return True
        return False

    ## Read protocol data from sensor unit
    def readData(self):
        read = False
        while(1):
            cts = self.sensorUnit.getCTS()
            if not cts:
                read = False
                time.sleep(0.1)
            elif not read:
                self.sensorUnit.flushInput()
                self.syncUart(0xac, 24)
                read = True
            elif read:
                try:
                    data = list(struct.unpack("<BHB" + "H" * 10, self.sensorUnit.read(24)))
                    if self.checkSum(data, 13):
                        autobutton = data[2]
                        if autobutton:
                            self.previous_front = None
                            self.front_error_count = -1
                        
                        gyroData = self.angularRate(sorted(data[3:8])[2])

                        # Convert side sensors
                        for i in range (9,13):
                            data[i] = self.convert_long_to_cm (data[i])

                        # Convert front sensor
                        data[8] = self.convert_short_to_cm (data[8])

                        data[8] = self.fix_front_error(data[8])

                        # Send to ai
                        sensor_values = {"gyro": gyroData, "sides":data[9:13], "front":data[8], "autobutton":autobutton}
                        self._ai_parameter_send(sensor_values)
                        self._telemetry_send(sensor_values)
                    else:
                        self.syncUart(0xAC, 24)
                except Exception as error:
                    self._telemetry_send ("Timeout on sensordata")
                    print "Failed to read data from sensor unit: {}".format(error)
