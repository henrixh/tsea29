import math

## Simple class providing an abstraction for PID controlling
class PIDController:
    def __init__ (self):
        self.k_p = 0.0
        self.k_i = 0.0
        self.k_d = 0.0
        self.scale = 0.0
        self.reset()

    ## Reset internal data
    # Will reset all internal data, except the control parameters k_* and the
    # scale.
    def reset (self):
        self._p = 0.0
        self._i = 0.0
        self._d = 0.0

    ## Add a new error to the modell and return the new correction.
    # @param e float absolute error
    # @param dt float time since last error was reported
    # @return float correction that should be applied
    def correct (self, e, dt):
        self._d = (self._p - e) / dt
        self._p = e
        self._i += e * dt

        return self.get_correct ()

    ## Get the current correction
    # @return float correction that should be applied
    def get_correct (self):
        v =  self.k_d * self._d
        v += self.k_p * self._p
        v += self.k_i * self._i
        v *= self.scale

        return v
