## List of constants for sensor placement and evaluations
# All constants are in centimeter

# Constant for maze area size
area_size = 80.

# Constants for the robot's sensor placement

sensor_side_x = 7.
sensor_side_y = 8.
sensor_front_y = 9.
sensor_side_width = sensor_side_y * 2.

# Constants for error handling and estimation
value_front_short = area_size/2 - sensor_front_y + 2
value_front_long = 80
offset_front = 9
front_discard_limit = 1.5 * area_size

value_side_short = area_size/2 - sensor_side_x + 3
value_side_medium = 3*area_size/2 - sensor_side_x
value_side_long = 140
offset_side = 12
side_discard_limit = 26

offset_rotation = 0 #4

# Movement distances to reach center or an entire block
movement_length_center_forward = area_size/2 - sensor_side_y - 12
movement_length_center_sideway = area_size/2 - 12
movement_length_block = area_size

# Constants for fuzzy distances
SHORT      = '    Short'
MEDIUM     = '   Medium'
LONG       = '     Long'
NEAR_SHORT = 'NearShort'
NEAR_LONG  = ' NearLong'
TOO_SHORT  = ' TooShort'

# Constants for modes
mode_evaluate_crossing = "Evaluating crossing"
mode_move_forward = "Moving forward aimlessly"
mode_move_sideway = "Moving sideway aimlessly"

mode_step_rotate = "Rotating a specific amount"
mode_step_forward = "Moving forward a specific amount"
mode_step_sideway = "Moving sideway a specific amount"

mode_nothing = None
