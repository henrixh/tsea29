import math
import numpy as np
from pidcontroller import PIDController
from constants import *

## Control aspects of the robots movement
#
# Make it not hit any walls.
class Controller:
    ## Initiate the controller
    #
    # @param v_speed float the base movement speed
    # @param omega_speed the base rotation speed
    #
    # Note that the controller may make the robot move faster than the above
    # parameters, they are just the base values.
    def __init__(self, v_speed, omega_speed):
        self.kP = 0
        self.kD = 0
        self.target = 0
        self.lastError = 0
        self.dt = 1/40.

        self.front_c = PIDController ()
        self.v_c     = PIDController ()
        self.omega_c = PIDController ()

        self.front_c.k_p = 5
        self.front_c.k_d = 0
        self.front_c.k_i = 0
        self.front_c.scale = 0.5

        self.v_c.k_p = 3
        self.v_c.k_d = 0
        self.v_c.k_i = 1
        self.v_c.scale = 1.0

        self.omega_c.k_p = 4
        self.omega_c.k_d = 0
        self.omega_c.k_i = 1
        self.omega_c.scale = 2

        self.v_speed = v_speed
        self.omega_speed = omega_speed


    ## Set named internal data from raw data
    #
    # @param sensors list of raw sensor data. len => 6
    # @param fuzzy_distances list of fuzzy sensor values. len => 6
    def set_data (self, sensors, fuzzy_distances):
        # Setup sensor values
        self.s_gyro        = sensors[0] # deg/sec
        self.s_right_front = sensors[1] # 20-150mm
        self.s_left_front  = sensors[2] # 20-150mm
        self.s_right_back  = sensors[3] # 20-150mm
        self.s_left_back   = sensors[4] # 20-150mm
        self.s_front       = sensors[5] # 10-80mm

        # Setup target values
        self.f_gyro        = fuzzy_distances[0] # deg/sec
        self.f_right_front = fuzzy_distances[1] # short medium long
        self.f_left_front  = fuzzy_distances[2] # short medium long
        self.f_right_back  = fuzzy_distances[3] # short medium long
        self.f_left_back   = fuzzy_distances[4] # short medium long
        self.f_front       = fuzzy_distances[5] # short long

    ## Calculate a decent movement from the parameters given by the AI.
    #
    # @param movement (x,y,omega) where $x,y,omega \in [-1,0,1], x+y+omega \leq 1$
    # @param sensors list of raw sensor data. len => 6
    # @param fuzzy_distances list of fuzzy sensor values. len => 6
    def get_next_movement(self, movement, sensors, fuzzy_distances):
        target_dx      = movement[0] # sideward
        target_dy      = movement[1] # forward
        target_d_omega = movement[2] # rotation rad/sec

        self.set_data (sensors, fuzzy_distances)


        # Regulate rotation in place
        if target_d_omega != 0.0:

            # Reset unused controllers
            self.front_c.reset()
            self.v_c.reset()
            self.omega_c.reset()

            # Regulate rotation in place
            # For now, just return
            return self.calculate_movement(
                0.,
                0.,
                target_d_omega * self.omega_speed)

        # Regulate crab walk
        if target_dx != 0:
            # Reset unused controllers
            self.v_c.reset()
            e_omega = self.angle_error ()
            omega = self.omega_c.correct (e_omega, self.dt)

            e_front = self.front_error ()
            front = self.front_c.correct (e_front, self.dt)
            return self.calculate_movement(
                target_dx * self.v_speed,
                front * self.v_speed,
                omega * self.omega_speed)

        if target_dy != 0:
            # Reset unused controllers
            self.front_c.reset()

            # Get absolute rotation error
            e_omega = self.angle_error ()
            e_v = self.v_error ()

            omega = self.omega_c.correct (e_omega, self.dt)
            v = self.v_c.correct (e_v, self.dt)

            # Return the new movement
            return self.calculate_movement(
                v * self.v_speed,
                target_dy * self.v_speed,
                omega * self.omega_speed)

        return self.calculate_movement(0,0,0)

    ## Calculate the rotational error.
    # @return float -1 < x < 1
    def angle_error (self):
        # Units in use are cm and radians
        a = 0.
        b = 0.
        n = 0.

        if self.f_left_front == SHORT and self.f_left_back == SHORT:
            a += self.s_left_back
            b += self.s_left_front
            n += 1.

        if self.f_right_front == SHORT and self.f_right_back == SHORT:
            a += self.s_right_front
            b += self.s_right_back
            n += 1.

        if n == 0.0:
            return 0

        c = (b - a) / n

        error = math.atan2 (c, sensor_side_width) / math.pi
        return error

    ## Calculate the error in sideway positioning
    # @return float -1 < x < 1
    def v_error (self):
        # Units in use are cm and radians
        left = 0.
        right = 0.
        nr = 0.
        nl = 0.

        optimum = value_side_short

        close_limit = value_side_medium + side_discard_limit

        if self.s_left_front <= close_limit:
            left += self.s_left_front % area_size
            nl += 1
        if self.s_left_back <= close_limit:
            left += self.s_left_back % area_size
            nl += 1
        if self.s_right_front <= close_limit:
            right += self.s_right_front % area_size
            nr += 1
        if self.s_right_back <= close_limit:
            right += self.s_right_back % area_size
            nr += 1

        left_error = 0
        right_error = 0
        n = 0.
        if nr != 0.0:
            right_error = -((right / nr) - optimum) / optimum
            n += 1.
        if nl != 0.0:
            left_error = ((left / nl) - optimum) / optimum
            n += 1.

        if n == 0.0:
            return 0

        return (right_error + left_error) / n
 
    ## Calculate the error in front positioning.
    # @return float -1 < x < 1
    def front_error (self):
        optimum = value_front_short
        if self.s_front > front_discard_limit:
            return 0
        return (self.s_front - optimum) / optimum


    ## Set movement according to speed
    # @param dx float
    # @param dy float
    # @param d_omega float
    # @return (float,float,float)
    def calculate_movement(self, dx, dy, d_omega):
        length = max(1, math.sqrt( dx**2 + dy**2 ) )
        dx *= self.v_speed / length
        dy *= self.v_speed / length
        return dx, dy, d_omega

    ## Set a new movement velocity for the robot
    # @param value the desired velocity
    def set_velocity(self, value):
        self.v_speed = float(value)

    ## Set a new rotation velocity for the robot
    # @param value the desired velocity
    def set_angular_velocity(self, value):
        self.omega_speed = float(value)
