import math
from constants import *
## @package AI
# Handles autonomous movement decisions based entirely on sensor
# values and time. The class processes sensor data and returns a
# desired movement.

class AI():

    def __init__(self, velocity, angular_velocity, send_decision_message, send_dance):
        self.dx = 0      # -1 0 1
        self.dy = 0      # -1 0 1
        self.d_omega = 0 # -1 0 1
        self.velocity = velocity
        self.angular_velocity = angular_velocity

        self.mode = mode_evaluate_crossing
        self.remaining_rotation = 0
        self.remaining_movement = 0

        self.dt = 1/40.

        self.send_decision_message = lambda message : send_decision_message('[AI] ' + message)
        self.send_decision_message("I am ready to roll! Evaluating my situation")
        self.send_dance = send_dance

        
    ## Main function to fix sensordata, decide and return suggested movement
    # @param [float] list of sensor values
    # @return (int, int, int), where all
    # values are -1, 0, 1, and the sum of is < 1.
    def get_next_movement(self, sensors):
        sensors = self.evaluate_sensors(sensors)
        self.make_decision(sensors)
        return self.calculate_movement(), sensors

    
    ## Decide what movement to make next based on simplified sensordata
    # @param [float] list of sensor values
    def make_decision(self, sensors):
        s_gyro        = sensors[0] # deg/sec
        s_right_front = sensors[1] # short medium long
        s_left_front  = sensors[2] # short medium long
        s_right_back  = sensors[3] # short medium long
        s_left_back   = sensors[4] # short medium long
        s_front       = sensors[5] # short long
        
        # Standing still
        if self.mode == mode_evaluate_crossing:
            # No walls anywhere. Victory!
            if s_front == s_right_front == s_right_back == s_left_front == s_left_back == LONG:
                self.send_decision_message('No walls anywhere. Victory!')
                self.set_movement(0, 0, 0)
                self.mode = mode_nothing
                self.send_dance()
            # Right side is open. Rotate right
            elif s_right_front == s_right_back == LONG:
                self.send_decision_message('Right side is open. Rotate there')
                self.set_movement(0, 0, -1)
                self.remaining_rotation = 90
                self.mode = mode_step_rotate
            # Left side is open. Rotate left
            elif s_left_front == s_left_back == LONG:
                self.send_decision_message('Left side is open. Rotate there')
                self.set_movement(0, 0, 1)
                self.remaining_rotation = 90
                self.mode = mode_step_rotate
            # Front is open. Go forward
            elif s_front == LONG:
                self.send_decision_message('Front is open. Go forward')
                self.set_movement(0, 1, 0)
                self.mode = mode_move_forward
            # Difficult crossing #4. Crab walk
            elif s_right_front == s_right_back == s_left_front == s_left_back == MEDIUM:
                self.send_decision_message("Difficult crossing #4. Crab walk")
                self.set_movement(1, 0, 0)
                self.mode = mode_move_sideway
            # Stuck in a dead end. Turn around
            elif s_right_front == s_right_back == s_left_front == s_left_back == SHORT:
                self.send_decision_message('Stuck in a dead end. Turn around')
                self.set_movement(0, 0, 1)
                self.remaining_rotation = 180
                self.mode = mode_step_rotate
            # Forward and left are blocked off. Rotate left
            elif s_right_front == s_right_back == SHORT:
                self.send_decision_message('Only available path is to the left. Rotate left')
                self.set_movement(0, 0, 1)
                self.remaining_rotation = 90
                self.mode = mode_step_rotate
            # Only available path is to the right. Rotate right
            elif s_left_front == s_left_back == SHORT:
                self.send_decision_message('Only available path is to the right. Rotate right')
                self.set_movement(0, 0, -1)
                self.remaining_rotation = 90
                self.mode = mode_step_rotate
            # Unsure what to do. Move forward
            elif s_front == NEAR_SHORT:
                self.send_decision_message('Unsure what to do. Move forward')
                self.set_movement(0, 1, 0)
                self.mode = mode_move_forward
            # Well, I am completely lost. Rotate left
            else:
                self.send_decision_message('Well, I am completely lost. Rotate left')
                self.set_movement(0, 0, 1)
                
        # Moving forward
        elif self.mode == mode_move_forward:
            # Wall right in front. Evaluate situation
            if s_front == SHORT:
                self.send_decision_message('Wall right in front. Evaluate situation')
                self.set_movement(0, 0, 0)
                self.mode = mode_evaluate_crossing
            # Right side is open. Step forward to center
            elif s_right_front == s_right_back == LONG:
                self.send_decision_message('Right side is open. Step forward to center')
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_center_forward
                self.mode = mode_step_forward
            # Left side is open. Step forward to center
            elif s_left_front == s_left_back == LONG:
                self.send_decision_message('Left side is open. Step forward to center')
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_center_forward
                self.mode = mode_step_forward
            # Right side is a dead end. Do nothing
            elif s_right_front == s_right_back == MEDIUM:
                pass
            # Left side is a dead end. Do nothing
            elif s_left_front == s_left_back == MEDIUM:
                pass

        # Moving sideways
        elif self.mode == mode_move_sideway:
            # Front is open. Step sideway to center
            if s_front == LONG:
                self.send_decision_message('Front is open. Step sideway to center')
                self.remaining_movement = movement_length_center_sideway
                self.mode = mode_step_sideway
            # Wall to the right. Go left
            elif s_right_front == s_right_back == SHORT:
                self.send_decision_message('Wall to the right. Go left')
                self.set_movement(1, 0, 0)
                self.mode = mode_move_sideway
            # Wall to the left. Go right
            elif s_left_front == s_left_back == SHORT:
                self.send_decision_message('Wall to the left. Go right')
                self.set_movement(-1, 0, 0)
                self.mode = mode_move_sideway
                
        # Step rotating
        elif self.mode == mode_step_rotate:
            self.remaining_rotation -= 1.25 * self.angular_velocity * self.dt * 180 / math.pi
            # Finished rotating. Evaluate situation
            if self.remaining_rotation <= 0:
                self.send_decision_message('Finished rotating. Step forward one block')
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_block
                self.mode = mode_step_forward

        # Step forward
        elif self.mode == mode_step_forward:
            self.remaining_movement -= abs(self.dy) * self.velocity / 10.0 * self.dt
            # Finished moving. Evaluate situation
            if self.remaining_movement <= 0:
                self.send_decision_message('Finished moving. Evaluate situation')
                self.set_movement(0, 0, 0)
                self.mode = mode_evaluate_crossing
            # Wall in front dangerously close. Evaluate situation
            if s_front == TOO_SHORT:
                self.send_decision_message('Finished moving. Evaluate situation')
                self.set_movement(0, 0, 0)
                self.mode = mode_evaluate_crossing

        # Step sideway
        elif self.mode == mode_step_sideway:
            self.remaining_movement -= abs(self.dx) * self.velocity / 10.0 * self.dt
            # Finished crabwalk. Step forward one block
            if self.remaining_movement <= 0:
                self.send_decision_message('Finished crabwalk. Step forward one block')
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_block
                self.mode = mode_step_forward
            # Wall to the left is dangerously close. Step forward one block
            if s_left_front == s_left_back == TOO_SHORT:
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_block
                self.mode = mode_step_forward
            # Wall to the right is dangerously close. Step forward one block
            if s_right_front == s_right_back == TOO_SHORT:
                self.set_movement(0, 1, 0)
                self.remaining_movement = movement_length_block
                self.mode = mode_step_forward

                
    ## Round and replace sensordata with evaluated distances for easier usage
    # @param [float] list of sensor values
    def evaluate_sensors(self, sensors):
        s_gyro        = sensors[0] # deg/sec
        s_right_front = sensors[1] # 20-150mm
        s_left_front  = sensors[2] # 20-150mm
        s_right_back  = sensors[3] # 20-150mm
        s_left_back   = sensors[4] # 20-150mm
        s_front       = sensors[5] # 10-80mm

        s_right_front = self.evaluate_distance_side(s_right_front)
        s_left_front  = self.evaluate_distance_side(s_left_front)
        s_right_back  = self.evaluate_distance_side(s_right_back)
        s_left_back   = self.evaluate_distance_side(s_left_back)
        s_front       = self.evaluate_distance_front(s_front)

        return s_gyro, s_right_front, s_left_front, s_right_back, s_left_back, s_front
    
    ## Evaluate which type of distance a front sensor has to a wall
    def evaluate_distance_front(self, value):
        if abs(value_front_short - value) <= offset_front:
            return SHORT
        if abs(value_front_long - value) <= offset_front:
            return LONG
        if value < value_front_short:
            return TOO_SHORT
        if value > value_front_long:
            return LONG
        return NEAR_SHORT

    ## Evaluate which type of distance a side sensor has to a wall
    # Preforms fuzzy matching of sensor values
    # @param value float
    # @return SHORT, MEDIUM, LONG, TO_SHORT, LONG, NEAR_SHORT, or NEAR_LONG
    def evaluate_distance_side(self, value):
        if abs(value_side_short - value) <= offset_side:
            return SHORT
        if abs(value_side_medium - value) <= 2*offset_side:
            return MEDIUM
        if abs(value_side_long - value) <= offset_side:
            return LONG
        if value < value_side_short:
            return TOO_SHORT
        if value > value_side_long:
            return LONG
        if value < value_side_medium:
            return NEAR_SHORT
        return NEAR_LONG

    ## Convert movement decisions to actual movement data with correct speed
    def calculate_movement(self):
        # On the other hand, nevermind
        return self.dx, self.dy, self.d_omega


    ## Set movement parameters 
    # @param dx float
    # @param dy float
    # @param d_omega float
    def set_movement(self, dx, dy, d_omega):
        self.dx = dx
        self.dy = dy
        self.d_omega = d_omega

    ## Set a new movement velocity for the robot
    # @param value num
    def set_velocity(self, value):
        self.velocity = float(value)

    ## Set a new rotation velocity for the robot
    # @param value num
    def set_angular_velocity(self, value):
        self.angular_velocity = float(value)
