#MG 2014 GameJam

import pygame, sys, math, time, random
from ai import AI
from controller import Controller

##################### Constants
# Useful constants
screen_width = 710
screen_height = 710
screen_fps = 200
dt = 1/40.0

# Colors
c_black = (0, 0, 0)
c_dkgray = (64, 64, 64)
c_gray = (128, 128, 128)
c_ltgray = (192, 192, 192)
c_white = (255, 255, 255)
c_red = (255, 0, 0)
c_orange = (255, 165, 0)
c_yellow = (255, 255, 0)
c_green = (0, 255, 0)
c_blue = (0, 0, 255)
c_cyan = (0, 255, 255)
c_purple = (225, 0, 225)
c_pink = (255, 181, 197)
c_brown = (165, 42, 42)
##################### IoHandler
class IOHandler():
    # Main pygame event handler
    def update(self):
        pygame.event.pump()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
##################### Maze
# -- Maze --
class Maze():
    def __init__(self):
        self.areas = []
        self.user_walls = []
        self.walls = []
        self.drawable_walls = None

    def add_area(self, x, y):
        self.areas += [(x,y)]
        self.calculate_walls()

    def add_wall(self, p1, p2):
        wall = [min(p1,p2), max(p1,p2)]
        self.user_walls.append(wall)
        
    def new_wall(self, p1, p2):
        wall = [min(p1,p2), max(p1,p2)]
        if wall in self.walls:
            self.walls.remove(wall)
        else:
            self.walls.append(wall)
        
    def calculate_walls(self):
        self.walls = []
        for x,y in self.areas:
            self.new_wall((x,y), (x+1,y))
            self.new_wall((x,y), (x,y+1))
            self.new_wall((x+1,y), (x+1,y+1))
            self.new_wall((x,y+1), (x+1,y+1))
        for p1,p2 in self.user_walls:
            self.new_wall(p1,p2)
        self.drawable_walls = []

    def get_drawable_areas(self):
        result = []
        for x,y in self.areas:
            result.append((resize(x), resize(y)))
        return result
            
    def get_walls(self):
        return self.walls
            
    def get_drawable_walls(self):
        self.drawable_walls = []
        for p1,p2 in self.walls:
            self.drawable_walls.append((map(resize,p1), map(resize,p2)))
        return self.drawable_walls

    def get_total_size(self):
        all_x = []
        all_y = []
        for p1,p2 in self.get_walls():
            all_x.append(p1[0])
            all_y.append(p1[1])
            all_x.append(p2[0])
            all_y.append(p2[1])
        x_range = min(all_x), max(all_x)
        y_range = min(all_y), max(all_y)
        return x_range, y_range
##################### Robot
# -- Robot --
class Robot():
    def __init__(self, sensors):
        self.x = 0
        self.y = 0
        self.angle = -math.pi/2
        self.p_angle = None
        self.sensors = sensors
        self.sensor_distances = []
        self.mess = 0
        self.sensor_mess = 0

    def set_start(self, x, y, angle):
        self.x = x + 0.5
        self.y = y + 0.5
        self.angle = angle * -math.pi/2
        
    def move(self, dx, dy):
        dx *= dt / 10.0 / 80.0
        dy *= dt / 10.0 / 80.0
        self.x += math.sin(math.pi+self.angle) * dx + math.cos(math.pi-self.angle) * dy
        self.y += math.cos(math.pi+self.angle) * dx + math.sin(math.pi-self.angle) * dy
        self.mess_up()
        
    def rotate(self, dr):
        dr *= dt
        self.p_angle = self.angle
        self.angle += dr
        self.mess_up()

    def mess_up(self):
        self.x += random.uniform(-self.mess, self.mess)
        self.y += random.uniform(-self.mess, self.mess)
        self.angle += random.uniform(-self.mess, self.mess)

    def get_all_sensors(self):
        return [self.get_gyro()] + self.sensor_distances
        
    def get_gyro(self):
        value = self.p_angle
        if value == None:
            return 0
        r = 1 + random.uniform(-self.sensor_mess, self.sensor_mess)
        return (r * math.degrees(value - self.angle))/dt
        
    def get_sensor_distances(self):
        return self.sensor_distances
        
    def get_sensor_collision(self, walls, ranges):
        self.sensor_distances = []

        x_to_check = range(ranges[1][0], ranges[1][1]+1)
        y_to_check = range(ranges[0][0], ranges[0][1]+1)
        
        sensor_points = []
        for px, py, angle, min_dist, max_dist in self.sensors:
            px = px/80.0
            py = py/80.0
            x = math.cos(math.pi-self.angle) * px + math.sin(math.pi+self.angle) * py
            y = math.sin(math.pi-self.angle) * px + math.cos(math.pi+self.angle) * py
            
            dx = math.cos(-(math.pi/2 + self.angle + angle))
            dy = math.sin(-(math.pi/2 + self.angle + angle))

            distance = []
            for wall_x in x_to_check:
                k = (wall_x - self.y - y) / (dy + 0.00001)
                k = min(10, max(0, k))
                if k > 0:
                    p2 = (self.x+x + k*dx, self.y+y + k*dy)
                    c_p = (round(p2[0],2), round(p2[1],2))
                    c_p = [(int(c_p[0]), int(c_p[1])), (int(c_p[0]+1), int(c_p[1]))]
                    if c_p in walls:
                        distance.append(k)
            for wall_y in y_to_check:
                k = (wall_y - self.x - x) / (dx + 0.00001)
                k = min(10, max(0, k))
                if k > 0:
                    p2 = (self.x+x + k*dx, self.y+y + k*dy)
                    c_p = (round(p2[0],2), round(p2[1],2))
                    c_p = [(int(c_p[0]), int(c_p[1])), (int(c_p[0]), int(c_p[1]+1))]
                    if c_p in walls:
                        distance.append(k)
            if distance:
                k = min(distance)
            else:
                k = 100

            p1 = (self.x+x, self.y+y)
            p2 = (self.x+x + k*dx, self.y+y + k*dy)
            distance = math.sqrt( (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 )
            r = 1 + random.uniform(-self.sensor_mess, self.sensor_mess)
            distance = max(min_dist/80.0, distance)
            distance = min(max_dist/80.0, distance)
            distance = 80 * r * distance
            self.sensor_distances.append(distance)
            sensor_points.append((p1, p2))
        return sensor_points
##################### Graphics
# -- Graphics --
class Graphics():
    def __init__(self, w, h, fps, title):
        # Window caption
        pygame.display.set_caption(title)
        self.screen = pygame.display.set_mode((w, h))

        # Fps
        self.clock = pygame.time.Clock()
        self.fps = fps
        self.msElapsed = 1
        self.fpsAVG = []
        self.step = 0
        self.timeA = time.time()
        self.timeB = 0
        self.timeAVG = []

        self.setBackground()
        self.font = pygame.font.SysFont("courier new", 12)

    # Refresh display and keep fps
    def update(self):
        self.fpsAVG.append(1000/self.msElapsed)
        self.fpsAVG = self.fpsAVG[-20:]
        self.timeB = time.time()
        self.timeAVG.append((self.timeB - self.timeA) * float(self.fps))
        self.timeAVG = self.timeAVG[-20:]
        
        self.msElapsed = self.clock.tick(self.fps)
        self.timeA = time.time()
        self.step += 1
        pygame.display.update()
    
    # Shift (rotated) object to its center
    def shift_center(self, pos, image):
        shift = (- image.get_width() / 2.0, - image.get_height() / 2.0)
        return map(sum, zip(pos, shift))
    
    # Prepare the background.
    def setBackground(self):
        # Drawing surfaces is cheap. Recreating the background every step is expensive.
        self.background = pygame.Surface((screen_width, screen_height))
        self.background.fill(c_black)

    # Draw background example
    def draw_background(self):
        self.screen.blit(self.background, (0,0))

    # Draw measured frames per second
    def draw_fps(self):
        r = 10
        pos = (screen_width-r, screen_height-r)
        s = str(int(100 * sum(self.timeAVG) / len(self.timeAVG))) + "%"
        text = self.font.render(s.upper(), True, c_white)
        rect = text.get_rect()
        rect.center = pos
        self.screen.blit(text, rect)

    def draw_robot(self, offset, robot, robot_radius, chassi, sensors):
        move = lambda x: (x[0]+offset[0], x[1]+offset[1])
        image = pygame.Surface((area_size, area_size), pygame.SRCALPHA)

        s = area_size / 80.0
        chassi = [(s*(40+x),s*(40+y)) for x,y in chassi]
        pygame.draw.circle(image, c_yellow, (area_size/2, area_size/2), int(robot_radius*s), 1)
        pygame.draw.lines(image, c_yellow, True, chassi, 1)
        
        sensors = [(int(s*(40+x)),int(s*(40-y))) for x,y,a,mi,ma in sensors]
        for sensor in sensors:
            pygame.draw.circle(image, c_red, sensor, 2, 0)
        
        image = pygame.transform.rotate(image, math.degrees(robot.angle))
        pos = (resize(robot.x), resize(robot.y))
        self.screen.blit(image, self.shift_center(move(pos), image))
        
        
    def draw_sensors(self, offset, sensors, distances, gyro):
        move = lambda x: (int(resize(x[0])+offset[0]), int(resize(x[1])+offset[1]))
        for p1,p2 in sensors:
            pygame.draw.circle(self.screen, c_red, move(p2), 3, 0)
            pointlist = [move(p1),move(p2)]
            pygame.draw.lines(self.screen, c_red, False, pointlist, 1)
            
            text = self.font.render(str(int(distances.pop(0))), True, c_white)
            self.screen.blit(text, self.shift_center(move(p1), text))
        text = self.font.render(str(int(gyro)), True, c_white)
        self.screen.blit(text, self.shift_center(move((robot.x,robot.y)), text))

    def draw_areas(self, offset, areas):
        o_x, o_y = offset
        for x,y in areas:
            rect = [x+o_x, y+o_y, area_size, area_size]
            pygame.draw.rect(self.background, (16,16,16), rect, 0)
            
    def draw_walls(self, offset, walls):
        move = lambda x: (x[0]+offset[0], x[1]+offset[1])
        for p1,p2 in walls:
            pointlist = [move(p1),move(p2)]
            pygame.draw.lines(self.background, c_white, False, pointlist, 2)
##################### Main

level = None
levels = ['basic1', 'basic2', 'basic3', 'cross1', 'cross2', 'cross3', 'cross4', 'regulate1','regulate2', 'regulate3', 'regulate4', 'regulate5', 'regulate6', 'regulate7', 'super1', 'super2', 'super3', 'super4', 'super5', 'tricky1', 'tricky2', 'tricky3', 'tricky4', 'tricky5', 'competition1']

if not sys.argv[1:]:
    print "python simulator.py <level>"
    for l in levels:
        print l
    sys.exit()
else:
    level = sys.argv[1]
    if level not in levels:
        for l in levels:
            print l
        sys.exit()        


chassi = [(-10,  0),
          ( -6, 12),
          (  6, 12),
          ( 10,  0),
          (  6,-12),
          ( -6,-12)]
sensors = [(-7, 8,-math.pi/2, 20, 150),
           ( 7, 8, math.pi/2, 20, 150),
           (-7,-8,-math.pi/2, 20, 150),
           ( 7,-8, math.pi/2, 20, 150),
           ( 0, 9, 0,         10, 80)]

robot_radius = 32
robot = Robot(sensors)


# Initialize pygame
pygame.init()

# Initialize template classes
graphics = Graphics(screen_width, screen_height, screen_fps, 'HexaHERM')
ioHandler = IOHandler()
    
maze = Maze()
    
if level == 'basic1':
    robot.set_start(0, 0, 2)
    maze.add_area(0,0)
    maze.add_area(0,1)
    maze.add_area(0,2)
    maze.add_area(0,3)
    maze.add_area(0,4)
    maze.add_wall((0,5), (1,5))

if level == 'basic2':
    robot.set_start(3, 0, 1)
    maze.add_area(0,0)
    maze.add_area(1,0)
    maze.add_area(2,0)
    maze.add_area(3,0)
    maze.add_area(4,0)
    maze.add_area(4,1)
    maze.add_area(4,2)
    maze.add_area(4,3)
    maze.add_area(4,4)
    maze.add_area(3,4)
    maze.add_area(2,4)
    maze.add_area(2,3)
    maze.add_area(2,2)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_area(0,3)
    maze.add_wall((0,4), (1,4))

if level == 'basic3':
    robot.set_start(0, 0, 2)
    maze.add_area(0,0)
    maze.add_area(0,1)
    maze.add_area(1,1)
    maze.add_area(1,2)
    maze.add_area(2,2)
    maze.add_area(2,3)
    maze.add_area(3,3)
    maze.add_wall((3,4), (4,4))
    
if level == 'cross1':
    robot.set_start(1, 2, 0)
    maze.add_area(1,2)
    maze.add_area(1,1)
    maze.add_area(0,1)
    maze.add_area(2,1)
    maze.add_area(3,1)
    maze.add_area(3,2)
    maze.add_area(3,0)
    maze.add_wall((3,0),(4,0))

if level == 'cross2':
    robot.set_start(0, 0, 1)
    maze.add_area(0,0)
    maze.add_area(1,0)
    maze.add_area(1,1)
    maze.add_area(2,0)
    maze.add_area(3,0)
    maze.add_area(3,1)
    maze.add_area(4,1)
    maze.add_area(3,2)
    maze.add_wall((3,3), (4,3))

if level == 'cross3':
    robot.set_start(3, 0, 3)
    maze.add_area(3,0)
    maze.add_area(2,0)
    maze.add_area(1,0)
    maze.add_area(2,1)
    maze.add_area(2,2)
    maze.add_area(2,3)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_wall((0,2), (0,3))
    
if level == 'cross4':
    robot.set_start(4, 3, 3)
    maze.add_area(4,3)
    maze.add_area(3,3)
    maze.add_area(3,4)
    maze.add_area(3,2)
    maze.add_area(2,2)
    maze.add_area(1,2)
    maze.add_area(1,1)
    maze.add_area(1,3)
    maze.add_area(0,3)
    maze.add_wall((0,3), (0,4))
    
if level == 'super1':
    robot.set_start(2, 3, 1)
    maze.add_area(2,3)
    maze.add_area(3,3)
    maze.add_area(3,4)
    maze.add_area(3,2)
    maze.add_area(4,2)
    maze.add_area(3,1)
    maze.add_area(3,0)
    maze.add_area(2,1)
    maze.add_area(1,1)
    maze.add_area(1,0)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_wall((0,2), (0,3))

if level == 'super2':
    robot.set_start(2, 4, 0)
    maze.add_area(2,4)
    maze.add_area(2,3)
    maze.add_area(3,3)
    maze.add_area(1,3)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_area(1,1)
    maze.add_area(1,0)
    maze.add_area(2,1)
    maze.add_area(3,1)
    maze.add_area(3,2)
    maze.add_wall((3,3), (4,3))
    maze.add_area(3,0)
    maze.add_wall((3,0), (4,0))

if level == 'super3':
    robot.set_start(6, 4, 2)
    maze.add_area(6,4)
    maze.add_area(6,5)
    maze.add_area(7,5)
    maze.add_area(6,6)
    maze.add_area(6,7)
    maze.add_area(7,7)
    maze.add_area(5,7)
    maze.add_area(4,7)
    maze.add_area(4,6)
    maze.add_area(4,5)
    maze.add_area(3,6)
    maze.add_area(2,6)
    maze.add_area(1,6)
    maze.add_area(2,5)
    maze.add_area(2,4)
    maze.add_area(1,4)
    maze.add_area(3,4)
    maze.add_area(3,3)
    maze.add_area(4,3)
    maze.add_area(3,2)
    maze.add_area(3,1)
    maze.add_area(4,1)
    maze.add_area(2,1)
    maze.add_area(1,1)
    maze.add_area(1,0)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_wall((0,2), (0,3))

if level == 'super4':
    robot.set_start(0, 6, 1)
    maze.add_area(0,6)
    maze.add_area(1,6)
    maze.add_area(1,7)
    maze.add_area(1,5)
    maze.add_area(2,5)
    maze.add_area(3,5)
    maze.add_area(3,6)
    maze.add_area(3,4)
    maze.add_area(4,4)
    maze.add_area(3,3)
    maze.add_area(3,2)
    maze.add_area(2,3)
    maze.add_area(1,3)
    maze.add_area(1,4)
    maze.add_wall((1,5), (2,5))
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_area(1,1)
    maze.add_area(1,0)
    maze.add_area(2,1)
    maze.add_area(3,1)
    maze.add_wall((3,2), (4,2))
    maze.add_area(4,1)
    maze.add_area(4,0)
    maze.add_area(4,2)
    maze.add_wall((4,2), (4,3))
    maze.add_area(5,2)
    maze.add_wall((6,2), (6,3))
    
if level == 'super5':
    robot.set_start(5, 3, 0)
    maze.add_area(5,3)
    maze.add_area(5,2)
    maze.add_area(6,2)
    maze.add_area(4,2)
    maze.add_area(4,1)
    maze.add_area(4,0)
    maze.add_area(2,1)
    maze.add_area(3,1)
    maze.add_area(1,0)
    maze.add_area(1,1)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_area(1,3)
    maze.add_area(1,4)
    maze.add_area(2,3)
    maze.add_area(3,3)
    #maze.add_wall((3,3), (4,3))
    #maze.add_wall((4,3), (4,4))
    maze.add_wall((5,4), (6,4))

if level == 'regulate1':
    robot.set_start(0, 0, 1)
    for i in range(15):
        maze.add_area(i,0)

if level == 'regulate2':
    robot.set_start(1, 0, 1)
    for i in range(7):
        maze.add_area(i,0)
        maze.add_area(i,6)
    for i in range(5):
        maze.add_area(0,1+i)
        maze.add_area(6,1+i)

if level == 'regulate3':
    robot.set_start(2, 1, 1)
    for i in range(6):
        maze.add_area(1+i,1)
        maze.add_area(i,5)
    for i in range(3):
        maze.add_area(1,2+i)
        maze.add_area(5,2+i)
    maze.add_area(5,6)
    maze.add_area(1,0)

if level == 'regulate4':
    robot.set_start(2, 1, 1)
    for i in range(6):
        maze.add_area(i,1)
        maze.add_area(1+i,5)
    for i in range(3):
        maze.add_area(1,2+i)
        maze.add_area(5,2+i)
    maze.add_area(5,0)
    maze.add_area(1,6)

if level == 'regulate5':
    robot.set_start(0, 1, 1)
    for i in range(10):
        maze.add_area(i,1)
    for i in range(4):
        maze.add_area(2*i+1,0)
        maze.add_area(2*i+2,2)

if level == 'regulate6':
    robot.set_start(0, 1, 1)
    for i in range(9):
        maze.add_area(i,1)
    for i in range(4):
        maze.add_area(2*i+1,0)
        maze.add_area(2*i+1,2)

if level == 'regulate7':
    robot.set_start(3, 1, 1)
    maze.add_area(3,1)
    maze.add_area(4,1)
    maze.add_area(4,0)
    maze.add_area(4,2)
    maze.add_area(5,2)
    maze.add_area(5,3)
    maze.add_area(5,4)
    maze.add_area(6,4)
    maze.add_area(4,4)
    maze.add_area(4,5)
    maze.add_area(3,5)
    maze.add_area(2,5)
    maze.add_area(2,6)
    maze.add_area(2,4)
    maze.add_area(1,4)
    maze.add_area(1,3)
    maze.add_area(1,2)
    maze.add_area(0,2)
    maze.add_area(2,2)
    maze.add_area(2,1)
    
if level == 'tricky1':
    robot.set_start(0, 0.1, 0.5)
    maze.add_area(0,0)
    maze.add_area(1,0)
    maze.add_area(2,0)
    maze.add_area(3,0)

if level == 'tricky2':
    robot.set_start(2, 2.87, 1.06)
    maze.add_area(0,2)
    maze.add_area(0,3)
    maze.add_area(0,1)
    maze.add_area(0,0)
    maze.add_area(0,4)
    maze.add_area(1,2)
    maze.add_area(2,2)
    maze.add_area(2,3)

if level == 'tricky3':
    robot.set_start(0, 0, 0)#.5)
    maze.add_area(0,0)

if level == 'tricky4':
    robot.set_start(2, 2, 2.4)
    maze.add_area(2,0)
    maze.add_area(2,1)
    maze.add_area(2,2)
    maze.add_area(2,3)
    maze.add_area(2,4)
    maze.add_area(0,2)
    maze.add_area(1,2)
    maze.add_area(3,2)
    maze.add_area(4,2)

if level == 'tricky5':
    robot.set_start(0.5, 0.5, 2)
    maze.add_area(0,0)
    maze.add_area(1,0)
    maze.add_area(0,1)
    maze.add_area(1,1)

if level == 'competition1':
    robot.set_start(0, 2, 0)
    maze.add_wall((0,2), (0,3))
    maze.add_area(0,2)
    maze.add_area(1,2)
    maze.add_area(2,2)
    maze.add_area(2,3)
    maze.add_area(2,1)
    maze.add_area(2,0)
    maze.add_area(3,1)
    maze.add_area(4,1)
    maze.add_area(4,0)
    maze.add_area(4,2)
    maze.add_area(5,2)
    maze.add_area(6,2)
    maze.add_area(5,3)
    maze.add_area(5,4)
    maze.add_area(5,5)
    maze.add_area(4,5)
    maze.add_area(3,5)
    maze.add_area(3,4)
    maze.add_area(2,5)
    maze.add_area(1,5)
    maze.add_area(1,4)
    maze.add_area(0,4)
    maze.add_wall((0,4), (0,5))
    
maze.calculate_walls()

xr,yr = maze.get_total_size()
area_size = max(screen_width, screen_height) / (max(xr[1]-xr[0], yr[1]-yr[0])+1)
resize = lambda x: area_size * x

w = screen_width/2 - area_size * (xr[0] + (xr[1]-xr[0])/2.0)
h = screen_height/2 - area_size * (yr[0] + (yr[1]-yr[0])/2.0)
screen_offset = (w,h)
graphics.draw_areas(screen_offset, maze.get_drawable_areas())
graphics.draw_walls(screen_offset, maze.get_drawable_walls())


def pri(m):
    print m
    
ai = AI(200, 0.8, pri)
pd = Controller(200, 0.8)

while True:

    #print len(maze.walls)
    #break
    # Basic
    graphics.update()
    ioHandler.update()
    
    robot.mess = 0.005
    robot.sensor_mess = 0.1

    robot.get_sensor_collision(maze.get_walls(), maze.get_total_size())
    movement, fuzzy = ai.get_next_movement(robot.get_all_sensors())
    dx, dy, d_omega = movement
    dx, dy, d_omega = pd.get_next_movement([dx,dy,d_omega], robot.get_all_sensors(), fuzzy)
    
    robot.move(dy, dx)
    robot.rotate(d_omega)
    
    # Draw
    graphics.draw_background()
    graphics.draw_fps()
    graphics.draw_robot(screen_offset, robot, robot_radius, chassi, sensors)
    graphics.draw_sensors(screen_offset, robot.get_sensor_collision(maze.get_walls(), maze.get_total_size()), robot.get_sensor_distances(), robot.get_gyro())
