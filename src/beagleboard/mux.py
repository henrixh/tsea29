## @package mux
# Routine for deciding what movement commands that are to be used

## Handles the passing of movement commands
# Incoming data for control decisions defines whether the robot is
# running in manual or autonomous mode. This is used to decide which
# incoming movement data that is to be sent to the walking algorithm.
class MuxHandler:
    autonomous = False
    
    ## Initialize MuxHandler
    # @param mux_data_recv Pipe for incoming movement decision data
    # @param walking_algorithm_send Pipe for outgoing movement decision data sent to walking algorithm
    # @param telemetry_send Pipe for outgoing log data
    def __init__(self, id, mux_data_recv, walking_algorithm_send, telemetry_send):
        self.id = id
        self._mux_data_recv = mux_data_recv
        self._walking_algorithm_send = lambda data : walking_algorithm_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)
    
    ## Run the MuxHandler
    # Start the main loop for reading incoming data and pass it on to the
    # walking algorithm. Control who controls the walking. Either we remote
    # control, or it's autonomous.
    #
    # Should run in its own thread
    def run(self):
        while True:                
            id, data = self._mux_data_recv ()
            if data == "STOP": break
            
            if id == -1: # empty data
                pass
            
            elif id == 4: # communication
                self.autonomous = data
                log = "[MuxHandler] Setting autonomous: {}".format(self.autonomous)
                self._walking_algorithm_send({"move":[0,0,0]})
                self._telemetry_send (log)
            
            elif id == 2: # ai
                tags = ["move", "tilt", "shift", "dance"]
                if any( [tag in data for tag in tags] ):
                    if self.autonomous:
                        self._walking_algorithm_send(data)
                if "autobutton" in data:
                    self.autonomous = not self.autonomous
                    log = "[MuxHandler] Setting autonomous: {}".format(self.autonomous)
                    self._walking_algorithm_send({"move":[0,0,0]})
                    self._walking_algorithm_send({"set_height":130})
                    self._telemetry_send (log)

            elif id == 5: # remote_control
                tags = ["set_height", "set_torque", "velocity", "angular_velocity"]
                if any( [tag in data for tag in tags] ):
                    self._walking_algorithm_send(data)
                else:
                    if not self.autonomous:
                        self._walking_algorithm_send(data)
                    else:
                        log = "[MuxHandler] Ignoring remote movement"
                        self._telemetry_send (log)

            else:
                error = "[MuxHandler] Received unknown data from {}: {}".format(id, data)
                self._telemetry_send (error)
