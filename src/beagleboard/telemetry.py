import time
import json

## @package telemetry
# Handle telemetry from other units

## Handles all telemetry data
#
# The handler can recieve either dictionaries or strings. Dictionaries will be
# saved away and sent off later, while strings will be added to a log and sent
# off in due time.
#
# Please note that keys are in a global namespace, so each user of this Handler
# must ensure it's not using any conflicting keys.
class TelemetryHandler:

    _telemetry_data = {
        "log" : []
    }
    _last_update = time.time ()
    _update_interval = 0.2

    ## Initialize TelemetryHandler
    # @param telemetry_recv Pipe for incoming telemetry data from any unit
    # @param computer_communication_send Pipe for outgoing telemetry data sent to the computer communication handler
    def __init__(self, id, telemetry_recv, computer_communication_send):
        self.id = id
        self._telemetry_recv = telemetry_recv
        self._computer_communication_send = lambda data : computer_communication_send(self.id, data)

    ## Run the TelemetryHandler
    #
    # Should run in its own thread.
    def run (self):
        while True:
            id, data = self._telemetry_recv()
            if data == "STOP": break

            if type (data) is str:
                self._telemetry_data["log"].append(data)
                print data
            else:
                for key, value in data.iteritems ():
                    self._telemetry_data [key] = value

            if time.time () - self._last_update >= self._update_interval:
                self._telemetry_data["timestamp"] = time.strftime("%H:%M:%S", time.gmtime())
                self._computer_communication_send(
                    json.dumps (self._telemetry_data))

                self._last_update = time.time ()

                # Reset the log.
                self._telemetry_data["log"] = []
