## @package legmodulereceiver
# Communication from the leg control module
import time

## Handles receiving serial data from the control module
class LegModuleReceiverHandler:
    running = True

    ## Initialize LegModuleReceiverHandler
    # @param serial_read Pipe for incoming data from the control module.
    # @param walking_algorithm_send Pipe for outgoing parametry data from the control module.
    # @param telemetry_send Pipe for outgoing parametry data from the control module.
    def __init__(self, id, serial_read, walking_algorithm_send, telemetry_send):
        self.id = id
        self._serial_read = serial_read
        self._walking_algorithm_send = lambda data : walking_algorithm_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)


    ## Run the LegModuleReceiverHandler
    #
    # Should run in it's own thread
    def run(self):
        while self.running:
            self._serial_read(self._telemetry_send)
