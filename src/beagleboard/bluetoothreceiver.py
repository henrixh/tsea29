import bluetooth, os, json
## @package bluetoothreceiver
# Communication from computer program

## Handles receiving data via bluetooth from the computer program
class BluetoothReceiverHandler:
    _server_sock = None
    _client_sock = None
    _client_info = None
    
    ## Initialize BluetoothReceiverHandler
    # @param computer_communication_send Pipe for outgoing data to the communication handler
    # @param telemetry_send Pipe for outgoing log messages
    def __init__(self, id, computer_communication_send, telemetry_send):
        self.id = id
        self._computer_communication_send = lambda data : computer_communication_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)

        
    ## Run the BluetoothReceiverHandler
    # Attempt to start up a bluetooth connection.
    # If successful, look for a client.
    # Once a connection is established, continue reading data from socket until client exits.
    def run(self):

        while True:
            
            try:
                self.init_bluetooth_server()
            except Exception as error:
                message = "[BluetoothReceiverHandler] WARNING: Bluetooth failed to initiate: {}".format(error)
                self._telemetry_send(message)
                return
            
            self._client_sock, self._client_info = self.wait_for_client()
            message = "[BluetoothReceiverHandler] Bluetooth: Accepted connection from {}".format(self._client_info)
            self._telemetry_send(message)
            
            try:
                while True:
                    data = self._client_sock.recv(1024)
                    if len(data) == 0: break

                    # Incoming data is interpreted as json
                    try:
                        split_data = ['{'+package for package in data.split('{')[1:]]
                        for package in split_data:
                            formatted_data = json.loads(package)
                            self._computer_communication_send(formatted_data)
                    except ValueError as error:
                        message = "[BluetoothReceiverHandler] Bluetooth: Received non-json data: %s %s" % (data, error)
                        self._telemetry_send(message)
            except IOError, bluetooth.BluetoothError:
                message = "[BluetoothReceiverHandler] Bluetooth: Other part disconnected"
                self._telemetry_send(message)

            message = "[BluetoothReceiverHandler] Bluetooth: Disconnecting"
            self._telemetry_send(message)
            self._client_sock.close()
            self._server_sock.close()
            self._server_sock = None
            self._client_sock = None
            self._client_info = None
        
        
    ## Initiate a bluetooth server socket
    def init_bluetooth_server(self):
        os.system("sudo hciconfig hci0 piscan")
        
        self._server_sock = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        self._server_sock.bind( ("", bluetooth.PORT_ANY) )
        self._server_sock.listen(1)

        uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
        bluetooth.advertise_service(
            self._server_sock,
            "HexaHERM",
            service_id = uuid,
            service_classes = [ uuid, bluetooth.SERIAL_PORT_CLASS ],
            profiles = [ bluetooth.SERIAL_PORT_PROFILE ], 
            protocols = [ bluetooth.OBEX_UUID ] 
        )

        
    ## Wait and listen until a client connects to the server socket.
    # Once a connection is established, return the client socket.
    def wait_for_client(self):
        port = self._server_sock.getsockname()[1]
        message = "[BluetoothReceiverHandler] Bluetooth: Waiting for connection on RFCOMM channel %d" % port
        self._telemetry_send(message)
        return self._server_sock.accept()

    ## Send data to the client via the client socket
    # This function is used by another thread running the computer communication handler
    # due to this class being the only connection to the client.
    # @param data The data that is to be sent to the client.
    def bluetooth_send(self, data):
        if self._client_sock:
            self._client_sock.send(data)
    
    
