#!/bin/bash
# This file will setup routing from laptops.

sysctl net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -o wlp3s0 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i enp0s25 -o wlp3s0 -j ACCEPT


