## Main entry point
from time import sleep

from message import MessageBus
from threadlaunch import launchHandlers

from telemetry import TelemetryHandler
from sensordata import SensorDataHandler
from ai import AIHandler
from bluetoothreceiver import BluetoothReceiverHandler
from communication import CommunicationHandler
from remotecontrol import RemoteControlHandler
from mux import MuxHandler
from walkingalgorithm import WalkingAlgorithmHandler
from legcommunication import LegCommunicationHandler
from legmodulereceiver import LegModuleReceiverHandler


## Main function to set up all busses, handlers and threads
def main ():
    # All busses connecting the threads
    telemetry_bus              = MessageBus ()
    sensor_data_bus            = MessageBus ()
    ai_parameter_bus           = MessageBus ()
    bluetooth_receiver_bus     = MessageBus ()
    computer_communication_bus = MessageBus ()
    remote_control_data_bus    = MessageBus ()
    mux_data_bus               = MessageBus ()
    walking_algorithm_bus      = MessageBus ()
    leg_communication_bus      = MessageBus ()
    leg_module_receiver_bus    = MessageBus ()

    # Collection of all busses
    busses = [
        telemetry_bus,
        sensor_data_bus,
        ai_parameter_bus,
        bluetooth_receiver_bus,
        computer_communication_bus,
        remote_control_data_bus,
        mux_data_bus,
        walking_algorithm_bus,
        leg_communication_bus,
        leg_module_receiver_bus
    ]
    
    # Creation of all handlers with respective busses
    telemetry_handler = TelemetryHandler ( 0,
        telemetry_bus.recv,
        computer_communication_bus.send)
    sensor_data_handler = SensorDataHandler ( 1,
        ai_parameter_bus.send,
        telemetry_bus.send)
    ai_handler = AIHandler (2,
        ai_parameter_bus.recv,
        mux_data_bus.send,
        telemetry_bus.send)
    bluetooth_receiver_handler = BluetoothReceiverHandler ( 3,
        computer_communication_bus.send,
        telemetry_bus.send)
    communication_handler = CommunicationHandler ( 4,
        computer_communication_bus.recv,
        ai_parameter_bus.send,
        mux_data_bus.send,
        remote_control_data_bus.send,
        bluetooth_receiver_handler.bluetooth_send)
    remote_control_handler = RemoteControlHandler ( 5,
        remote_control_data_bus.recv,
        mux_data_bus.send)
    mux_handler = MuxHandler ( 6,
        mux_data_bus.recv,
        walking_algorithm_bus.send,
        telemetry_bus.send)
    walking_algorithm_handler = WalkingAlgorithmHandler ( 7,
        walking_algorithm_bus.recv,
        leg_communication_bus.send,
        telemetry_bus.send)
    leg_communication_handler = LegCommunicationHandler ( 8,
        leg_communication_bus.recv,
        telemetry_bus.send)
    leg_module_receiver_handler = LegModuleReceiverHandler ( 9,
        leg_communication_handler.serial_read,
        walking_algorithm_bus.send,
        telemetry_bus.send)

    # Collection of all active handlers
    handlers = [
        telemetry_handler,
        sensor_data_handler,
        ai_handler,
        bluetooth_receiver_handler,
        communication_handler,
        remote_control_handler,
        mux_handler,
        walking_algorithm_handler,
        leg_communication_handler,
        leg_module_receiver_handler
    ]
    
    
    # Start an unique thread for each handler
    threads = launchHandlers(handlers)
    
    # Wait for user termination and tell all handlers to shut down
    try:
        sleep(100000)
    except:
        pass
    finally:
        stopBusses(busses)
        leg_module_receiver_handler.running = False

## Send "STOP" termination command to all busses
def stopBusses(busses):
    for bus in busses:
        bus.send(-1, "STOP")


if __name__ == "__main__":
    main()
