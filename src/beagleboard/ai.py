from autonomous.ai import AI
from autonomous.controller import Controller
## @package ai
# Decision making for autonomous movements

## Handles the autonomous decision making for the robot's movements
class AIHandler:

    ## Initialize AIHandler
    # @param ai_parameter_recv Pipe for incoming movement parameter data
    # @param mux_data_send Pipe for outgoing movement decision data
    # @param telemetry_send Pipe for outgoing movement decision data
    def __init__(self, id, ai_parameter_recv, mux_data_send, telemetry_send):
        self.id = id
        self._ai_parameter_recv = ai_parameter_recv
        self._mux_data_send = lambda data : mux_data_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)

        self.velocity = 200
        self.angular_velocity = 0.5

        
    ## Run the AIHandler
    # Handles incoming sensordata and ai-parameters in order to calculate the next movement.
    # Incoming changes in autonomous mode resets the ai and pid regulator.
    #
    # Should run in its own thread
    def run(self):
        ai = AI(self.velocity, self.angular_velocity, self.send_decision_message, self.send_dance)
        pd = Controller(self.velocity, self.angular_velocity)
        
        while True:
            id, data = self._ai_parameter_recv ()
            if data == "STOP": break

            if id == 4: # communication
                if "velocity" in data:
                    self.velocity = data["velocity"]
                    ai.set_velocity( data["velocity"] )
                    pd.set_velocity( data["velocity"] )
                    log = "[AIHandler] Setting velocity: {}".format(data)
                    self._telemetry_send (log)
                if "angular_velocity" in data:
                    self.angular_velocity = data["angular_velocity"]
                    ai.set_angular_velocity( data["angular_velocity"] )
                    pd.set_angular_velocity( data["angular_velocity"] )
                    log = "[AIHandler] Setting angular velocity: {}".format(data)
                    self._telemetry_send (log)
                if "autonomic" in data:
                    ai.__init__(self.velocity, self.angular_velocity, self.send_decision_message, self.send_dance)
                    pd.v_c.reset()
                    pd.omega_c.reset()
                    pd.front_c.reset()

                # Set parameters.
                if "set_v_kp"     in data: pd.v_c.k_p     = data["set_v_kp"]
                if "set_v_kd"     in data: pd.v_c.k_d     = data["set_v_kd"]
                if "set_v_ki"     in data: pd.v_c.k_i     = data["set_v_ki"]
                if "set_front_kp" in data: pd.front_c.k_p = data["set_front_kp"]
                if "set_front_kd" in data: pd.front_c.k_d = data["set_front_kd"]
                if "set_front_ki" in data: pd.front_c.k_i = data["set_front_ki"]
                if "set_omega_kp" in data: pd.omega_c.k_p = data["set_omega_kp"]
                if "set_omega_kd" in data: pd.omega_c.k_d = data["set_omega_kd"]
                if "set_omega_ki" in data: pd.omega_c.k_i = data["set_omega_ki"]
                if "set_front_sc" in data: pd.front_c.scale = data["set_front_sc"]
                if "set_v_sc"     in data: pd.v_c.scale     = data["set_v_sc"]
                if "set_omega_sc" in data: pd.omega_c.scale = data["set_omega_sc"]

            elif id == 1: # sensor unit
                #data = ['magic', 'checksum', 'auto-button', 'gyro', 'sensor1', 'sensor2', 'sensor3', 'sensor4', 'sensor5']
                sensor_data = [data['gyro']] + data['sides'] + [data['front']]
                move, evaluated_distances = ai.get_next_movement(sensor_data)
                x,y,omega = pd.get_next_movement(move, sensor_data, evaluated_distances)
                move = y,x,-omega
                move = {'move': move}
                self._mux_data_send(move)

                # Button for autonomous trigger
                if data['autobutton']:
                    self._mux_data_send({'autobutton': data['autobutton']})
                    ai.__init__(self.velocity, self.angular_velocity, self.send_decision_message, self.send_dance)
                    pd.v_c.reset()
                    pd.omega_c.reset()
                    pd.front_c.reset()

            else:
                error = "[AIHandler] Received unknown data from {}: {}".format(id, data)
                self._telemetry_send (error)

                
    ## Allow the AI to send decision messages to the computer
    # @param message
    def send_decision_message(self, message):
        self._telemetry_send (message)

    ## Allow the AI to send a decision to make the robot dance
    def send_dance(self):
        self._mux_data_send({"dance": True})
