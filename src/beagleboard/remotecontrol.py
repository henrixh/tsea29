## @package remotecontrol
# Routines for handling remote control

## Handles remote controlled movement commands passed from the computer program
class RemoteControlHandler:

    ## Initialize RemoteControlHandler
    # @param communication_recv Pipe for incoming movement commands
    # @param mux_data_send Pipe for outgoing commands sent to the Mux
    def __init__(self, id, communication_recv, mux_data_send):
        self.id = id
        self._communication_recv = communication_recv
        self._mux_data_send = lambda data : mux_data_send(self.id, data)

        self.velocity = 100.
        self.computer_velocity = 200.
        self.velocity_scale = self.velocity / self.computer_velocity
        
        self.angular_velocity = 0.5
        self.computer_angular_velocity = 0.8
        self.angular_velocity_scale = self.angular_velocity / self.computer_angular_velocity


    ## Run the RemoteControlHandler
    # Converts incoming movement parameters to a complete move. Passes on other
    # factors without formatting. Move the robot according to remote control
    # instructions
    #
    # Should run in its own thread
    def run(self):
        while True:
            id, data = self._communication_recv()
            if data == "STOP": break

            if id == 4: # communication

                tags = ["movement_x", "movement_y", "rotation"]
                if all( [tag in data for tag in tags] ):
                    command = {"move": [
                        data["movement_x"] * self.velocity_scale,
                        data["movement_y"] * self.velocity_scale,
                        data["rotation"] * self.angular_velocity_scale
                    ]}
                    self._mux_data_send(command)

                tags = ["tilt_x", "tilt_y", "tilt_z"]
                if all( [tag in data for tag in tags] ):
                    command = {"tilt": [
                        data["tilt_x"],
                        data["tilt_y"],
                        data["tilt_z"]
                    ]}
                    self._mux_data_send(command)
                    
                tags = ["shift_x", "shift_y", "shift_z"]
                if all( [tag in data for tag in tags] ):
                    command = {"shift": [
                        data["shift_x"],
                        data["shift_y"],
                        data["shift_z"]
                    ]}
                    self._mux_data_send(command)
                    
                tags = ["set_height", "set_torque", "dance"]
                if any( [tag in data for tag in tags] ):
                    self._mux_data_send(data)

                if "velocity" in data:
                    self.velocity = data["velocity"]
                    self.velocity_scale = self.velocity / self.computer_velocity
                    self._mux_data_send(data)
                if "angular_velocity" in data:
                    self.angular_velocity = data["angular_velocity"]
                    self.angular_velocity_scale = self.angular_velocity / self.computer_angular_velocity
                    self._mux_data_send(data)

            else:
                error = "[RemoteControlHandler] Received unknown data from {}: {}".format(id, data)
                self._telemetry_send (error)
            
