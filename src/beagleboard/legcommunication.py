import serial as s
import struct
import time

## @package legcommunication
# Communication to/from the beagleboard

## Handles communication between the beagleboard and the control module
class LegCommunicationHandler:

    controlUnit = None
    setServoID     = 0
    setMaxTorqueID = 1
    setSpeedID     = 2
    setEnableID    = 3
    setIdID        = 4
    setLedID       = 5
    setDispID      = 6


    ## Initialize LegCommunicationHandler
    # @param leg_communication_recv Pipe for incoming movement decision data to be sent to the control module.
    # @param telemetry_send Pipe for outgoing log messages
    def __init__(self, id, leg_communication_recv, telemetry_send):
        self.id = id
        self._leg_communication_recv = leg_communication_recv
        self._telemetry_send = lambda data : telemetry_send(self.id, data)

        try:
            self.controlUnit = s.Serial('/dev/ttyUSB0', 76800, timeout = 0.2)
        except Exception as error:
            message = "[LegCommunicationHandler] WARNING: Serial connection failed to initiate: {}".format(error)
            self._telemetry_send(message)


    # Run the LegCommunicationHandler
    # Start main loop for reading incoming data for servo angles and settings.
    def run(self):
        self.enableServos(1)
        self.setMaxTorque(800)
        self.setSpeed(1023)
        while True:
            id, data = self._leg_communication_recv()
            if data == "STOP": break

            if id == 7:
                if "angles" in data:
                    self.moveLegs(data["angles"])
                if "set_torque" in data:
                    self.setMaxTorque(data["set_torque"])

    ## Moves the legs by sending data to the control unit
    # @param angles - A list of 18 integers between 0 and 1023 representing each
    # servo position,
    def moveLegs(self, angles):
        if not self.controlUnit: return
        checksum = self.setServoID
        data = struct.pack("B", self.setServoID)
        anglesString = ""
        for a in angles:
            checksum += a >> 8
            checksum += a & 0xFF
            anglesString += struct.pack("H", (a & 0xFFFF))
        data += struct.pack("B", (checksum & 0xFF))
        data += anglesString
        self.controlUnit.write(data)

    ## Sets the torque enable register in all servos.
    # @param enable - The value to set the register to.
    def enableServos(self, enable):
        if not self.controlUnit: return
        checksum = self.setEnableID + enable
        data = struct.pack("B", self.setEnableID)
        data += struct.pack("B", (checksum & 0xFF))
        data += struct.pack ("B", (enable & 0xFF))
        self.controlUnit.write(data)

    ## Sets the maximum torque on the servos.
    # @param torque - A integer between 0 and 1023 representing the torque.
    def setMaxTorque(self, torque):
        if not self.controlUnit: return
        checksum = self.setMaxTorqueID + (torque & 0xFF) + (torque >> 8)
        data = struct.pack("B", self.setMaxTorqueID)
        data += struct.pack("B", (checksum & 0xFF))
        data += struct.pack("H", (torque & 0xFFFF))
        self.controlUnit.write(data)

    ## Sets the maximum speed on the servos.
    # @param speed - A integer between 0 and 1023 representing the speed.
    def setSpeed(self, speed):
        if not self.controlUnit: return
        checksum = self.setSpeedID + (speed & 0xFF) + (speed >> 8)
        data = struct.pack("B", self.setSpeedID)
        data += struct.pack("B", (checksum & 0xFF))
        data += struct.pack("H", (speed & 0xFFFF))
        self.controlUnit.write(data)

    ## Sync uart communication
    # @param byte The byte to sync against
    # @param packetSize The size of the packet
    def syncUart(self, byte, packetSize):
        if not self.controlUnit: return None
        while True:
            a = self.controlUnit.read(1)
            if (a != "" and struct.unpack("B", a)[0] == byte):
                break
        packet = self.controlUnit.read(packetSize - 1)

    ## Compare checksums
    # @return - True or False if the checksums matched.
    def checkSum(self, data, length):
        checksum = data[0]
        for i in range(2, length):
            checksum += data[i] & 0x00FF
            checksum += data[i] >> 8
        if checksum & 0xFF == data[1]:
            return True
        return False

    ## Reads data from the control unit
    # @param telemetry_send - The bus to send data to the rest of the program.
    def serial_read(self, telemetry_send):
        if not self.controlUnit: return None
        read = False
        while(1):
            if not self.controlUnit.getCTS():
                read = False
                time.sleep(0.1)
            elif not read:
                self.controlUnit.flushInput()
                self.syncUart(0xBB, 38)
                read = True
            elif read:
                try:
                    data = struct.unpack("BB" + "H"*18, self.controlUnit.read(38))
                    if self.checkSum(data, 20):
                        telemetry_send({"servo angles": data[2:]})
                    else:
                        self.syncUart(0xBB, 38)
                except Exception as error:
                    print "Failed to read data from control unit: {}".format(error)


