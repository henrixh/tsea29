import numpy as np
import math
import time
from helpers import *
from leg import Leg
## @package Contains and handles all of the 6 legs

## Leg handler handles the legs and general movement
class LegHandler:
    legs = []
    height = 0
    min_lift_height = 20
    max_lift_height = 50

    dt = 1.0/50.0

    leg_switch = False
    start_ground_allowance = 0
    ground_allowance = 0

    ## Create a leg handler
    def __init__(self):
        self.step = 0
     
        self.legs = []

        offset_angle = math.pi/12

        # Add all 6 legs. Set their position, rotation, and offset angle.
        self.legs.append(Leg(
            np.matrix ([ 120, 60, self.height]).T,
            math.pi *1/4,
            -offset_angle))
        self.legs.append(Leg(
            np.matrix ([  0, 100, self.height]).T,
            math.pi *2/4,
            0))
        self.legs.append(Leg(
            np.matrix ([-120, 60, self.height]).T,
            math.pi *3/4,
            offset_angle))
        self.legs.append(Leg(
            np.matrix ([-120,-60, self.height]).T,
            math.pi *5/4,
            -offset_angle))
        self.legs.append(Leg(
            np.matrix ([  0,-100, self.height]).T,
            math.pi *6/4,
            0))
        self.legs.append(Leg(
            np.matrix ([ 120,-60, self.height]).T,
            math.pi *7/4,
            offset_angle))

        for leg in self.legs:
            leg.set_foot_position (leg.normal)

        for i in range(0, len(self.legs), 2):
            self.legs[i].on_ground = False

        # Relative movement direction and rotation
        self.dv = np.matrix ([0.,0.,0.]).T
        self.dv_length = 0
        self.tilt = np.matrix ([0.,0.,0.]).T
        self.shift = np.matrix ([0.,0.,0.]).T
        self.center_tilt = np.matrix ([0.,0.,0.]).T
        self.airbourne_speed = 1.2*200
        self.d_omega = 0
        self.dh = 0
        self.target_height = 130
        self.previous_no_movement = None

        # Dancing
        self.dance_state = None
        self.dance_timer = 0
    
    
    ## Run one tick
    # Tick the legs forward one tick
    def tick (self):
        self.step += 1

        # Check is dance is running
        if self.dance_state > 0:
            self.calculate_dance()
            self.calculate_tilt()
            return
        
        # Calculate the next tick
        self.adjust_height()
        
        self.dv_length = vec3_length(self.dv)
        no_movement = not np.any(self.dv) and self.d_omega == 0
        legs_at_normal = all( [not np.any(leg.optimal_landing - leg.normal) for leg in self.legs] )
        stable = False
        #if no_movement:
        if self.ground_allowance == 0 and legs_at_normal:
            stable = True
            self.start_ground_allowance = 0.5
            if self.dance_state == 0:
                self.dance_state = 1
            self.calculate_tilt()
        else:
            if self.previous_no_movement != no_movement:
                g = self.get_ground_allowance()
                self.ground_allowance = min(g, 1-g)
                self.start_ground_allowance = 0
            self.ground_allowance += self.airbourne_speed / 6000.
        if no_movement:
            self.set_height(self.height + self.dh)
        else:
            for leg in self.legs:
                leg.optimal_landing = np.matrix([0,0,0]).T
        if not stable or not no_movement:
            self.calculate_next_position()
        self.previous_no_movement = no_movement

    ## Gradually change the height until the target height is reached
    def adjust_height(self):
        self.dh = 0
        step = self.airbourne_speed / 100.
        if self.height < self.target_height:
            self.dh = step
        if self.height > self.target_height:
            self.dh = - step
        if abs(self.height - self.target_height) < step:
            self.height = self.target_height
            self.dh = 0
            
    ## Move legs according to the relative direction (self.dv) and rotation (self.d_omega)
    def calculate_next_position (self):
        # Create the rotation matrix along the robot's center
        omega = self.dt * self.d_omega
        r_matrix = angle_to_rotation_matrix (omega)

        for leg in self.get_grounded_legs ():
            leg.grounded_move = self.move_grounded (leg, r_matrix)

        for leg in self.legs:
            leg.allowance = self.leg_allowance (leg, r_matrix)
        self.ground_allowance = max( [leg.allowance for leg in self.get_grounded_legs()] )

        # If a switch between grounded legs and airbourne legs is activated
        if self.ground_allowance > 1.00:
            self.leg_switch = not self.leg_switch
            self.start_ground_allowance = 0
            self.ground_allowance = 0
        else:
            self.leg_switch = False
        if self.leg_switch:
            for leg in self.get_airbourne_legs():
                leg.set_foot_position (leg.optimal_landing)
            for leg in self.legs:
                leg.on_ground = not leg.on_ground
        else:
            for leg in self.get_grounded_legs():
                self.set_grounded_height (leg.grounded_move)
                leg.set_foot_position (leg.grounded_move)
            for leg in self.get_airbourne_legs():
                pos = self.move_airbourne(leg, r_matrix)
                self.set_airbourne_height (pos)
                leg.set_foot_position (pos)
        
    ## Transform legs by rotating and shifting their normal position, thus tilting the entire robot
    def calculate_tilt(self):
        tilt_angle = math.pi / 16
        m_tilt  = angle_to_rotation_matrix(tilt_angle * self.tilt[0,0], 'x')
        m_tilt *= angle_to_rotation_matrix(tilt_angle * self.tilt[1,0], 'y')
        m_tilt *= angle_to_rotation_matrix(tilt_angle * self.tilt[2,0], 'z')
        shift_distance = self.legs[0].normal_radius / 1.8
        m_shift = shift_distance * self.shift
        for leg in self.legs:
            p = leg.normal
            p = m_tilt * (p + self.center_tilt) + m_shift - self.center_tilt
            leg.set_foot_position(p)


    ## Initiate dance sequence
    def start_dance(self):
        if self.dance_state == None:
            self.dance_state = 0
            self.dance_timer = 0

    ## Set designated tilt and shift depending on dance pattern
    # Values in tilt and shift have to be from -1 to 1
    # Dance timer reaches from 0 to 1
    def calculate_dance(self):
        # Dance 1,2. Push up
        if self.dance_state == 1:
            self.shift[2,0] = 0.5 - 0.5 * math.cos( 3 * self.dance_timer * 2 * math.pi )
            self.dance_timer += 1/90.
        # Lean forward
        elif self.dance_state == 2:
            self.shift[2,0] = math.sin( self.dance_timer * math.pi/2 )
            self.tilt[1,0] = - math.sin( self.dance_timer * math.pi/2 )
            self.tilt[2,0] = 0
            self.center_tilt[0,0] = 120 * math.sin( self.dance_timer * math.pi/2 )
            self.dance_timer += 1/15.
        # Shake front
        elif self.dance_state == 3:
            self.shift[2,0] = 1
            self.tilt[1,0] = -1
            self.tilt[2,0] = math.sin( 3 * self.dance_timer * 2 * math.pi )
            self.center_tilt[0,0] = 120
            self.dance_timer += 1/55.
        # Lean backward
        elif self.dance_state == 4:
            self.shift[2,0] = 1 - math.sin( self.dance_timer * math.pi )
            self.tilt[1,0] = - math.cos( self.dance_timer * math.pi )
            self.tilt[2,0] = 0
            self.center_tilt[0,0] = 120 * math.cos( self.dance_timer * math.pi )
            self.dance_timer += 1/30.
        # Shake rear
        elif self.dance_state == 5:
            self.shift[2,0] = 1
            self.tilt[1,0] = 1
            self.tilt[2,0] = - math.sin( 3 * self.dance_timer * 2 * math.pi )
            self.center_tilt[0,0] = -120
            self.dance_timer += 1/55.
        # Return to normal
        elif self.dance_state == 6:
            self.shift[2,0] = math.cos( self.dance_timer * math.pi/2 )
            self.tilt[1,0] = math.cos( self.dance_timer * math.pi/2 )
            self.tilt[2,0] = 0
            self.center_tilt[0,0] = - 120 * math.cos( self.dance_timer * math.pi/2 )
            self.dance_timer += 1/15.
        # Dance over
        else:
            self.dance_state = None
            self.target_height = 170
            
        if self.dance_timer >= 1:
            self.dance_state += 1
            self.dance_timer = 0
    
    ## Get a list of servo angles
    #
    # @return list of floats
    def get_servo_angles(self):
        angles = []
        for leg in self.legs:
            for angle in leg.angle:
                angles += [angle]
        return angles

    ## Return a list of all legs currently designated to stay on the ground
    def get_grounded_legs(self):
        return [leg for leg in self.legs if leg.on_ground]

    ## Return a list of all legs currently designated to stay in the air
    def get_airbourne_legs(self):
        return [leg for leg in self.legs if not leg.on_ground]

    ## Change the height of the robot, and sequentially each leg's normal
    #
    # @param height the heigth
    def set_height(self, height):
        if height == self.height:
            return
        height = max(-98, min(198, height))
        self.height = height
        for leg in self.legs:
            leg.set_position(2, height)
            leg.set_allowance()
        self.start_ground_allowance = 0

    ## Change the velocity of the robot's airbourne legs
    #
    # @param velocity The airbourne moving speed that is doubled regardless
    def set_airbourne_speed(self, velocity):
        self.airbourne_speed = velocity * 1.2
    
    ## Return the suggested height for legs to lift to at max
    def get_lift_height(self):
        return min( self.max_lift_height, max(self.min_lift_height, self.height - 98))

    ## Calculate the optimal landing position to ensure longest traveling distance
    #
    # @param leg the leg to get the landing position for
    # @return position of the optimal landing position
    def get_landing_position(self, leg):
        omega_vec = leg.omega_vec * (self.d_omega * leg.normal_distance)
        move_vec = self.dv + omega_vec
        move_vec_len = vec3_length (move_vec)
        if move_vec_len > 0:
            move_vec *= leg.normal_radius / move_vec_len
        return leg.normal + move_vec
    

    ## Calculate a grounded movement for one leg
    #
    # @param leg
    # @param r_matrix The current rotation matrix
    # @return vector next position of the leg
    def move_grounded (self, leg, r_matrix):
        # Grounded movement is simple. First subtract the desired movement
        # vector v, then rotate with the inverse of the rotation matrix r.
        # The z axis is always the ground plane.
        v = self.dt * self.dv
        return r_matrix.T * (leg.get_foot_position() - v)
    
    ## Calculate an airbourne movement for one leg
    #
    # @param leg
    # @param r_matrixTThe current rotation matrix
    # @return vector next position of the leg
    def move_airbourne (self, leg, r_matrix):
        leg.optimal_landing = self.get_landing_position(leg)
        v = leg.optimal_landing - leg.get_foot_position()
        v_len = vec3_length (v)
        if v_len > 0:
            v /= v_len
        #v *= self.dv_length + leg.normal_distance * abs(self.d_omega)
        v *= self.airbourne_speed + leg.normal_distance * abs(self.d_omega)

        if (self.ground_allowance == 1.0):
            return leg.get_foot_position ()

        fac = (1 - leg.allowance) / (1 - self.ground_allowance)
        fac = max( min (fac, 2), 0.5)
        v *= -fac
        v *= self.dt

        return leg.get_foot_position() - v
    
    ## Calculate the remaining distance before a leg hits the edge of its allowance area
    #
    # @param leg
    # @param r_matrix The current rotation matrix
    # @return float the ratio of the way left for the leg to move,
    # and the total distance the leg can move.
    def leg_allowance (self, leg, r_matrix):
        pos = leg.get_foot_position()

        if self.dv_length == 0.0 and self.d_omega == 0:
            return self.ground_allowance
        
        if leg.on_ground:
            dv = pos - leg.grounded_move
        else:
            dv = leg.get_foot_position() - leg.optimal_landing
    
        backward, forward = ray_sphere_intersect_distances (
            pos,
            dv,
            leg.normal_radius,
            leg.normal)
        return max(0.001, forward / (forward - backward))


    ## Calculate the heigth above the ground for airbourne legs
    #
    # @param pos position to be airbourne
    # @return vector airbourne position
    def set_airbourne_height (self, pos):
        x = self.get_ground_allowance()
        heigth = -4 * x * (x - 1)
        
        if -1 < x < 1:
            pos[2,0] = heigth * self.get_lift_height()
        else:
            pos[2,0] = 0
        return pos
    
    ## Set the height to ground-level
    #
    # @param pos position to be ground-level
    # @return vector ground-level position
    def set_grounded_height (self, pos):
        pos[2,0] = 0
        return pos

    ## Return ground allowance dependant on its start value
    def get_ground_allowance(self):
        return (self.ground_allowance - self.start_ground_allowance) / (1 - self.start_ground_allowance)
