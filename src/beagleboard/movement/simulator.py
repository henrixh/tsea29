#MG 2014 GameJam

import pygame, sys, os.path, time
from math import *
import random
import numpy as np
from legHandler import *
from helpers import *

# Initialize pygame
pygame.init()

######################### Im so sorry
# Useful constants
screen_width = 600
screen_height = 600
screen_fps = 35

# -- Graphics --

# Colors
c_black = (0, 0, 0)
c_dkgray = (64, 64, 64)
c_gray = (128, 128, 128)
c_ltgray = (192, 192, 192)
c_white = (255, 255, 255)
c_red = (255, 0, 0)
c_orange = (255, 165, 0)
c_yellow = (255, 255, 0)
c_green = (0, 255, 0)
c_blue = (0, 0, 255)
c_cyan = (0, 255, 255)
c_purple = (225, 0, 225)
c_pink = (255, 181, 197)
c_brown = (165, 42, 42)

c_twitter = (0x00, 0xa0, 0xd1)
c_facebook = (0x3b, 0x59, 0x98)
c_youtube = (0xc4, 0x30, 0x2b)
c_tumblr = (0x34, 0x52, 0x6f)


# ---IO---

# Keyboard keys
k_backspace = pygame.K_BACKSPACE
k_tab = pygame.K_TAB
k_enter = pygame.K_RETURN
k_shift = pygame.K_LSHIFT
k_control = pygame.K_LCTRL
k_escape = pygame.K_ESCAPE
k_esc = pygame.K_ESCAPE
k_space = pygame.K_SPACE
k_left = pygame.K_LEFT
k_right = pygame.K_RIGHT
k_up = pygame.K_UP
k_down = pygame.K_DOWN
# ...and many more at http://www.pygame.org/docs/ref/key.html

# Mouse buttons
mb_left = 1
mb_middle = 2
mb_right = 3
mb_wheel_up = 4
mb_wheel_down = 5
mb_wheel_left = 6
mb_wheel_right = 7


# Error message
ERROR = "\033[31;1mERROR\033[0m "
######################### Im so sorry

######################### Im so sorry
# -- IOHandler --
# Collects user key- and mouse-input and stores it in maps

class IOHandler():
    # All stored user input
    text = ''
    keys = {}
    keys_pressed = {}
    keys_released = {}
    mouse = {'x': 0, 'y': 0, 'pre_x': 0, 'pre_y': 0}
    mouse_pressed = {}
    mouse_released = {}
    
    # Main pygame event handler
    def update(self):
        # Reset
        for e in self.keys_pressed:
            self.keys_pressed[e] = False
        for e in self.keys_released:
            self.keys_released[e] = False
        for e in self.mouse_pressed:
            self.mouse_pressed[e] = False
        for e in self.mouse_released:
            self.mouse_released[e] = False
        self.mouse['pre_x'] = self.mouse['x']
        self.mouse['pre_y'] = self.mouse['y']

        joystick = pygame.joystick
        joystick.init()
        for js in range(joystick.get_count()):
            joystick = joystick.Joystick(js)
            joystick.init()
    
        # Loop through stack of events
        pygame.event.pump()
        for event in pygame.event.get():

            # Quit-button
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                
            # Key pressed
            if event.type == pygame.KEYDOWN:
                try: self.keys[event.key]
                except: self.keys[event.key] = False
                if not self.keys[event.key]:
                    self.keys_pressed[event.key] = True
                    if event.key >= 97 and event.key <= 122:
                        self.text += pygame.key.name(event.key)
                    if event.key == 32 and self.text:
                        self.text += ' '
                    if event.key == 8:
                        self.text = self.text[0:-1]
                self.keys[event.key] = True
                
            # Key released
            if event.type == pygame.KEYUP:
                try: self.keys[event.key]
                except: self.keys[event.key] = True
                if self.keys[event.key]:
                    self.keys_released[event.key] = True
                self.keys[event.key] = False
                
            # Mouse pressed
            if event.type == pygame.MOUSEBUTTONDOWN:
                try: self.mouse[event.button]
                except: self.mouse[event.button] = False
                if not self.mouse[event.button]:
                    self.mouse_pressed[event.button] = True
                self.mouse[event.button] = True
                
            # Mouse released
            if event.type == pygame.MOUSEBUTTONUP:
                try: self.mouse[event.button]
                except: self.mouse[event.button] = False
                if self.mouse[event.button]:
                    self.mouse_released[event.button] = True
                self.mouse[event.button] = False
                
            # Mouse move
            if event.type == pygame.MOUSEMOTION:
                (self.mouse['x'], self.mouse['y']) = event.pos

            if event.type == pygame.VIDEORESIZE:
                graphics.screen = pygame.display.set_mode(event.dict['size'],pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)
                
    # Check if key or keys are in a dictionary
    def check_dict(self, key, dic, All):
        if isinstance(key, list):
            r = [self.check_dict(k, dic, All) for k in key]
            if All: return all(r)
            else: return any(r)
        try: return dic[key]
        except: return False

            
    # Keys
    
    # Check if key is held down
    def check_key(self, key, All=False):
        return self.check_dict(key, self.keys, All)
    
    # Check if key is pressed
    def check_key_pressed(self, key, All=False):
        return self.check_dict(key, self.keys_pressed, All)

    # Check if key is released
    def check_key_released(self, key, All=False):
        return self.check_dict(key, self.keys_released, All)
    

    # Mouse
    
    # Check if mousebutton is held down
    def check_mouse(self, button, All=False):
        return self.check_dict(button, self.mouse, All)
    
    # Check if mousebutton is pressed
    def check_mouse_pressed(self, button, All=False):
        return self.check_dict(button, self.mouse_pressed, All)

    # Check if mousebutton is released
    def check_mouse_released(self, button, All=False):
        return self.check_dict(button, self.mouse_released, All)
    
    # Get mouse position - tuple (x,y)
    def get_mouse(self):
        try: return (self.mouse['x'], self.mouse['y'])
        except: return False
    
    # Get mouse relative movement - tuple(rx,ry)
    def get_mouse_previous(self):
        try: return (self.mouse['pre_x'], self.mouse['pre_y'])
        except: return False
    
    
    # User written text (A-Z + space)
    
    # Reset written text
    def reset_text(self, text=''):
        self.text = text

    # Get written text
    def get_text(self):
        return self.text
######################### Im so sorry

######################### Im so sorry
# -- Graphics --
# Creates window and handles drawing events

class Graphics():
    def __init__(self, w, h, fps, title):
        # Window caption
        pygame.display.set_caption(title)

        # Screen size
        self.screen = pygame.display.set_mode((w, h), pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)

        # Fps
        self.clock = pygame.time.Clock()
        self.fps = fps
        self.msElapsed = 1
        self.fpsAVG = []
        self.step = 0
        self.timeA = time.time()
        self.timeB = 0
        self.timeAVG = []

        self.font = pygame.font.SysFont("courier new", 12)

    # Refresh display and keep fps
    def update(self):
        self.fpsAVG.append(1000/self.msElapsed)
        self.fpsAVG = self.fpsAVG[-20:]
        self.timeB = time.time()
        self.timeAVG.append((self.timeB - self.timeA) * float(self.fps))
        self.timeAVG = self.timeAVG[-20:]
        
        self.msElapsed = self.clock.tick(self.fps)
        self.timeA = time.time()
        self.step += 1
        pygame.display.update()

    # Shift (rotated) object to its center
    def shift_center(self, pos, image):
        shift = (- image.get_width() / 2.0, - image.get_height() / 2.0)
        return map(sum, zip(pos, shift))
        
    # Scale image after scale-factors
    def image_scale(self, image, xscale, yscale):
        return pygame.transform.scale(image, (
            int(xscale * image.get_width()),
            int(yscale * image.get_height())
        ))        

    # Draw background example
    def draw_background(self, texts):
        self.screen.fill(c_black)
        for i in range(len(texts)):
            text = self.font.render(texts[i], True, c_white)
            self.screen.blit(text, (0, i*text.get_size()[1]))
        

    # Draw body
    def draw_chassi(self, center, chassi, height, dv, omega):
        thick = 20
        
        dx, dy = center[0]
        pl = [(x+dx, y+dy) for x,y,a in chassi]
        # Polygon: pygame.draw.polygon(screen, color, pointlist, 5)
        pygame.draw.polygon(self.screen, c_gray, pl, 8)

        k = vec3_length(dv)/3
        pl = [(dx,dy), (dx+dv[1,0],dy+dv[0,0])]
        pygame.draw.lines(self.screen, c_red, False, pl, 1)
        text = self.font.render("Omega: %s"%omega, True, c_white)
        self.screen.blit(text, (dx, dy))

        dx, dy = center[1]
        pl = [(-200,height), (200,height)]
        pl = [(x+dx, y+dy) for x,y in pl]
        pygame.draw.lines(self.screen, c_gray, False, pl, 1)
        pls = [[(x, -thick), (x, thick)] for x,y,a in chassi]
        for pl in pls:
            pl = [(x+dx, y+dy) for x,y in pl]
            pygame.draw.polygon(self.screen, c_gray, pl, 8)
        x1,x2 = min([x for x,y,a in chassi]), max([x for x,y,a in chassi])
        pl = [(x1,-thick), (x1,thick), (x2,thick), (x2,-thick)]
        pl = [(x+dx, y+dy) for x,y in pl]        
        pygame.draw.polygon(self.screen, c_gray, pl, 8)

        dx, dy = center[2]
        pl = [(-200,height), (200,height)]
        pl = [(x+dx, y+dy) for x,y in pl]
        pygame.draw.lines(self.screen, c_gray, False, pl, 1)
        pls = [[(y, -thick), (y, thick)] for x,y,a in chassi]
        for pl in pls:
            pl = [(x+dx, y+dy) for x,y in pl]
            pygame.draw.polygon(self.screen, c_gray, pl, 8)
        y1,y2 = min([y for x,y,a in chassi]), max([y for x,y,a in chassi])
        pl = [(y1,-thick), (y1,thick), (y2,thick), (y2,-thick)]
        pl = [(x+dx, y+dy) for x,y in pl]        
        pygame.draw.polygon(self.screen, c_gray, pl, 8)
        
        
    # Draw arms
    def draw_arms(self, center, arms, height, line_trace):
        def draw_leg(pointlist, on_ground, alpha=1):
            if not line_trace:
                pygame.draw.lines(self.screen, map(lambda c:c*alpha, c_red), False, pointlist, 3)
            for pos in pointlist[:-1]:
                pygame.draw.circle(self.screen, map(lambda c:c*alpha, c_orange), map(int,pos), 3, 0)
            pygame.draw.circle(self.screen, map(lambda c:c*alpha, [c_cyan, c_blue][on_ground]), map(int,pointlist[-1]), 4, 0)
            
        #surf = pygame.Surface((screen_width, screen_height))
        dx,dy = center[0]
        for arm in arms:
            y,x,z = arm.optimal_landing
            try:
                if not (x == y == z == 0):
                    pygame.draw.circle(self.screen, c_yellow, map(int,[x+dx,y+dy]), 6, 0)
            except:
                pass
            x,y,z = arm.normal
            pygame.draw.circle(self.screen, c_yellow, map(int,[y+dx,x+dy]), int(arm.normal_radius), 1)
            pointlist = [(j[1]+dx,j[0]+dy) for j in arm.get_joint_positions()]
            draw_leg(pointlist, arm.on_ground)

        dx,dy = center[1]
        for arm in arms[2:4]:
            pointlist = [(j[1]+dx,lh.height-j[2]+dy) for j in arm.get_joint_positions()]
            draw_leg(pointlist, arm.on_ground, 0.3)
        for arm in arms[:2] + arms[4:]:
            pointlist = [(j[1]+dx,lh.height-j[2]+dy) for j in arm.get_joint_positions()]
            draw_leg(pointlist, arm.on_ground)

        dx,dy = center[2]
        for arm in arms[:3]:
            pointlist = [(j[0]+dx,lh.height-j[2]+dy) for j in arm.get_joint_positions()]
            draw_leg(pointlist, arm.on_ground, 0.3)
        for arm in arms[3:]:
            pointlist = [(j[0]+dx,lh.height-j[2]+dy) for j in arm.get_joint_positions()]
            draw_leg(pointlist, arm.on_ground)

######################### Im so sorry

# Initialize template classes
graphics = Graphics(screen_width, screen_height, screen_fps, 'HexaHERM')
ioHandler = IOHandler()


chassi = [(-100, 0,    180),
          (-60,  120,  135),
          ( 60,  120,  45),
          ( 100, 0,    0),
          ( 60,  -120, 315),
          (-60,  -120, 225)]

lh = LegHandler()
line_trace = False

# -- Game loop --
while True:

    # Basic
    graphics.update()
    ioHandler.update()
    if ioHandler.check_key_pressed(k_esc):
        pygame.quit()
        sys.exit()

    lh.dv = np.matrix ([0,0,0]).T
    lh.d_omega = 0
    k = 200
    if ioHandler.check_key(ord('a')): lh.dv -= np.matrix ([0,k,0]).T
    if ioHandler.check_key(k_left): lh.dv -= np.matrix ([0,k,0]).T
    if ioHandler.check_key(ord('d')): lh.dv += np.matrix ([0,k,0]).T
    if ioHandler.check_key(k_right): lh.dv += np.matrix ([0,k,0]).T
    if ioHandler.check_key(ord('w')): lh.dv -= np.matrix ([k,0,0]).T
    if ioHandler.check_key(ord('s')): lh.dv += np.matrix ([k,0,0]).T
    if ioHandler.check_key(ord('q')): lh.d_omega -= 0.5
    if ioHandler.check_key(ord('e')): lh.d_omega += 0.5
    if ioHandler.check_key(ord('r')): lh.target_height = 20
    if ioHandler.check_key(ord('f')): lh.target_height = 130

    if ioHandler.check_key_pressed(k_space):
        lh.start_dance()

    k = 1
    if ioHandler.check_key(k_down):
        lh.set_height(lh.height + 1)
        for leg in lh.legs:
            leg.set_allowance()

    if ioHandler.check_key(k_up):
        lh.set_height(lh.height - 1)
        for leg in lh.legs:
            leg.set_allowance()

    if ioHandler.check_key_pressed(pygame.K_F1):
        line_trace = not line_trace
        
    if ioHandler.check_key(pygame.K_F2):
        lh.__init__()
        
    if ioHandler.check_key(k_backspace):
        k = graphics.step / 50.0
        for leg in lh.legs:
            leg.set_foot_position (leg.normal +\
            np.matrix ([
                leg.normal_radius*math.cos(k),
                leg.normal_radius*math.sin(k),
                0]).reshape ((3,1)))

    if line_trace:
        surf = pygame.Surface(graphics.screen.get_size(), pygame.SRCALPHA)
        surf.fill((0,0,0,3))
        graphics.screen.blit(surf, (0,0))
    
    
    #if ioHandler.check_key(k_enter):
    lh.tick ()

    texts = [
        "[F1] Toggle line tracing",
        "[F2] Reset values",
        "[Escape] Quit",
        "[Q/W/E/A/S/D] Change direction",
        "[Arrowkeys] Change direction",
        "[Enter] LegHandler tick",
        "[Space] Get servo angles",
        "[Backspace] Move legs along allowance",
    ]
    
    # Draw
    if not line_trace:
        graphics.draw_background(texts)
    w,h = graphics.screen.get_size()
    center = (0.75*w, 0.5*h), (0.25*w,0.7*h), (0.25*w, 0.3*h)
    graphics.draw_chassi(center, chassi, lh.height, lh.dv, lh.d_omega)
    graphics.draw_arms(center, lh.legs, lh.height, line_trace)

