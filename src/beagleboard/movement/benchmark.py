from legHandler import *
from helpers import *
import numpy as np
import math
import time

if __name__ == "__main__":
    lh = LegHandler ()

    n = 1000
    t = time.time ()
    for i in range (n):
        lh.tick ()
    print n/(time.time () - t)
