import numpy as np
import math
from helpers import *
## @package Definitions for leg data

## Representation of one leg
#
# Contains data and methods for operating one leg on the robot
class Leg:
    # Length of the different parts of the leg
    length = np.array ([50,66,133])

    # Physical max/min limits for the leg
    max_limit = np.array ([math.pi/2,
                           math.pi/2,
                           math.pi/4])

    min_limit = np.array ([-math.pi/2,
                           -math.pi*3/4,
                           -math.pi*3/4])

    normal_distance = None


    ## Constructor
    #
    # @param position vector to the legs physical position on the robot
    # @param physical_rotation physical rotation of the leg on the robot, in radians
    # @param offset_angle the offset of the legs normal position from the legs
    # physical attachment angle
    def __init__ (self, position, physical_rotation, offset_angle):
        # Where is the leg situated?
        self.position = position
        # What rotation does the leg have?
        self.rotation = physical_rotation

        # Direction towards the legs comfortable zones center
        self.offset_angle = offset_angle

        # Normalized leg position, that is, the point in which the leg feels the
        # most comfortable.
        self.set_allowance ()
        self.allowance = 0

        # What positions do the servos have?
        self.angle = np.array ([0.0, 0.0, 0.0])

        # Cache already calculated foot position until a new one is issued
        self._foot_position_cache = None

        # Calculated landing point for airbourne legs
        self.optimal_landing = np.matrix([0,0,0]).T

        # Whether the leg is on the ground or not
        self.on_ground = True

        self.omega_vec = np.matrix(np.cross(np.matrix ([0,0,-1]), self.normal.T)).T
        self.omega_vec /= vec3_length(self.omega_vec)
        

    ## Nullify cache for foot position and allow it to be recalculated
    def reset_cache(self):
        self._foot_position_cache = None

    ## Set leg's main position (height)
    # @param n Which servo position to change
    # @param value Value of new angle
    def set_position(self, n, value):
        self.position[n] = value
        self.reset_cache()
        
    ## Set specific servo angle
    # @param n Which servo angle to change
    # @param value Value of new angle
    def set_angle(self, n, value):
        self.angle[n] = value
        self.reset_cache()


    ## Get absolute robot relative coordinates for a leg
    #
    # @return a numpy.matrix([[x],[y],[z]])
    def get_foot_position (self):
        if not self._foot_position_cache is None:
            return self._foot_position_cache

        # Let "r" be the distance in the "robot normal" plane
        r = self.length[1] * math.cos (self.angle[1]) \
        + self.length[2] * math.cos (self.angle[1] + self.angle[2]) \
        + self.length[0]

        # Let "z" be the distance from the "robot normal" plane
        z = self.length[1] * math.sin (self.angle[1]) \
        + self.length[2] * math.sin (self.angle[1] + self.angle[2])

        # Let "omega" be the angle of the first joint
        omega = self.angle[0]

        # Rotate omega to "robot space"
        omega = omega + self.rotation

        # We now have (r,omega,z). Convert this cylindrical vector to a cartesian
        # vector.
        w = cylindrical_to_cartesian (r, omega, z) + self.position

        # Move to correct position and return
        self._foot_position_cache = w
        return self._foot_position_cache


    ## Get absolute robot relative coordinates for each joint on a leg
    #
    # Is used in order to properly plot the joints in the simulator.
    #
    # @return a numpy.matrix([[x],[y],[z]])
    def get_joint_positions (self):
        positions = [np.matrix ([0,0,0]).T]

        omega = self.angle[0]
        omega = omega + self.rotation

        # First joint
        r = self.length[0]
        z = 0
        positions.append (cylindrical_to_cartesian (r, omega, z))

        # Second joint
        r = self.length[1] * math.cos (self.angle[1]) \
        + self.length[0]
        z = self.length[1] * math.sin (self.angle[1])
        positions.append (cylindrical_to_cartesian (r, omega, z))

        # Move to correct position
        positions = map (lambda x: self.position + x, positions)

        # Third joint
        positions.append (self.get_foot_position())

        return positions


    ## Perform inverse kinematics on one leg from global coordinates
    #
    # If the movement isn't possible, the function will return False and not modify
    # the given leg. If the movement is possible, the function will modify the legs
    # servo angles and return True.
    #
    # @param c_global is a vector to the desired global coordinates
    # @return bool is the movement possible at all?
    def set_foot_position (self, c_global):
        self.reset_cache()
        # Let the length of the joints be ji
        j0 = self.length[0]
        j1 = self.length[1]
        j2 = self.length[2]

        # First, move c to leg local coordinates
        c = c_global - self.position

        # Convert to cylindrical coordinates (r, omega, z)
        cs = cartesian_to_cylindrical(c)
        r, omega_j0, z = cs[0,0], cs[1,0], cs[2,0]

        # Rotate omega_j0 to the correct place
        omega_j0 = omega_j0 - self.rotation

        # Remove length of first joint from the radius
        r = r - j0

        # Calculate the distance between the first joint and the destination
        d = math.sqrt(r*r + z*z)

        # Bail if it's impossible.
        if d >= j1 + j2 or d < abs (j1 - j2):
            return False

        # Calculate the angle between j1 and d
        omega_j1_d = math.acos ((j1*j1 + d*d - j2*j2) / (2 * j1 * d))

        # Calculate the angle between (0,0) and (r,z)
        omega_d_0 = math.atan2(z,r)

        # Calculate the joint angle omega_j1
        omega_j1 = omega_j1_d + omega_d_0

        # Calculate the angle between the legs
        omega_j1_j2 = math.acos ((j1*j1 + j2*j2 - d*d) / (2 * j1 * j2))

        # Calculate omega_j2 from omega_j1_j2
        omega_j2 = - (math.pi - omega_j1_j2)


        # We now have all the servo angles. Compare these to max and min values.
        angle = np.array ([omega_j0, omega_j1, omega_j2])

        # Ensure all values are in [-pi,pi]
        angle = map ((lambda x: math.atan2 (math.sin (x), math.cos (x))), angle)

        # Check max/min limits
        limitcheck = lambda min_val, x, max_val: min_val < x < max_val
        if all ( map (limitcheck, self.min_limit, angle, self.max_limit)):
            self.angle = angle
            return True

        return False


    ## Calculate the allowed movement area a leg has access to
    #
    # The allowance is calculated by how far and near the leg can
    # reach on ground level. The result is converted into a circle,
    # rather than the actual half-moon area. This is to ensure no
    # collision, and allow faster, synchronized movement.
    def set_allowance(self):
        l0, l1, l2 = self.length
        ml2 = self.min_limit[2]
        height = self.position[2]

        # Maximum distance reachable from the second joint and whether it can reach the gruond
        max_reach = l1 + l2
        if max_reach < abs(height):
            return 0

        # Closest distantce to the second joint (when the two end legs are folded)
        min_reach = math.sqrt(l1*l1 + l2*l2 - 2*l1*l2 * math.cos(math.pi-ml2))+1

        # Furthest point you can reach on the ground from the first joint
        far_ground_point = l0 + math.sqrt( max_reach*max_reach - height*height )

        # Closest point you can reach on the ground from the first joint (ignoring walking underneath)
        near_ground_point = 60
        if min_reach > height:
            near_ground_point = l0 + math.sqrt( min_reach*min_reach - height*height )

        allowance_radius = (far_ground_point - near_ground_point) * 0.5

        # Max radius from each centerpoint to a possible collision. Calculated by hand from 60 degree start angles.
        # Max angle: pi*7/32
        collision_radius = 167
        if allowance_radius > collision_radius:
            allowance_radius = collision_radius

        # Distance on the ground from the legs position to the allowance center
        center_point_distance = far_ground_point - allowance_radius
        omega = self.rotation + self.offset_angle
        z = -height
        center_point = self.position + cylindrical_to_cartesian (center_point_distance, omega, z)

        # Minus one for safety measures
        self.normal = center_point
        self.normal_radius = allowance_radius - 1
        self.normal_distance = vec3_length (self.normal)
