import math

## Convert angles in radians to absolute servo positions
#
# @param angles list of floats representing radians
# @return list of int in [0,1023], representing servo positions
def convert_angles_to_servo_positions (angles):
    servo_range = 300/360. * 2 * math.pi
    step_per_rad = 1024 / servo_range
    
    angles = map ((lambda x: -x*step_per_rad), angles)

    offsets = [512 + 0,
               512 - 32,
               512 - 115] * 6
    
    angles = map ((lambda x,y: int(x+y)), offsets, angles)
    
    def inv (i):
        angles[i] = 1024 - angles[i]

    inv (0)
    inv (3)
    inv (6)
    inv (9)
    inv (10)
    inv (11)
    inv (12)
    inv (13)
    inv (14)
    inv (15)
    inv (16)
    inv (17)
    
    return angles
