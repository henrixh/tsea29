import numpy as np
import math

## @package Contains routines for forward kinematics

## Convert cylindrical coordinates to cartesian
#
# @param r float
# @param omega float
# @param z float
# @return a numpy.matrix([[x1],[x2],[x3]])
def cylindrical_to_cartesian (r,omega,z):
    return np.matrix ( [
        [r * math.cos (omega)],
        [r * math.sin (omega)],
        [z]
    ])


## Convert cartesian coordinates to cylindrical
#
# @param c numpy.array([[x1],[x2],[x3]])
# @param a numpy.matrix ([[r],[omega],[z]])
def cartesian_to_cylindrical (c):
    return np.matrix ([
        [math.sqrt (c[0,0]**2 + c[1,0]**2)],
        [math.atan2 (c[1,0], c[0,0])],
        [c[2,0]]
    ])

## Create a rotation matrix
#
# Create a rotation matrix for rotation around the z-axis.
#
# @param a rotation angle
# @param type Which axis the rotation matrix revolves
# @return 3 by 3 numpy matrix
def angle_to_rotation_matrix (a, type='z'):
    if type == 'z':
        return np.matrix ([
            [ math.cos (a), math.sin (a), 0],
            [-math.sin (a), math.cos (a), 0],
            [0,             0,            1]
        ])
    if type == 'y':
        return np.matrix ([
            [math.cos (a), 0,-math.sin (a)],
            [0,            1,            0],
            [math.sin (a), 0, math.cos (a)]
        ])
    if type == 'x':
        return np.matrix ([
            [1,             0,            0],
            [0,  math.cos (a), math.sin (a)],
            [0, -math.sin (a), math.cos (a)]
        ])


## The length of a 3D vector
#
# @param c numpy.array([[x1],[x2],[x3]])
# @return float
def vec3_length (c):
    return math.sqrt (c.T * c)

## Distance between two edges of a circle depending on a line of movement
#
# @param vorigin Current position, numpy.array([[x1],[x2],[x3]])
# @param vdir Movement direction, numpy.array([[x1],[x2],[x3]])
# @param radius Circle radius, float
# @param sorigin Center of circle, numpy.array([[x1],[x2],[x3]])
# @return float float
def ray_sphere_intersect_distances (vorigin, vdir, radius, sorigin):
    # http://www.cs.princeton.edu/courses/archive/fall00/cs426/lectures/raycast/sld013.htm
    v = vdir / vec3_length (vdir)
    l = sorigin - vorigin
    tca = (l.T * v)[0,0]
    
    d_squared = l.T * l - tca * tca

    if d_squared > radius*radius:
        return (-1,0)

    thc = math.sqrt (radius*radius - d_squared)
    t1 = tca - thc
    t2 = tca + thc

    if t1 == t2:
        return (-1,0)
    return (min (t1,t2), max(t1,t2))
