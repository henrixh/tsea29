from movement.legHandler import LegHandler
from movement.angleconvert import *
import time, numpy

## @package walkingalgorithm
# Servo motor movement decision making

## Handles general movement decisions and calculates leg and servo movement
class WalkingAlgorithmHandler:
    
    ## Initialize WalkingAlgorithmHandler
    # @param walking_algorithm_recv Pipe for incoming movement decision data
    # @param leg_communication_send Pipe for outgoing servo movement data
    # @param telemetry_send Pipe for outgoing leg movement decision telemetry data
    def __init__(self, id, walking_algorithm_recv, leg_communication_send, telemetry_send):
        self.id = id
        self._walking_algorithm_recv = walking_algorithm_recv
        self._leg_communication_send = lambda data : leg_communication_send(self.id, data)
        self._telemetry_send = lambda data : telemetry_send(self.id, data)

        
    ## Run the WalkingAlgorithmHandler
    #
    # Should run in it's own thread
    def run(self):
        lh = LegHandler ()
        dt = 1/35.

        while True:
            t = time.time ()

            # Update to new movement data by discarding all but the latest data,
            # thus emptying the queue.
            while True:
                id, data = self._walking_algorithm_recv (blocking=False)
                if data == "STOP": break

                if id == -1: # empty data
                    break
                
                elif id == 6: # mux
                    if "move" in data:
                        dx, dy, d_omega = data["move"]
                        lh.dv[0,0] = dx
                        lh.dv[1,0] = dy
                        lh.d_omega = d_omega
                        self._telemetry_send ({"dx":dx, "dy":dy, "d_omega":d_omega})

                    if "tilt" in data:
                        tilt_x, tilt_y, tilt_z = data["tilt"]
                        lh.tilt[0,0] = tilt_x
                        lh.tilt[1,0] = tilt_y
                        lh.tilt[2,0] = tilt_z
                        self._telemetry_send ({"tilt_x":tilt_x, "tilt_y":tilt_y, "tilt_z":tilt_z})

                    if "shift" in data:
                        shift_x, shift_y, shift_z = data["shift"]
                        lh.shift[0,0] = shift_x
                        lh.shift[1,0] = shift_y
                        lh.shift[2,0] = shift_z
                        self._telemetry_send ({"shift_x":shift_x, "shift_y":shift_y, "shift_z":shift_z})
                        
                    if "set_height" in data:
                        lh.target_height = data["set_height"]
                                                
                    if "set_torque" in data:
                        self._leg_communication_send (data)

                    if "velocity" in data:
                        # Ignoring angular velocity
                        lh.set_airbourne_speed(data["velocity"])

                    if "dance" in data:
                        lh.start_dance()
                        
                elif id == 9: # leg_module_receiver
                    pass
                    # self._telemetry_send()
                    # Received data from legs!
                    
                else:
                    error = "[WalkingAlgorithmHandler] Received unknown data from {}: {}".format(id, data)
                    self._telemetry_send (error)

            if data == "STOP": break
            
            lh.tick ()
            self._telemetry_send ({"tick_rate": 1/lh.dt})

            angles = lh.get_servo_angles ()
            angles = convert_angles_to_servo_positions (angles)
            command = {"angles": angles}
            self._leg_communication_send (command)
            
            # Communication with leg_control in same thread
            lh.dt = time.time () - t
            sleeptime = dt - lh.dt
            time.sleep(max (sleeptime, 0))
