## @package communication
# Communication to/from the beagleboard and to the computer program

## Handles communication between the beagleboard and the computer program
class CommunicationHandler:

    ## Initialize CommunicationHandler
    # @param communication_recv Pipe for incoming telemetry or bluetooth data
    # @param ai_parameter_send Pipe for control parameters to the AI handler
    # @param mux_data_send Pipe for manual/autonomous movement sent to the MUX
    # @param remote_control_data_send Pipe for movement commands sent to the remote control handler
    # @param bluetooth_send Send function linked to the bluetooth receivers connected bluetooth socket
    def __init__(self, id, communication_recv, ai_parameter_send, mux_data_send, remote_control_data_send, bluetooth_send):
        self.id = id
        self._communication_recv = communication_recv
        self._ai_parameter_send = lambda data : ai_parameter_send(self.id, data)
        self._mux_data_send = lambda data : mux_data_send(self.id, data)
        self._remote_control_data_send = lambda data : remote_control_data_send(self.id, data)
        self._bluetooth_send = bluetooth_send

    ## Run the CommunicationHandler
    # Collects data from bluetooth and passes it to respective parts of the main program.
    # It also passes all telemetry data via bluetooth to the computer.
    def run(self):
        while True:
            id, data = self._communication_recv ()
            if data == "STOP": break

            if id == 0: # telemetry_handler
                self._bluetooth_send(data)
            
            elif id == 3: # bluetooth_receiver
                if "power" in data:
                    pass
                
                if "autonomic" in data:
                    self._mux_data_send(data["autonomic"])
                    self._ai_parameter_send(data)

                tags = ["velocity", "angular_velocity"]
                if any( [tag in data for tag in tags] ):
                    self._ai_parameter_send(data)
                    self._remote_control_data_send(data)
                
                tags = ["movement_x", "movement_y", "rotation", "set_height", "set_torque", "tilt_x", "tilt_y", "tilt_z", "shift_x", "shift_y", "shift_z"]
                if any( [tag in data for tag in tags] ):
                    self._remote_control_data_send(data)
                if "lets_dance" in data:
                    self._remote_control_data_send({"dance":True})

                tags = [
                    "set_v_kp", "set_v_kd", "set_v_ki",
                    "set_front_kp", "set_front_kd", "set_front_ki",
                    "set_omega_kp", "set_omega_kd", "set_omega_ki",
                    "set_front_sc", "set_v_sc", "set_omega_sc"
                ]
                if any( [tag in data for tag in tags] ):
                    self._ai_parameter_send(data)

            else:
                error = "[CommunicationHandler] Received unknown data from {}: {}".format(id, data)
                self._telemetry_send (error)
                
