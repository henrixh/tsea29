import Queue
##@package message
# Simple support for message passing

## A simple message bus for message passing across threads
# Sent messages are guaranteed to be recieved in the order they are
# sent. Multiple threads using recv() at the same time are not
# supported, that is, the system is limited to a single reciever per
# message bus. The number of senders is unlimited.
class MessageBus():
    
    ## Initialize MessageBus
    def __init__(self):
        self.q = Queue.Queue()

    ## Send a message to the message bus
    # @param message Data that is to be stored in the bus
    def send(self, id, message):
        self.q.put([id, message])

    ## Recieve a message from the message bus
    # @param blocking=True If set to False the message bus will not block, but will instead return None in case of an empty message bus.
    def recv(self, blocking=True):
        if blocking or not self.q.empty():
            return self.q.get()
        return -1, None
