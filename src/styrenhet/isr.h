#include <avr/interrupt.h>
#include "defines.h"
//#include <util/delay.h>
#include "uart.h"


/**
 * Timer interrupt vector that sets a flag which tells the program to send data
 * to the beagleboard.
 */
ISR(TIMER3_COMPA_vect){
  sendData = true;
}

/**
 * Timer interrupt vector that sets a flag which tells the program to read data
 * from a servo.
 */
ISR(TIMER1_COMPA_vect) {
  readData = true;
}

/**
 * Timer interrupt vector that sets a flag which is used as timeout for servo
 * communication.
 */
ISR(TIMER2_COMPA_vect){
  PORTD &= ~(1 << servo_dir);
  if (servoUart.rxCurrent != 0 ){
    free(servoUart.rxCurrent);
    servoUart.rxCurrent = 0;
  }
}

/**
 * Timer interrupt vector that sets a flag which is used as timeout for
 * beagleboard communication.
 */
ISR(TIMER0_COMPA_vect){
  free(beagleUart.rxCurrent);
  beagleUart.rxCurrent = 0;
}

/**
 * A interrupt vector that gets called everytime the UART0 transceiver has
 * completed its task. UART0 is the UART that communicates with the servos.
 */
ISR(USART0_TX_vect){
  bool temp = servoUart.txCurrent->response;
  //Deallocate memory
  free(servoUart.txCurrent->data);
  free(servoUart.txCurrent);
  //Change signal direction if we expect a response
  if(temp){
    servoUart.txCurrent = 0;
    PORTD |= (1 << servo_dir);

    //Start timeout timer
    TCNT2 = 0;
    TCCR2B = (1 << CS22) | (1 << CS20);

  }else if (!bufferEmpty(&servoUart.txBuffer)){
    // If the txBuffer is not empty start a new transmission
    servoUart.txCurrent = (struct txPackage*) bufferHead(&servoUart.txBuffer);
    UCSR0B |= (1 << UDRIE0);
  }else {
    servoUart.txCurrent = 0;
  }
}

/**
 * A interrupt vector that gets called everytime the UART0 data register
 * is empty. UART0 is the UART that communicates with the servos.
 */
ISR(USART0_UDRE_vect){
  //Check if we are supposed to be here
  if ( ((PORTD >> servo_dir) & 1) == 0 &&
      (!bufferEmpty(&servoUart.txBuffer) || servoUart.txCurrent != 0)){

    //Disable rx timeout timer
    TCCR2B = 0;

    //Setup a new packet
    if (servoUart.txCurrent == 0) {
      servoUart.txCurrent = (struct txPackage*)bufferHead(&servoUart.txBuffer);
    }

    struct txPackage* pack = servoUart.txCurrent;

    if (pack->index < pack->size){
      UDR0 = pack->data[pack->index];
      pack->index++;
      return;
    }
  }
  //Disable data register empty interrupts
  UCSR0B &= ~(1 << UDRIE0);
}

/**
 * A interrupt vector that gets called everytime the UART0 resceiver has
 * completed its task. UART0 is the UART that communicates with the servos.
 */
ISR(USART0_RX_vect){
  byte_t data = UDR0;

  //Reset timeout timer
  TCNT2 = 0;

  //If its the first byte allocate memory
  if (servoUart.rxCurrent == 0){
    servoUart.rxCounter = 0;
    servoUart.rxCurrent = (byte_t*)malloc(sizeof(struct statusPackage));
  }

  if (servoUart.rxCurrent == NULL){
    PORTB |= (1 << PB0);
    return;
  }
  //Cast package to different pointer type
  struct statusPackage* pack = (struct statusPackage*)(servoUart.rxCurrent);

  //Check if we are reading parameters or header of packet
  if (servoUart.rxCounter < 5){
    servoUart.rxCurrent[servoUart.rxCounter] = data;
  }else{
    //Allocate memory for parameters
    if (servoUart.rxCounter == 5){
      pack->parameters = (byte_t*)malloc(sizeof(byte_t)*(pack->length-2));
      servoUart.rxSize = pack->length + 3;

      //Make sure malloc passed
      if(pack->parameters == NULL){
        free(servoUart.rxCurrent);
        PORTB |= (1 << PB0);
        servoUart.rxCurrent = NULL;
        return;
      }

    }
    //Save checksum and exit
    if (servoUart.rxCounter == servoUart.rxSize){
      pack->checksum = data;
      if (!bufferInsert(&servoUart.rxBuffer, servoUart.rxCurrent)){
        free(pack->parameters);
        free(pack);
      }

      servoUart.rxCurrent = 0;
      //Change signal direction
      PORTD &= ~(1 << servo_dir);
      //Disable timer
      TCCR2B = 0;
      TCNT2 = 0;
      //Start next transmission
      if (!bufferEmpty(&servoUart.txBuffer)){
        UCSR0B |= (1 << UDRIE0);
      }
    }else{
      pack->parameters[servoUart.rxCounter - 5] = data;
    }
  }
  servoUart.rxCounter++;
}

/**
 * A interrupt vector that gets called everytime the UART1 data register
 * is empty. UART1 is the UART that communicates with the beagleboard
 */
ISR(USART1_UDRE_vect){
  if (beagleUart.txCurrent == 0){
    //Return if buffer is empty
    if (bufferEmpty(&beagleUart.txBuffer)){
      UCSR1B &= ~(1 << UDRIE1);
      return;
    }
    //Get packet
    beagleUart.txCurrent = (struct txPackage*)bufferHead(&beagleUart.txBuffer);
  }

  struct txPackage* pack = beagleUart.txCurrent;

  //Send data
  if (pack->index < pack->size){
    UDR1 = pack->data[pack->index];
    pack->index++;
  }else{
    UCSR1B &= ~(1 << UDRIE1);
  }
}

/**
 * A interrupt vector that gets called everytime the UART1 transceiver has
 * completed its task. UART1 is the UART that communicates with the beagleboard.
 */
ISR(USART1_TX_vect){
  //Make sure we are relly done
  if (beagleUart.txCurrent->index < beagleUart.txCurrent->size){
    UCSR1B |= (1 << UDRIE1);
    return;
  }

  beagleUart.txCurrent = 0;
  // If the txBuffer is not empty start a new transmission
  if (!bufferEmpty(&beagleUart.txBuffer)){
    UCSR1B |= (1 << UDRIE1);
  }
}

/**
 * A interrupt vector that gets called everytime the UART1 resceiver has
 * completed its task. UART1 is the UART that communicates with the beagleboard.
 */
ISR(USART1_RX_vect){
  byte_t data = UDR1;
  //Reset timer
  TCNT0 = 0;
  
  if (beagleUart.rxCurrent == 0){
    //Start timer
    TCCR0B = (1<<CS02)|(1<<CS00);
    //Setup new packet
    beagleUart.rxCounter = 0;
    beagleUart.rxSize = instr_size[data];
    beagleUart.rxCurrent = (byte_t*)malloc(sizeof(byte_t) * instr_size[data]);
  }
  if( beagleUart.rxCurrent == NULL){
    PORTB |= (1 << PB0);
    return;
  }
  //Save data
  beagleUart.rxCurrent[beagleUart.rxCounter] = data;
  beagleUart.rxCounter++;

  //Done reading
  if (beagleUart.rxSize <= beagleUart.rxCounter){
    if(!bufferInsert(&beagleUart.rxBuffer, beagleUart.rxCurrent)){
      free(beagleUart.rxCurrent);
    }
    beagleUart.rxCurrent = 0;
    TCCR0B = 0;
  }
}
