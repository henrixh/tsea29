#ifndef BUFFER_H
#define BUFFER_H
#include <stdlib.h>
#include <stdio.h>
#include "defines.h"
#include <util/atomic.h>

#define SIZE 20

/**
 * A buffer implemented as a queue in a circular array. Head is the position of
 * the first element in the queue. Tail is the position where the next new
 * element will be placed.
 */
struct Buffer{
  volatile byte_t head;
  volatile byte_t tail;
  byte_t** array;
};

/**
 * Initializes a buffer.
 *  @Param buff - The buffer that is initiliazed,
 *  @Param size - The size of the buffer.
 */
void initBuffer(struct Buffer* buff){
  //Set variables
  buff->head = 0;
  buff->tail = 0;
  //Allocate memory for the array
  buff->array = (byte_t**)malloc(sizeof(byte_t*)*SIZE);
}

/** Checks if the buffer is empty */
bool bufferEmpty(struct Buffer* buff){
  return (buff->head == buff->tail);
}

/** Checks if the buffer is full */
bool bufferFull(struct Buffer * buff){
  return (buff->head == (buff->tail + 1) % SIZE);
}

/**
 * Inserts a new element into the buffer. If the buffer is full the call is
 * ignored.
 * @Param buff - The buffer
 * @Param element - The element to be inserted
 */
bool bufferInsert(struct Buffer* buff, byte_t* element){
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
    byte_t nextTail = (buff->tail + 1) % SIZE;
    //Make sure buffer is not full
    if (buff->head != nextTail){
      buff->array[buff->tail] = element;
      buff->tail = nextTail;
      return true;
    }
  }
  return false;
}

/**
 * Removes and returns the head of the buffer (the first element in the queue).
 * Returns -1 if the queue is empty.
 * @Param buff - The buffer.
 */
byte_t* bufferHead(struct Buffer* buff){
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
    if (!bufferEmpty(buff)){
      byte_t* result = buff->array[buff->head];
      buff->head = (buff->head + 1) % SIZE;
      return result;
    }
  }
  return (byte_t*)-1;
}


#endif
