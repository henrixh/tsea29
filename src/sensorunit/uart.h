

#ifndef UART_H
#define UART_H
#include "buffer.h"
#include "sensorDefines.h"

struct txPackage{
  volatile byte_t  size;
  volatile byte_t  index;
  byte_t* data;
  // If we expect a response, only used for halfduplex communication.
  bool response;
};

/**
 * Creates a new txPackage
 @Param size - The size of the package.
 @Param data - A pointer to the data
 @Param r - If we expect a response or not, only used
 * for halfduplex communication
 * @Return Returns a pointer to a new txPackage
 */
struct txPackage* newTxPackage(byte_t size, byte_t* data, bool r){
  struct txPackage* pack = (struct txPackage*)malloc(sizeof(struct txPackage));
  pack->size = size;
  pack->index = 0;
  pack->data = data;
  pack->response = r;
  return pack;
}

struct statusPackage{
  word_t filler;
  byte_t id;
  byte_t length;
  byte_t error;
  byte_t* parameters;
  byte_t checksum;

};


/**
 * Struct that controls the uart, it contains two buffers, one for data that
 * will be sent and one for data that it has recived.
 */
struct UART{

  //TX variables
  struct Buffer txBuffer;
  struct txPackage* txCurrent;
  //RX variables
  struct Buffer rxBuffer;
  byte_t rxCounter;
  byte_t rxSize;
  byte_t* rxCurrent;
};

/**
 * Initalizes a new uart struct.
 * @Param u - The uart to initiliaz.
 * @Param txBuffSize - The desired size of the tx buffer.
 * @Param rxBuffSize - The desired size of the rx buffer.
 */
void initUart (struct UART* u, word_t txBuffSize, word_t rxBuffSize){
  initBuffer(&u->txBuffer, txBuffSize);
  initBuffer(&u->rxBuffer, rxBuffSize);
  u->rxCurrent = 0;
  u->txCurrent = 0;
  u->rxCounter = 0;
  u->rxSize    = 0;
}

/**
 * Deallocates memory for the uarts buffers.
 * @Param u - The uart to free.
 */
void freeUart(struct UART* u){
  freeBuffer(&u->txBuffer);
  freeBuffer(&u->rxBuffer);
}

/**
* Calculates the checksum of package.
*/
byte_t calcCheckSum(byte_t* data, int size){
  byte_t result = 0;
  for (int i = 2; i < size; i++){
    result += data[i];
  }
  return ~result;
}



#endif
