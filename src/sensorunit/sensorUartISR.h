/**
 * sensorUartISR.h
 *
 * Interrupt service routine for uart
 */

#include <avr/interrupt.h>
#include <util/delay.h>
#include "uart.h"

struct UART beagleUart;

/**
 * A interrupt vector that gets called every time the UART1 data register
 * is empty. UART1 is the UART that communicates with the beagleboard
 */
ISR(USART1_UDRE_vect){

  if (beagleUart.txCurrent == 0){
    if (bufferEmpty(&beagleUart.txBuffer)){
      UCSR1B &= ~(1 << UDRIE1);
      return;
    }

    beagleUart.txCurrent = (struct txPackage*)bufferHead(&beagleUart.txBuffer);
  }

  struct txPackage* pack = beagleUart.txCurrent;

  if (pack->index != pack->size){
    UDR1 = pack->data[pack->index];
    pack->index++;
  }else{
    UCSR1B &= ~(1 << UDRIE1);
  }
}

/**
 * A interrupt vector that gets called every time the UART1 transceiver has
 * completed its task. UART1 is the UART that communicates with the beagleboard.
 */
ISR(USART1_TX_vect){
  free(beagleUart.txCurrent->data);
  free(beagleUart.txCurrent);
  beagleUart.txCurrent = 0;

  // If the txBuffer is not empty start a new transmission
  if (!bufferEmpty(&beagleUart.txBuffer)){
    beagleUart.txCurrent = (struct txPackage*) bufferHead(&beagleUart.txBuffer);
    UCSR1B |= (1 << UDRIE1);
  }
}
