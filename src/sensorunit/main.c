/**
 * main.c
 *
 * Autors: Hanna Müller & Elin Petersén
 * Date: 2015-11-07
 * Version: 1.0
 *
 * Sensor unit:
 * The timer interrupts controls the sensor measurements.
 * Counter 0 triggers an interrupt every 8 ms, used for gyro.
 * Counter 3 triggers an interrupt every 40 ms, used for IR-sensors.
 * Transfers a protocol to beagle board every 40 ms.
 *
 */

#include "uart.h"
#include "buffer.h"
#include "sensorUartISR.h"
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h>

/**
 * Global variables
 */
// 2D array of IR-sensor measurement, 5*values{IRsensor1,...,IRsensor5}.
unsigned int IRvalues [5][5];
// The current/latest mean value of IR-sensor measurement, 5 IR-sensors.
unsigned int meanIRvalue [5];
// Number of measurements before stating to filter.
unsigned int IRcounter;
// 1 = autonomous/manual button just pressed.
unsigned int buttonNr;
// Gyro values to save in protocol.
unsigned int gyroValues[5];
// To remember which gyro measurement we are at.
unsigned int gyroMeasurementCounter;

/**
 * I/O configuration
 */
void IOinit() {
  // All 8 bits for port A are set to input
  DDRA = 0x00;
  DDRB = 0x01; //uses PB0 as CTS
  DDRC = 0x00;
  // set PD3 to output, transmits data to beagleboard
  DDRD = 0x08;

  PORTA = 0x00;
  PORTB = 0x00;
  PORTC = 0x00;
  PORTD = 0x00;
}

/**
 * ADC initialization
 */
void ADCinit() {
  //enable ADC, prescaler div factor=128 which sets the adc conversion frequency around 125 kHz
  ADCSRA=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}

/**
 * Initializes the uart which communicates with the Beagle board
 * @Param baud - UART baud rate
 */
void initBeagleUart( unsigned int baud )
{
  // Set baud rate
  UBRR1H = (unsigned char)(baud>>8);
  UBRR1L = (unsigned char)baud;
  // Set frame format: 8data, 1 stop bit
  UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);
  // Enable transmitter and interrupts
  UCSR1B = (1 << TXEN1) | (1  << TXCIE1);
  // Double the uart baud rate
  UCSR1A = (1 << U2X1);
  // Set uart
  initUart(&beagleUart, 10, 0);
}


/**
 * Enable external interrupt.
 * Triggered by the autonomy/manually button.
 */
void externalInterruptEnable() {
  // set Global Interrupt Enable
  sei();
  // enable INT0 (PD2)
  EIMSK |= (1<<INT0);
  // trigger INT0 on rising edge
  EICRA |= (1<<ISC01)|(1<<ISC00);
}

/**
 * Enable internal interrupt.
 * 8-bit counter for gyro, interrupt every 8 ms.
 */
void counter0Enable() {
  // Enable Interrupt TimerCounter0 Compare Match A (TIMER0_COMPA_vect)
  TIMSK0 = (1<<OCIE0A);
  // Mode = CTC
  TCCR0A = (1<<WGM01);
  // Clock/1024, 0.000064 seconds per tick
  TCCR0B = (1<<CS02) | (1<<CS00);
  //0.000064*125=0.008 s, The OCR0A defines the top value for the counter, interrupt every 8 ms
  OCR0A=125;
}

/**
 * Enable internal interrupt.
 * 16-bit counter for IR-sensors, interrupt every 40 ms.
 */
void counter3Enable() {
  // Enable Interrupt TimerCounter3 Compare Match A (TIMER3_COMPA_vect)
  TIMSK3 = (1<<OCIE3A);
  // Clock/1024, 0.000064 seconds per tick, mode=CTC
  TCCR3B = (1<<CS32)|(1<<CS30)|(1<<WGM32);
  //0.000064*625~=0.04 s, The OCR3A defines the top value for the counter, interrupt every 40 ms
  OCR3A=625;
}

/**
 * Array declaration
 */
void arrayDeclaration() {
  int rowIRvalues;
  int colIRvalues;
  for (rowIRvalues=0; rowIRvalues<5; rowIRvalues++) {
    for (colIRvalues=0; colIRvalues<5; colIRvalues++) {
      IRvalues[rowIRvalues][colIRvalues] = 0;
    }
  }

  int row;
  for (row=0; row<5; row++) {
    meanIRvalue[row] = 0;
    gyroValues[row] = 0;
  }

}

/**
 * Gets the high bits (bit 15-8).
 * @param h - the integer to transmit
 * @return the high byte
 */
byte_t getHighbits (int h) {
  byte_t byte;
  h = h >> 8;
  byte = (byte_t) (h & 0xFF);
  return byte;
}

/**
 * Create new package and send protocol.
 * Enable uart interrupt.
 *
 * Package:
 * pack[0] = MAGIC
 * pack[1] = CHKSUM_L
 * pack[2] = CHKSUM_H
 * pack[3] = BUTTON
 * pack[4] = GYRO_L_0
 * pack[5] = GYRO_H_0
 * pack[6] = GYRO_L_1
 * pack[7] = GYRO_H_1
 * pack[8] = GYRO_L_2
 * pack[9] = GYRO_H_2
 * pack[10] = GYRO_L_3
 * pack[11] = GYRO_H_3
 * pack[12] = GYRO_L_4
 * pack[13] = GYRO_H_4
 * pack[14] = IR_L_0
 * pack[15] = IR_H_0
 * pack[16] = IR_L_1
 * pack[17] = IR_H_1
 * pack[18] = IR_L_2
 * pack[19] = IR_H_2
 * pack[20] = IR_L_3
 * pack[21] = IR_H_3
 * pack[22] = IR_L_4
 * pack[23] = IR_H_4
 */
void protocolTransfer(){

  int sum;
  byte_t* pack = (byte_t*)malloc(sizeof(byte_t)*24);

  pack[0] = (byte_t) 0xAC;

  pack[3] = (byte_t) buttonNr;

  pack[4] = (byte_t) gyroValues[0] & 0xFF;
  pack[5] = getHighbits(gyroValues[0]);
  pack[6] = (byte_t) gyroValues[1] & 0xFF;
  pack[7] = getHighbits(gyroValues[1]);
  pack[8] = (byte_t) gyroValues[2] & 0xFF;
  pack[9] = getHighbits(gyroValues[2]);
  pack[10] = (byte_t) gyroValues[3] & 0xFF;
  pack[11] = getHighbits(gyroValues[3]);
  pack[12] = (byte_t) gyroValues[4] & 0xFF;
  pack[13] = getHighbits(gyroValues[4]);

  pack[14] = (byte_t) meanIRvalue[0] & 0xFF;
  pack[15] = getHighbits(meanIRvalue[0]);
  pack[16] = (byte_t) meanIRvalue[1] & 0xFF;
  pack[17] = getHighbits(meanIRvalue[1]);
  pack[18] = (byte_t) meanIRvalue[2] & 0xFF;
  pack[19] = getHighbits(meanIRvalue[2]);
  pack[20] = (byte_t) meanIRvalue[3] & 0xFF;
  pack[21] = getHighbits(meanIRvalue[3]);
  pack[22] = (byte_t) meanIRvalue[4] & 0xFF;
  pack[23] = getHighbits(meanIRvalue[4]);

  sum = pack[0] + pack[3] + pack[4] + pack[5] + pack[6] + pack[7] + pack[8] +  pack[9] +
    pack[10] + pack[11] + pack[12] + pack[13] + pack[14] + pack[15] + pack[16] +
    pack[17] + pack[18] + pack[19] + pack[20] + pack[21] + pack[22] + pack[23];

  pack[1] = (byte_t) sum & 0xFF;
  pack[2] = getHighbits(sum);

  // Starts a new transmission
  bufferInsert(&beagleUart.txBuffer, (byte_t*)newTxPackage(24, pack, false));
  UCSR1B |= (1 << UDRIE1);

  buttonNr = 0;
}

/**
 * ADC conversion.
 * Reads value from port and returns it.
 * @param ch - channel, decides which IR-sensor to read
 * @return the value from ad-converter
 */
int ADCread(uint8_t ch) {

  // value from ADC and therefor sensor
  int ADCvalue = 0;
  int lowADC = 0;

  // AND operation with 7, will keep the value of ch between 0 and 7
  ch = ch&0b00000111;
  // AREF = AVcc, clear channels
  ADMUX = (1<<REFS0)|(0<<MUX0)|(0<<MUX1)|(0<<MUX2)|(0<<MUX3)|(0<<MUX4);
  // Select ADC Channel ch, must be 0-7
  ADMUX|=ch;
  // start single conversion, write 1 to ADSC
  ADCSRA |= (1<<ADSC);

  // wait for conversion to complete
  // ADSC becomes 0 again
  // till then, run loop continuously
  while(ADCSRA & (1<<ADSC));

  // get the 2 most significant values, from ADCL
  lowADC = ADCL;
  // get the 8 most significant bits from ADCH and add lowADC
  ADCvalue = ADCH<<8 | lowADC;

  return (ADCvalue);
}

/**
 * Calculate mean value for IR-sensors.
 * Updates meanIRvalues.
 */
void meanValueCalculation() {

  int sum = 0;

  int row;
  int col;
  for (col=0; col<5; col++) {
    for (row=0; row<5; row++) {
      sum += IRvalues[row][col];
    }
    sum = (int) round(sum/5);
    meanIRvalue[col] = sum;
    sum = 0;
  }
}

/**
 * Shift 2D-array row one step left (or up).
 */
void shiftLeft() {

  int row;
  int col;
  for (row=0; row<4; row++){
    for (col=0; col<5; col++) {
      IRvalues[row][col]= IRvalues[row+1][col];
    }
  }
}

/**
 * Interrupt service routine for INT0, for autonomous/manual mode.
 */
ISR(INT0_vect) {
  buttonNr = 1;
}

/**
 * Interrupt service routine for TIMER0, for gyro.
 */
ISR(TIMER0_COMPA_vect) {
  sei();
  uint16_t ADCvalue = 0;
  // channel 5 = PA5
  int ch = 5;
  // input voltage from gyro
  ADCvalue = ADCread(ch);
  gyroValues[gyroMeasurementCounter] = ADCvalue;

  if (gyroMeasurementCounter < 4){
    gyroMeasurementCounter++;
  }
  else {
    gyroMeasurementCounter = 0;
  }
}


/**
 * Interrupt service routine for TIMER3, for IR-sensors.
 */
ISR(TIMER3_COMPA_vect) {
  sei();

  uint16_t ADCvalue = 0;
  // ADC channel to be read, start at channel0 (sensor 1)
  int channel = 0;

  shiftLeft();

  // read value from each IR-sensor
  while(channel < 5){
    ADCvalue = ADCread(channel);
    IRvalues[4][channel] = ADCvalue;
    channel++;
  }

  int col;
  // first 5 IR-sensor measurements, add directly to meanIRvalue array
  if (IRcounter<5) {
    for (col=0; col<5; col++) {
      meanIRvalue[col] = IRvalues[4][col];
    }
    IRcounter++;
  }
  else {
    meanValueCalculation();
  }
  protocolTransfer();
}


int main(void) {

  IRcounter = 0;
  buttonNr = 0;
  gyroMeasurementCounter = 0;

  arrayDeclaration();
  IOinit();
  ADCinit();
  externalInterruptEnable();
  initBeagleUart(25);

  // set CTS to low
  PORTB &= ~(1 << PORTB0);

  counter0Enable();
  // 60 ms delay before enable the counter for IR-sensors, unstable output in the beginning
  _delay_ms(60);
  counter3Enable();

  while(1) {}
  return 1;
}
