
#ifndef DEFINES_H
#define DEFINES_H
#define true 1
#define false 0

typedef unsigned char bool;
typedef unsigned char byte_t;
typedef unsigned int word_t;

#define F_CPU 16000000UL
#define BROADCASTING_ID 0xFE



#endif
