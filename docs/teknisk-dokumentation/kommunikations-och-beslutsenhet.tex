\section{Kommunikations- och Beslutsenhet}
Kommunikations- och Beslutsenheten ansvarar dels för kommunikation med både
dator och systemets AVR-processorer, och dels för AI-beslut, regleralgoritm och
gångalgoritm. Denna modul är väldigt mjukvarubaserad och kan utan problem
ersättas med till exempel en vanlig laptop.

\subsection{Hårdvara}
Kommunikations- och Beslutsenheten är implementerad på en Beagleboard-xM
\cite{beagleboard}, något som gör att man kan skriva mjukvaran snabbt och enkelt
i Python samt använda den fulla kraften hos det Linuxsystem som körs på den.

Man använder en WiFi-dongel för att få tillgång till internet och en
Bluetooth-dongel för att kommunicera med dator. För kommunikation med de två
AVR-processorerna används två USB-serielladaptrar. Linux hanterar all hårdvara
utan att någon konfiguration är nödvändig. Det finns dock en liten möjlighet
att Linux under uppstart råkar ``byta plats'' på USB-serielladaptrarna.

\subsection{Operativsystem}
Man har använt Ubuntu \cite{beagleboardubuntu}, då detta system var enkelt att
installera och konfigurera. Ovanpå detta har man installerat diverse mjukvara
både för utveckling av programvaran och nödvändiga programvarubibliotek. För att
ansluta till internet används \texttt{NetworkManager}.

\subsection{Programmeringsspråk}
För att snabbt och smidigt kunna utveckla mjukvaran har man valt att använda
Python. För bästa stöd gentemot bibliotek har man även valt att arbeta med
version 2 av språket.

\paragraph{Programmvarubibliotek}
Man har valt att använda ett antal programvarubibliotek för att förenkla
utvecklingsarbetet.
\begin{itemize}
  \item PySerial \cite{pyserial}, används för seriell kommunikation över UART,
  \item NumPy \cite{numpy}, används för diverse beräkningar för kinematik och
    gångalgoritmer,
  \item PyBluez \cite{pybluez}, används för bluetoothkommunikation med dator.
\end{itemize}

\subsection{Mjukvaruarkitektur}
Enhetens mjukvara bygger på en flertrådad arkitektur där varje tråd ansvarar för
en begränsad uppgift.

\begin{figure}
  \includegraphics[width=\textwidth]{mjukvaruarkitektur.pdf}
  \caption[Schematisk bild över mjukvaruarkitekturen]{Schematisk bild
    över mjukvaruarkitekturen. De ljusgrå rutorna visar hur programmet
    delas upp i flera trådar. Pilarna representerar de enkelriktade
    meddelandeköer som sätts upp mellan de olika modulerna. Streckade
    pilar är meddelandeköer specifikt för telemetridata.}
  \label{figure:mjukvaruarkitektur} 
\end{figure}
        
\paragraph{Main}
Huvudprogrammet ansvarar för att initiera alla hanterar klasser,
trådar och bussar sinsemellan. Vid uppstart skapas först alla bussar,
varav alla klasser instansieras med dess angivna bussar. Sist startas
alla trådar samtidigt och programmet är i gång.

\paragraph{Message passing}
Mjukvaran bygger på en message-passing arkitektur. Detta leder till en
enkel och ren modell för flertrådning av mjukvaran utan state mellan
trådarna. Nackdelen är dock en viss intern latens och en något ökad
minnesåtgång. Denna latens påverkar dock inte systemet märkbart, och
det finns gott om minne på kortet. Message passing bör inte ha någon
större effekt på prestandan, men beroende på schemaläggning kan tiden
för att utföra en uppgift variera ganska mycket. Detta har dock inte
varit något problem i praktiken. Vår message-passing implementation är
en tunn wrapper kring Pythons synkrona \texttt{Queue} som ingår i
standardbiblioteket.

Systemets message-passing-implementation kan användas både blockerande och
icke-blockerande. Ytterligare sätts en maxstorlek för meddelandekön. Denna
maxstorlek får aldrig uppnås under normal användning. Implementationen kallar
kön för \emph{pipe}, mottagarens del för \emph{recieve} och avsändarens del för
\emph{send}. Man bildar alltså ett par av \emph{sends} och \emph{recieves}.

Vid programstart skapas par av sends och recieves, varefter de ges som argument
till de trådar som startas upp, vilket bildar ett nätverk, en riktad graf.

För att systemet ska fungera krävs att följande krav uppfylls:
\begin{itemize}
\item Ingen del av systemet får använda sig av busy waiting. Detta skulle stjäla
  processortid från övriga trådar samt öka latensen i systemet. Därmed måste man
  antingen be schemaläggaren vänta en viss tid med \texttt{time.sleep(x)} eller
  använda blockerande message-passing.
\item Om man väljer att använda \texttt{time.sleep(x)} måste tråden läsa sin
  recieve icke-blockerande, samt se till så att den tömmer kön helt varje gång
  den läses. Att inte göra detta kan leda till att man fyller kön.
\item Om man använder blockerande message passing får man inte använda 
  \texttt{time.sleep(x)} i samma tråd. Dessutom får man då bara ha endast en
  recieve för att undvika att fylla upp de underliggande köerna.
\item Man får inte skicka meddelanden snabbare än de kan behandlas på
  mottagarsidan annat än högst tillfälligt.
\end{itemize}

\paragraph{Telemetri}
Telemetriklassen hanterar inkommande telemetridata från andra enheter
i syfte av att logga status, debug-meddelanden eller samlingar av
data. Klassen kan ta emot dictionaries eller strängar, oavsett från
var det kommer ifrån. Om inkommande data är en sträng så läggs
meddelandet in i en log-lista. Om det istället är en dictionary så
läggs det till i en uppsamlings dictionary som ska skickas vidare,
tillsammans med log-meddelanden, till kommunikationsmodulen.

På grund av att all inkommande data hanteras på samma villkor, medger
uppsamlings dictionary:n ett globalt namespace där data kan
övreskrivas av andra delar av programmet.

\paragraph{Sensordata}
Sensordataklassen tar hand om mottagandet av data från sensorenheten,
vilket sker seriellt med hjälp av bilioteket Pyserial. Klassen väntar
på att \textit{CTS} blir aktiv vilket betyder att sensorenheten är
redo att börja sända data till BeagleBoarden. Synkronisering av
paketen sker genom att leta efter en header som finns i varje
paket. För att försäkra att överföringen gått korrekt till sker en
kontroll med hjälp av den checksumma som medföljer varje paket. Innan
datan skickas vidare till relevanta mottagare konverteras rådatan från
sensorenheten till mer hanterbara enheter som centimeter, spänning
eller vinkelhastighet.

\paragraph{AI}
I denna klass hanteras all beslutstagning för autonoma
styrkommandon. Klassen väntar på inkommande sensordata och slussar
sedan igenom det via dess separata ai-modul och regleralgoritm-modul
för att få fram ett färdigt styrbeslut, se \ref{sub:ai} och
\ref{sub:regleralgoritm}. Sist vidarebefodras kommandot till
MUX. Klassen tar även hand om att informera MUX om knappen för byte
mellan manuellt och autonomt läge, vidarebefodran av sensordata och
autonoma beslut till telemetri, samt intern hantering av inkommande
styr- och reglerparametrar, alla vilka skickas till de separata
modulerna.

När sensordata kommer in i hanteraren skickas detta direkt in i
ai-modulen, vilket returnerar ett generellt styrbeslut om hur roboten
vill röra sig i helhet. Resultatet tillsammans med all sensordata
skickas direkt vidare till regleralgoritmen för finjustering av
styrningen. Till sist returneras det autonoma beslutet vilket
konverteras till ett färdigt styrkommando.

\paragraph{Bluetooth mottagare}
Denna klass hanterar all direkt dataöverföring via bluetooth mellan
datorprogrammet och BeagelBoarden. Koden är avsedd att ligga i
vilo-läge och lyssna på ett datorprogram som vill parakoppla sig,
varav en blåtandslänk initieras. Vid denna punkt kan data, enligt
protokollspecifikation i appendix, tas emot från datorprogrammet tills
dess att den kopplar ifrån.

I denna klass hanteras även data överföringen till datorprogrammet då
en blåtandslänk inkluderar både skicka som ta emot. Däremot hanteras
all skickning i kommunikation med dator modulen som ligger i en
separat tråd. Detta tillåter telemetri data att skickas kontinuerligt.

\paragraph{Kommunikation med dator}
Detta är den centrala enheten för kommunikation mellan robotens
interna system och datorprogrammet. Klassen har i uppgift att delegera
bluetooth kommandon till fjärrstyrning, AI samt MUX. Dessa kommandon
innefattar manuella styrbeslut, styrparametrar samt byte mellan
autonomt och manuellt läge. Klassen har även i uppgift att
vidarebeforda alla inkommande telemetri data paket via bluetooth till
datorprogrammet.

\paragraph{Fjärrstyrning}
Här passerar alla manuella styrkommandon och styrparametrar relevanta
till robotens benstyrning. Klassens huvuduppgift är att översätta
datorprogrammets relativt spridda kommandon till funktionerande
kommandon för benstyrningen. Denna abstraktion åtgärdar även
eventuella fel eller ofullständigheter av kommandon skickade från
datorprogrammet, samt tillåter ändringar i benstyrningen utan att
datorprogrammet behöver skrivas om.

\paragraph{MUX}
Denna klass har i uppgift att välja vilka styrkommandon som ska
skickas vidare till benstyrningen beroende på om roboten befinner sig
i manuellt eller autonomt läge. MUX tar kontinuerligt emot eventuella
styrkommandon från fjärrstyrning och AI, och bestämmer vad som ska
vidarebefodras.

MUX vidarebefodrar även styrparametrar från datorprogrammet som är
skilda från styrning. Dessa inkluderar ändringar i höjd, hastighet
samt servo-styrka.

\paragraph{Benstyrning}
Denna klass har i uppgift att läsa inkommande styrkommandon och
kontinuerligt beräkna vinkelbeslut för alla unika servomotorer, vilket
slutligen skickas till klassen för kommunikation med styrmodulen.

Benstyrningen använder sig av den separata gångalgoritm-modulen för
att utföra den grundliga servoberäkning, se
\ref{sub:walkingalgorithm}. Gångalgoritmens beräkningar klockas av
benstyrningen till att utföra beräkningar 35 gånger i sekunden, allt
för att hålla uppdateringen kontinuerligt. Efter beräkningar
konverteras alla utmatade servo vinklar i radianer till absoluta servo
positioner.

För att hålla beräkningshastigheten konstant läser klassen in
eventuella styrkommandon icke-blockerat från dess buss. Vid inläsning
töms bussen fullständigt för att försäkra benstyrningen om att den
hanterar senaste data.

\paragraph{Kommunikation med styrmodul}
Kommunikationen med styrmodulen sker seriellt med hjälp av biblioteket Pyserial.
Den kollar först om AVR är redo att skicka och ta emot data genom att kolla om
signalen CTS är aktiv. Den synkar paketen den får från styrmodulen genom att
leta efter paketets header. Den har ett antal olika funktioner för att skicka
alla kommandon till styrmodulen. Den kan skicka servo positioner, maximala
vridmomenten, maximala hastigheten och slå på/av vridmomentet.


\paragraph{Mottagare för styrmodul}
Här hanteras all seriell läsning av data skickat från styrenheten. Denna
data innefattar aktuella servo vinklar, vilket skickas direkt vidare
till telemetri enheten.
