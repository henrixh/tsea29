\section{Sensorenhet}

% Inledning
Mikrokontrollern i sensorenheten läser data från sensorerna och
knapparna som används för att återställa mikrokontrollern och för att
slå av och på det autonoma läget. Den mottagna datan behandlas samt
filtreras av enheten och skickas sedan vidare till kommunikations- och
beslutsenheten seriellt i ett väl definerat protokoll.

En översikt av sensorenheten visas i figur
\ref{fig:overview-sensorenhet}.  I figur
\ref{fig:kopplingsschema-sensor} visas ett kopplingsschema över
sensorenheten.

\begin{figure}[h]
  \centering \includegraphics[width=10cm]{sensorenhet-designskiss.pdf}
  \caption{Översikt av sensorenheten}
    \label{fig:overview-sensorenhet}
\end{figure}


% Hårdvara
\subsection{Hårdvara}
\paragraph{Mikrokontroller}
Mikrokontrollen som används till sensorenheten är en
\emph{ATmega1284P}. Den har åtta stycken analoga insignaler kopplade
till A/D-omvandlaren, där sensorerna är anslutna. Mikrokontrollern
har 128 KB minne, vilket är tillräckligt med utrymme för sensorenheten. 

\paragraph{Optiska avståndsmätare}
För att läsa av robotens omgivning är fem stycken optiska
avståndsmätare placerade på robotens chassi. Sensorernas placering
kan ses i figur \ref{fig:Sensorplacering}.  Utsignalerna från de
optiska avståndsmätarna är analoga.  Längst fram på roboten är
avståndsmätaren som mäter inom intervallet 10-80 cm (GP2Y0A21YK)
placerad.  På bägge sidorna av roboten är två sensorer som mäter
mellan 20-150 cm (GP2Y0A02YK) fastsatta så långt ifrån varandra som
möjligt för att undvika att störa varandras mätvärden.  Mellan de
optiska avståndsmätarna och AVR:en är ett LP-filter med resistansen
18kohm och kapacitansen 100nF fastvirade, i syfte att dämpa störningar
av mätvärdena.

\begin{figure}[h]
  \centering \includegraphics[width=10cm]{Sensor-placement.pdf}
  \caption{Sensorplacering över chassit sett från ovan.}
    \label{fig:Sensorplacering}
\end{figure}


\paragraph{Gyro}
För att beräkna vinkelhastigheten då roboten roterar används
vinkelhastighetssensorn \emph{MLX90609}.  Gyrot är placerad ovanpå
chassits mitt.  Utsignalen från gyrot kan avläsas både digitalt och
analogt och till sensorenheten valdes den analoga utsignalen.
Signalen är linjär och inget filter behöver användas.

\paragraph{Knapp för autonomt läge}
För att aktivera robotens autonoma/manuella läge används en avstudsad
tryckknappsmodul 2x1.  Den är placerad på chassins främre kant, för
att lätt komma åt den.  Tryckknappsmodulen har en grå och en blå
knapp, där det är den blå som används för att ändra till
autonomt/manuellt läge. Då roboten startas kommer den att vara i
manuellt läge.  Den grå knappen tillhör och hanteras i styrenheten.

\paragraph{Kristalloscillator}
En kristalloscillator \emph{EXO-3} används som klocka både till styr-
och sensorenheten.  Den har hastigheten 16 MHz.

\paragraph{JTAG}
En JTAG används för att programmera mikrokontrollen direkt från Atmel
Studio samt emulera programmet. JTAG:en är seriekopplad mellan styr-
och sensorenheten.

\paragraph{RESET}
En röd enkel tryckknapp är placerad längst fram på robotens chassi.
När denna knapp trycks ned återställs alla inställningar på
mikrokontrollen.

\paragraph{UART}
Mellan AVR:en och beagleboarden är en uart kopplad.  Den används för
att skicka paket innehållandes sensordata från sensorenheten till kommunikations- och
beslutsenheten.


% Mjukvara
\subsection{Mjukvara}
Mjukvaran i sensorenheten är skrivet helt och hållet i
programmeringsspråket C.  I centrum för mjukvaran står de olika
avbrott som sköter det mesta av programmets olika funktioner. Dessa
gör det möjligt för sensorenheten att ta hand om sensormätningar och
dataöverföring till kommunikations- och beslutsenheten efter specifika
tidsintervall.

\paragraph{Huvudloop}
Huvudloopen gör inte mycket annat än att initiera och möjliggöra
användning av de register och funktioner som behövs. Den väntar hela
tiden på olika avbrott.

% Avbrott
\paragraph{Timeravbrott}
Programmet för mikrokontrollern styrs av två tidsbundna avbrott, vilka
triggas var 8:e och 40:e millisekund.  I den snabbare tidsbaserade
avbrottsrutinen hämtas data från den senaste gyromätningen.  I detta
fall valdes tidsintervallet 8 ms för att få med alla gyrots
förädndringar med lagom noggrannhet.  I den andra avbrottsrutinen
hämtas data från alla IR-sensorer.  Därefter beräknas medelvärdet av
det nya värdet plus de fyra tidigare mätvärdena, för att filtrera bort
feldata. Detta görs för varje IR-sensor.  Vi satte timern på 40 ms
eftersom att IR-sensorerna mäts i samma takt.  Var 40:e ms sänds även
ett paket till besluts- och kommunikationsenheten, med de senaste
uppdateringarna av värdena från sensorerna och tryckknappen. Paketet
innehåller 5 gyro-mätningsvärde och en mätning för varje IR-sensorn,
eftersom gyrot läses av 5 gånger snabbare än IR-sensorerna.  Paketet
sänds med hjälp av en buffer, som är implementerad som en kö i en
cirkulär array innehållande pekare till minnet.

\paragraph{UART-avbrott}
Det tidsbundna avbrottet, på 40 ms, triggar igång uartavbrott i
samband med att paketet sätts.  I uartavbrottet hanteras paketet och
innehållet delas upp så att all data skickas seriellt via uarten.
Programmet använder två olika avbrott för uarten, \texttt{ TX
  complete} och \texttt{UDRE empty}.  De används för att skicka data
på UART-bussarna.  \texttt{UDRE empty} används för att skicka varje
enskild byte.  Först så hämtas ett helt paket ut från en buffer. Sedan
så skickas en byte vid varje avbrott. En räknare används för att veta
när hela paketet har skickats.  \texttt{TX complete} kallas efter att
ett paket har skickats färdigt och används bland annat till att rensa
upp allokerat minne.

\paragraph{Externa avbrott}
Programmet för mikrokontrollern innehåller även ett externt avbrott,
som triggas då den autonoma/manuella knappen tryckts ner.  Knappdatan
innehåller värdet 0 förutom då knappen tryckts ned.  Då blir den 1,
och så fort det första paketet med detta knappvärde har skickats
iväg så får den värdet 0 igen.

\paragraph{Protokoll}
Sensorenhetens kommunikationsprotokoll beskrivs i appendix
\ref{appendix:sensorkbm}. 




