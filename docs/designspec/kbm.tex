\section{Kommunikations- och Beslutsmodul}
Kommunikations- och beslutsmodulen ska hantera dels kommunikationen med datorn,
och dels besluts-, regler- och gångalgoritmer.

\subsection{Hårdvara}
\paragraph{Beagleboard}
Denna modul ska köras på enkortsdatorn Beagleboard-xM
\cite{beagleboard}. Denna dator har en ARM Cortex-A8 processor klockad
till 1GHz och 512MB minne. Därmed har datorn väldigt mycket prestanda,
även hårdvarustöd för de flyttalsberäkningar man kommer behöva göra i
samband med den omvända kinematiken.

\paragraph{USB $\leftrightarrow$ UART}
Eftersom Beagleboard-xM jobbar på en annan logiknivå (1.8V) än resten av
processorerna på roboten (5V) går det inte att koppla ihop dem direkt. Därför
används USB-anslutna serieadaptrar för att kommunicera med robotens
AVR-processorer.

\paragraph{Bluetooth}
För bluetoothkommunikation använder man sig av de-facto standardimplementationen
för Linux, BlueZ. För att sätta upp en serieport används konfigurationsfilen
\texttt{/etc/bluetooth/rfcomm.conf}, som läses och appliceras vid uppstart.

För den faktiska kommunikationen över bluetooth används text i
JSON-format, då detta är lätt att koda och avläsa. Den
överföringshastighet som bluetooth tillåter ska räcka till för
detta. För att undvika latensproblem ska dock den information som
skickas hållas relativt kort när kort latens önskas. Genom RFCOMM ska
bluetoothkommunikationen skötas genom PySerial, på samma sätt som
resten av seriekommunikationen.

Den faktiska hårdvara som krävs för bluetoothkommunikation är en
USB-bluetoothdongel av en modell som stöds av Linuxkärnan.

\subsection{Operativsystem och Mjukvara}
Då en Beagleboard är en fullfjädrad dator krävs ett operativsystem. Som
operativsystem har man valt att använda en ARM-version av Linuxdistributionen
Ubuntu. \cite{beagleboardubuntu}. Denna distribution kräver ett SD-kort på minst
2GB, men helst större. 

\paragraph{Systemets mjukvara}
Först och främst bör onödig mjukvara tas bort. Särskilt eventuella
grafiska program och bibliotek, till exempel X \cite{xorg}, tar upp
mycket plats och även mycket resurser ifall de starat upp tillsammans
med operativsystemet.

För att förenkla utvecklingsarbetet krävs viss mjukvara på systemet. Listat
nedan är de paket som behöver installeras.
\begin{itemize}
\item \texttt{openssh-server} - Tillåt inloggning på systemet från andra datorer.
\item \texttt{python2} - Referensimplementation av Python.
\item \texttt{make} - Kompilera programkod.
\item \texttt{gcc} - Kompilera programkod.
\item \texttt{git} - Enklaste sättet att föra över vår kod till systemet.
\item \texttt{emacs, vim} - Praktiskt att redigera kod direkt på systemet
  ibland. Även för eventuella konfigurationsfiler.
\end{itemize}

Gruppmedlemmar ska inte i allmänhet jobba med rooträttigheter på Beagleboardens
Linuxsystem. Därför skapas användarkonton utan rooträttigheter som används under
utvecklingsarbetet. Man kan även behöva ge dessa användare rättigheter att
använda \texttt{sudo} för att de ska kunna komma åt datorns serieportar.

\subsection{Programmeringsspråk}
Då man har en ARM Cortex-A8 tillsammans med en komplett Linuxmiljö har man
möjlighet att använda ett språk på högre abstraktionsnivå än C utan att robotens
prestanda blir lidande. Man kommer därför arbeta med Python, i version 2 av
språket.

\subsubsection{Programvarubibliotek}
\paragraph{NumPy}
Då man i beräkningarna kring benens rörelser kräver viss användning av
linjär algebra ska man använda sig av biblioteket numpy \cite{numpy}
för dessa beräkningar. Biblioteket är relativt väloptimerat och är
de-factostandard för numeriska beräkningar i Python.

\paragraph{PySerial}
Seriellkommunikationsbiblioteket PySerial \cite{pyserial} kommer
användas för kommunikationen mellan Kommunikations- och beslutsenheten
och systemets övriga enheter.

\subsection{Mjukvaruarkitektur}
Mjukvaran ska skrivas enligt en modell med flera processer eller trådar utan
delat state. Kommunikation mellan processerna sköts genom att skicka
meddelanden.

Ett huvudprogram ska användas för att starta upp alla
programvarumoduler och sätta upp trådarna, varvid trådarna sedan
kommunicerar med varandra via ett system byggt på Pythons inbyggda
trådsäkra Queue. Se figur \ref{figure:mjukvaruarkitektur} för en
översiktlig bild av mjukvaruarkitekturen.

\begin{figure}
  \includegraphics[width=\textwidth]{mjukvaruarkitektur.pdf}
  \caption[Schematisk bild över mjukvaruarkitekturen]{Schematisk bild över
    mjukvaruarkitekturen. De ljusgrå rutorna visar hur programmet delas upp i
    flera trådar. Pilarna representerar de enkelriktade meddelandeköer som sätts
    upp mellan de olika modulerna. Den data som skickas över meddelandeköerna
    kan vara i princip vad som helst beroende på sändare och mottagare, men är
    ofta olika typer av styr- och sensordata. Streckade pilar är meddelandeköer
    specifikt för telemetridata.}
  \label{figure:mjukvaruarkitektur} 
\end{figure}

Det är möjligt att använda antingen separata trådar eller separata processer
utan större förändringar i kod. Detta val kan alltså göras långt in i projektet
beroende på vilken schemaläggare som presterar bäst; Linuxkärnans schemaläggare
eller den schemaläggare som tillhör Pythons runtime.

\paragraph{Kommunikation med dator}
Här hanteras all bluetooth-kommunikation med datorn, det vill säga hur
data ska skickas samt tas emot. Inkommande styrkommandon sorteras och
skickas vidare till Fjärrstyrning. Kommunikationsdelen informerar
dessutom Muxen om robotens styrläge manuell eller autonom.

\paragraph{Fjärrstyrning}
Fjärrstyrningen tar emot styrkommandon från datorn och tolkar dessa
enligt ett protokoll. Styrdata konverteras till kommandon som är
vänliga för gångalgoritmen och vidarebefodrar dessa till Muxen.

\paragraph{Telemetri}
Telemetridatan består av all relevant information som ska skickas
vidare till datorprogrammet. Datan samlas ihop och skickas slutligen
till kommunikationsdelen som i sin tur ser till att vidarebefodra det
till datorn. Denna data består av
\begin{itemize}
\item Sensordata från Sensormodul,
\item Telemetridata från Styrmodul,
\item Styrbeslut från AI och Regleralgoritm,
\item Styrbeslut från Gångalgoritm.
\end{itemize}

\paragraph{Sensordata}
Mjukvaran tar emot sensordata ifrån sensorenheten i en separat tråd. Denna tråd
ansvarar för att skicka vidare sensordata till relevanta mottagare, till
exempel för telemetri eller AI- och regleralgoritmer.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{intersections_A.pdf}
    \caption[Intersection A]{Korsning typ A.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=0.65\textwidth]{intersections_B.pdf}
    \caption[Intersection B]{Korsning typ B.}
  \end{subfigure}
  \\
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{intersections_C.pdf}
    \caption[Intersection C]{Korsning typ C.}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=0.65\textwidth]{intersections_D.pdf}
    \caption[Intersection D]{Korsning typ D.}
  \end{subfigure}
  \caption{Skiss över de olika korsningar som kan förekomma.}
 \label{figure:intersections}
\end{figure}

\paragraph{AI}
Robotens AI kommer att vara aktivt i faser då roboten befinner sig i
olika typer av korsningar i labyrinten samt när roboten detekterar
målområdet. Resterande tid i labyrinten tar regleralgoritmen
kontrollen. Roboten har fem optiska sensorer till hjälp för att
upptäcka olika korsningar, se figur \ref{figure:intersections}.

\paragraph{Regleralgoritm}
Regleralgoritmens programmodul tar emot sensordata och kan med hjälp
av denna information avgöra robotens position relativt labyrintens
väggar. Därefter beräknas och skickas informationen om vilken rörelse
roboten ska utföra härnäst till gångalgoritmen.

Den styrsignal som ska skickas vidare till gångalgoritmen beräknas enligt
\begin{equation}
  \label{eq:reglerfunktion}
  u[n] = K_p \cdot e[n] + K_D \cdot \left (e[n] - e[n - 1] \right ),
\end{equation}
där $e[n]$ är felsignalen, $u[n]$ styrsignalen och $K_p$ samt $K_D$ är godtyckliga konstanter. 

%felsignal $(e[n])$ $\rightarrow$ regulator $\rightarrow$ styrsignal $(u[n])$

% PD-reglering (P=proportionerlig, D=deriverande) : u[n] = KP x e[n] + KD x (e[n]-e[n-1])

%Ökat KP-värde leder till: ökad snabbhet, minskade stabilitetsmarginaler, förbättrad kompensering av processtörningar, ökad styrsignalaktivitet
% Ökat TD-värde leder till:bättre stabilitetsmarginaler (större Td-värde ger bättre stabilitet) ökad inverkan av mätfel

% Om gyron används så kan följande PD-formel  användas: 
% u[n] = Kp x e[n] + K2 x vinkeln


\paragraph{Mux}
Mjukvaran innehåller en ``mjukvarumultiplexer'', vars uppgift är att
bestämma vad som får sända data till gångalgoritmen. Den tar emot
styrdata från de mjukvarumoduler som vill styra robotens gång, men
skickar enbart vidare data från en av dessa sändare. Muxen ska dock
prioritera styrkommandon från datorn, det vill säga att roboten kan
manuellt styras under autonom styrning, men återgår till de autonoma styrkommandona efter en viss tid.

\paragraph{Gångalgoritm}

Programmodulen för gångalgoritmen ska köras på en egen tråd som även
inkluderar benstyrningen. Här ska den generella benstyrningen
bestämmas utifrån begärd rörelse skickade från övriga moduler.
Beräkningen för benrörelsen utgår ifrån vilken riktning samt rotering
roboten ska förflytta sig med.

För både effektivitet samt enkelhet ska varje bens fot abstraheras som
en punkt i en 3D-rymd relativt robotens chassi. Denna relativa punkt
är vad som skickas vidare till benstyrningen för beräkning. För
optimerad gångstil ska de sex benen paras ihop i grupper om tre (där
en av grupperna är det främre vänstra, mittersta högra och bakre
vänstra benet). På detta sätt krävs endast en generell rörelse för de
två ``ben''-grupperna beräknas.

Resultaten av vilka relativa punkter som benen ska nå skickas vidare
till benstyrningen för beräkning.


\paragraph{Benstyrning}
Här ska den matematiska beräkningen för begärd servostyrning
beräknas. Modulen ska köras på samma tråd som gångalgoritmen och blir
kontinuerligt uppdaterad om den ovannämnda begärda målpunkten för
robotens fot.

Beräkningen för styrning av servomotorer ska utföras med hjälp av
inverterad kinematik. Eftersom varje ben är utrustad med tre
servomotorer som tillåter vridning i en vinkel, kan dessa abstraheras
till ett system som ska sträcka sig mellan varje bens utgångspunkt och
fot. Beräkningen anger varje servomotors begärda vinkel som slutligen
skickas vidare till styrmodulen enligt ett beskrivet protokoll.
% Men timing?

\paragraph{Sensordata från ben}
Den sensordata som tas emot från benen behandlas i en egen tråd. Denna tråd
ansvarar för att vidarebefodra datan till relevanta mottagare. Gångalgoritmen
och telemetridatatråden bör få denna data vidarebefodrat till sig.
