#!/bin/sh
rm *.aux *.bbl *.blg *.fdb_latexmk *.fls *.lof *.log *.lot *.out *.toc
latexmk -pdf -pvc -interaction=nonstopmode designspec.tex
rm *.aux *.bbl *.blg *.fdb_latexmk *.fls *.lof *.log *.lot *.out *.toc
